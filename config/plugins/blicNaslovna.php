<?php
return [
    'settingPage' => [
        'pageTitle' => 'Euroblic',
        'menuTitle' => 'Euroblic',
        'capability' => 'manage_options',
        'menuSlug' => 'blic-naslovna',
        'template' => 'blicNaslovna'
    ],
    'registerSettings' => [
        'optionGroup' => 'blicNaslovnaImgs',
        'options'     => [
            'blicNaslovnaImg' => [],
        ]
    ]
];