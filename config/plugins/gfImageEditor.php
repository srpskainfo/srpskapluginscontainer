<?php
return [
    'settingPage' => [
        'pageTitle' => 'GF Image Editor',
        'menuTitle' => 'GF Image Editor',
        'capability' => 'manage_options',
        'menuSlug' => 'gf-image-editor',
        'template' => 'gfImageEditor'
    ],
    'registerSettings' => [
        'optionGroup' => 'gfImages',
        'options'     => [
            'gfImage' => [],
        ]
    ]
];