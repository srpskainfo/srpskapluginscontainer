<?php
return [
    'settingPage' => [
        'pageTitle' => 'Add New Images To Queue',
        'menuTitle' => 'Add New Images To Queue',
        'capability' => 'manage_options',
        'menuSlug' => 'add-newly-added-images-to-queue',
        'template' => 'addNewlyAddedImagesToQueue'
    ],
    'registerSettings' => [
        'optionGroup' => 'addNewlyAddedImagesToQueues',
        'options'     => [
            'addNewlyAddedImagesToQueue' => [],
        ]
    ]
];