<?php
/*
Plugin Name: GF Plugins
Plugin URI:
Description: Green friends shop theme plugins
Author: Green Friends
Author URI: http://greenfriends.systems/
Version: 1.0.0
*/

namespace GfWpPluginContainer;

use GfWpPluginContainer\Gutenberg\Gutenberg;
use GfWpPluginContainer\Wp\CustomPostApi\CustomPostApi;
use GfWpPluginContainer\Wp\AklamatorRss\AklamatorRss;
use GfWpPluginContainer\Wp\PostCount;
use GfWpPluginContainer\Wp\PostHelper;
use GfWpPluginContainer\Wp\PostLogger;
use GfWpPluginContainer\Wp\PushNotifications\PushNotifications;
use GfWpPluginContainer\Wp\Widgets;
use GfWpPluginContainer\Wp\WpEnqueue;
use GfWpPluginContainer\Wp\ShortCodes;
use GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors;

define('PLUGIN_DIR_URI', plugin_dir_url(__DIR__ . '/gfShopThemePlugins/'));
define('PLUGIN_DIR', __DIR__ . '/');


/**
 * Trenutno će se settings pagevi konfigurisati kroz array, u budučnosti možemo i napraviti core clasu
 * al za sada mislim da je ovo bolje zbog perfomansi pošto je kod za kreiranje settinga za plugin isti pa ne
 * vidim poentu pravljenja posebnih klasa
 *
 */
class GfShopThemePlugins
{

    public function __construct()
    {
        $this->init();
    }

    private function actionAdminMenu($function)
    {
        add_action('admin_menu', function () use ($function) {
            $function();
        });
    }

    private function init()
    {
        //not needed for this project atm
//        WpEnqueue::addGlobalAdminStyle('bootstrap_css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
        WpEnqueue::actionAdminEnqueueScripts(static function () {
            wp_enqueue_media();
        });
        WpEnqueue::addFrontendScript('jquery');
//        WpEnqueue::addFrontendScript( 'jqueryUi', 'https://code.jquery.com/jquery-1.12.4.min.js');
//        WpEnqueue::addFrontendScript('jqueryUiMin', PLUGIN_DIR_URI . '/assets/js/jquery.ui.1.12.1.min.js');
        WpEnqueue::addGlobalAdminScript('fontAwesomeAdmin', PLUGIN_DIR_URI . '/assets/js/fa-adf4c66ded.js');

        //slick slider
//        WpEnqueue::addFrontendStyle('slickCss', PLUGIN_DIR_URI . '/assets/css/slick.css');
//        WpEnqueue::addFrontendStyle('slickThemeCss', PLUGIN_DIR_URI . '/assets/css/slick-theme.css');
//        WpEnqueue::addFrontendScript('slickJs', PLUGIN_DIR_URI . '/assets/js/slick.min.js', ['jquery']);

        //$this->addMainSettingPage(); for now this is not needed
        $this->setupPlugins();
        $this->actionAdminMenu(function () {
            remove_submenu_page('theme_plugins', 'theme_plugins');
        });
        $widgets = new Widgets();
        $widgets->registerWidgets();

        $gutBlocks = new Gutenberg();
        $gutBlocks->init();

        $postCount = new PostCount();
        $postCount->init();
        PostHelper::postPublishChecklist();

        $postLogger = new PostLogger();
        $postLogger->init();

        $multipleAuthors = new MultipleAuthors();
        $multipleAuthors->activate();

        $customPostApi = new CustomPostApi();
        $customPostApi->init();
        
        $pushNotifications = new PushNotifications();
        $pushNotifications->init();

        $aklamatorRss = new AklamatorRss();
        $aklamatorRss->init();



    }

    private function setupPlugins()
    {
        foreach (glob(__DIR__ . '/config/plugins/*.php') as $file) {
            $pluginInfo = require $file;
            $adminTemplateDirUri = PLUGIN_DIR_URI . 'templates/admin/';
            $adminTemplateDir = PLUGIN_DIR . 'templates/admin/';
            $scTemplateDir = PLUGIN_DIR . 'templates/shortcodes';
            $scTemplateDirUri = PLUGIN_DIR_URI . 'templates/shortcodes';
            $pageTitle = __($pluginInfo['settingPage']['pageTitle'], 'gfShopThemePlugins');
            $menuTitle = $pluginInfo['settingPage']['menuTitle'];
            $capability = $pluginInfo['settingPage']['capability'];
            $menuSlug = $pluginInfo['settingPage']['menuSlug'];
            $templateName = $pluginInfo['settingPage']['template'];
            $optionGroup = $pluginInfo['registerSettings']['optionGroup'];

            //For now this page is not needed
            if (strlen($pageTitle) > 0) {
                if ($pageTitle === 'Blic Iframe'){
                    continue;
                }
                //This page needs to be solo in menu
                if ($pageTitle === 'Euroblic'){
                    $this->addMenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName);
                }

                if($pageTitle === 'Set Default Featured Image'){
                    $this->addMenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName);

                }
                if ($pageTitle === 'Add New Images To Queue') {
                    $this->addMenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName);
                }
                if ($pageTitle === 'GF Image Editor') {
                    $this->addMenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName);
                }

                $this->addSubmenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName);
                if (file_exists($adminTemplateDir . $templateName . '/css/' . $templateName . '.css')){
                    WpEnqueue::addAdminStyle($templateName, $menuSlug, $adminTemplateDirUri . $templateName . '/css/' . $templateName . '.css');
                }
                if (file_exists($adminTemplateDir . $templateName . '/js/' . $templateName . '.js')){
                    WpEnqueue::addAdminScript($templateName, $menuSlug, $adminTemplateDirUri . $templateName . '/js/' . $templateName . '.js');
                }
            }
            foreach ($pluginInfo['registerSettings']['options'] as $optionName => $optionArgs) {
                $this->registerSetting($optionGroup, $optionName, $optionArgs);
            }

        }
        if (isset($pluginInfo['shortCode']) && $pluginInfo['shortCode'] != '') {
            ShortCodes::setupShortCode($scTemplateDir, $scTemplateDirUri, $pluginInfo['shortCode']);
        }

    }
    private function addMenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName)
    {
        $this->actionAdminMenu(function () use ($pageTitle, $menuTitle, $capability, $menuSlug, $templateName) {
            add_menu_page($pageTitle, $menuTitle, $capability, $menuSlug, function () use ($templateName) {
                $this->getTemplatePart('admin', $templateName);
            });

        });
        return $this;
    }

    private function addSubmenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName)
    {
        $this->actionAdminMenu(function () use ($pageTitle, $menuTitle, $capability, $menuSlug, $templateName) {
            add_submenu_page('theme_plugins', $pageTitle, $menuTitle, $capability, $menuSlug, function () use ($templateName) {
                $this->getTemplatePart('admin', $templateName);
            });
        });

        return $this;
    }

    /*
     * Page is empty because its not used its only used to create header menu for settings
     */
    private function addMainSettingPage()
    {
        $this->actionAdminMenu(function () {
            add_menu_page('Theme plugin settings', 'Theme plugin settings', 'manage_options', 'theme_plugins', null, null);
        });
    }

    private function getTemplatePart($section, $name)
    {
        require_once PLUGIN_DIR . '/templates/' . $section . '/' . $name . '/view/' . $name . '.php';
    }

    public static function getTemplatePartials($section, $pluginName, $fileName, $data = [])
    {
        require PLUGIN_DIR . '/templates/' . $section . '/' . $pluginName . '/view/' . $fileName . '.php';
    }

    private function registerSetting($optionGroup, $optionName, $optionArgs)
    {
        $this->actionAdminMenu(function () use ($optionGroup, $optionName, $optionArgs) {
            if (count($optionArgs) > 0) {
                register_setting($optionGroup, $optionName, $optionArgs);
            }
            register_setting($optionGroup, $optionName);
        });
    }

    private function setDefaultFeaturedImage(){
        $this->actionAdminMenu(function () {
            add_menu_page('Set Default Featured Image', 'Set Default Featured Image', 'manage_options', 'theme_plugins', '', null, null);
        });
    }
}

new GfShopThemePlugins();