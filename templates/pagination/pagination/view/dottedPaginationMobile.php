<?php
$page = (int) $data['page'];
$lastPage = (int) $data['lastPage'];
$firstPageClass = '';
$firstPageNextButton = '';
$lastPageClass = '';
$lastPagePreviousButton = '';
if ($page === 1) {
    $firstPageClass = 'firstPageMobile';
    $firstPageNextButton = 'firstPageNextMobile';
}
if ($page === $lastPage) {
    $lastPageClass = 'lastPageMobile';
    $lastPagePreviousButton = 'lastPagePreviousMobile';
}

?>

<ul>
    <?php if ($page !== 1): ?>
        <li class="prevButton <?= $lastPagePreviousButton ?>"><?= get_previous_posts_link('<i class="fas fa-arrow-left "></i>') ?></li>
    <?php endif; ?>
    <li class="activePageMobile active <?= $firstPageClass ?><?= $lastPageClass ?>"><?= $page ?></li>
    <?php if ($page !== $lastPage): ?>
        <li class="nextButton <?= $firstPageNextButton ?>"><a href="<?= get_pagenum_link($page + 1) ?>"><i class="fas fa-arrow-right "></i></a></li>
    <?php endif; ?>
</ul>
