<?php
$option = get_option('defaultFeaturedImage');
$imgId = $option ? : 0;
$buttonText = $imgId > 0 ? 'Izmenite sliku' : 'Ubacite sliku';
?>


<form method="post" action="options.php">
    <?php
    settings_fields('defaultFeaturedImages');
    ?>
    <h2>Set/Change Default Featured Image</h2>
    <div>
        <input id="defaultFeaturedImage" type="hidden" name="defaultFeaturedImage" value="<?= $imgId ?>" />
        <input id="imgUploadButton" type="button" class="button-primary" value="<?= $buttonText ?>" />
    </div>
    <div>
        <?php if ($imgId !== 0):?>
            <img id="defaultFeaturedImagePreview" src="<?= wp_get_attachment_image_url($imgId)?>" alt="blic naslovna">
        <?php endif; ?>
    </div>
    <?php submit_button() ?>
</form>