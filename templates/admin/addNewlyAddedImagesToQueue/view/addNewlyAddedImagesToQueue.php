<?php
$option = get_option('addNewlyAddedImagesToQueue');
?>
<h1>Do you want newly added images to be added to the image optimizer queue?</h1>
<form method="post" action="options.php">
    <?php settings_fields('addNewlyAddedImagesToQueues'); ?>
    <label for="yes">Yes</label>
    <input type="radio" id="yes" name="addNewlyAddedImagesToQueue" value="1" <?= $option === '1' ? 'checked' : '' ?>>
    <label for="no">No</label>
    <input type="radio" id="no" name="addNewlyAddedImagesToQueue" value="0"  <?= $option === '0' ? 'checked' : '' ?>>
    <?php submit_button() ?>
</form>
