<link rel="stylesheet" href="<?=PLUGIN_DIR_URI?>assets/js/cropSelect/crop-select-js.min.css">
<?php
$imageId = $_GET['imageId'] ?? null;
$redirectAfterBlur = admin_url('upload.php');
if ($imageId) {
    $imageInfo = wp_get_attachment_image_src($imageId,'full');
    $imageSrc = $imageInfo[0];
}
if($imageSrc === null) {
    echo '<h1>Slika nije pronađena</h1>';
}
?>
<h1 style="text-align:center;"><?=__('Izaberite deo slike koji želite da bude pikselizovan','gfImageEdit')?></h1>
<div id='crop-select'></div>
<input class="button-primary" type="button" id="blurImage" value="Sačuvaj">

<script>
    jQuery('#crop-select').CropSelectJs({
        imageSrc:'<?=$imageSrc?>'
    });
    jQuery('#blurImage').on('click',function(){
        // Loader
        let loader = '<div id="pixelateLoader"></div>';
        let cropContainer = jQuery('.crop-wrapper');
        if (jQuery('#pixelateLoader').val() === undefined) {
            cropContainer.append(loader);
            jQuery('.crop-image').css('opacity','0.3');
        }
        // Get the crop info
        let originalImageWidth = jQuery('#crop-select').CropSelectJs('getImageWidth');
        let clientWidth = document.getElementById('crop-select').clientWidth;
        let originalImageHeight = jQuery('#crop-select').CropSelectJs('getImageHeight');
        let clientHeight = document.getElementById('crop-select').clientHeight;
        let selectionWidth = jQuery('#crop-select').CropSelectJs('getSelectionBoxWidth')
        let selectionHeight = jQuery('#crop-select').CropSelectJs('getSelectionBoxHeight')
        let selectionX = jQuery('#crop-select').CropSelectJs('getSelectionBoxX')
        let selectionY = jQuery('#crop-select').CropSelectJs('getSelectionBoxY')
        /* If the image is greater than the container calculate the necessary things to make it the same,
        because we force it to a certain width */
        if (originalImageWidth > 1000) {
            let ratioDifferenceWidth = (originalImageWidth / clientWidth);
            let ratioDifferenceHeight = (originalImageHeight / clientHeight);

            selectionWidth = jQuery('#crop-select').CropSelectJs('getSelectionBoxWidth') * ratioDifferenceWidth;
            selectionHeight = jQuery('#crop-select').CropSelectJs('getSelectionBoxHeight') * ratioDifferenceHeight;
            selectionX = jQuery('#crop-select').CropSelectJs('getSelectionBoxX') * ratioDifferenceWidth;
            selectionY = jQuery('#crop-select').CropSelectJs('getSelectionBoxY') * ratioDifferenceHeight;
        }

        jQuery.ajax({
            url: '<?=admin_url('admin-ajax.php')?>',
            type: 'POST',
            data: {
                action: 'imageEdit',
                originalImageId:<?=$imageId?>,
                selectionX: selectionX,
                selectionY: selectionY,
                selectionWidth: selectionWidth,
                selectionHeight: selectionHeight
            },
            success: function(data) {
                // Print out the json error if there is one
                if (data.success === false) {
                    alert(data.data.message);
                    return;
                }
                // Redirect user to uploads.php
                window.location.href = "<?=$redirectAfterBlur?>";
            },
            error: function() {
                alert('Dogodila se neočekivana greška')
            },
        });
    })
</script>

