jQuery(document).ready(function($){
    let mediaUploader;
    jQuery('#imgUploadButton').click(function(e) {
        e.preventDefault();
        if (mediaUploader) {
            mediaUploader.open();
            return;
        }
        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Izaberite sliku',
            button: {
                text: 'Ubacite sliku'
            }, multiple: false });
        mediaUploader.on('select', function() {
            let attachment = mediaUploader.state().get('selection').first().toJSON();
            jQuery('#naslovnaImg').val(attachment.id);
            jQuery('#naslovnaPreview').attr('src',attachment.url);
        });
        mediaUploader.open();
    });
});