<?php
$isMobile = (int) wp_is_mobile();
use Carbon\Carbon; ?>
<div class="gray">
    <div class="container">
        <h3 class="box__title"><a title="<?=esc_attr($data['posts'][0]['categoryName'])?>" href="<?= $data['posts'][0]['categoryUrl'];
            ?>"><?= $data['posts'][0]['categoryName']; ?></a></h3>
        <section class="news news--over-img items__4">
        <?php foreach ($data['posts'] as $key => $post):
            Carbon::setLocale('bs');
            $postDate = new Carbon($post['publishDate'], new \DateTimeZone('Europe/Sarajevo'));
            $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());
            $postDateHtml = '<span class="postDate"><i class="fa fa-clock"></i> ' . $timeAgo[0] . ' ' . $timeAgo[1] . '</span>';
            if ($isMobile) {
                if ($key < 1) {
                    $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'large'));
                    $post['imageWidth'] = 374;
                    $post['imageHeight'] = 250;
                } else {
                    $postDateHtml = '<span style="color: grey;" class="postDate"><i class="fa fa-clock"></i> ' . $timeAgo[0] . ' ' . $timeAgo[1] . '</span>';
                    $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'medium'));
                    $post['imageWidth'] = 120;
                    $post['imageHeight'] = 82;
                }
            } else {
                $postDateHtml = '<span style="color: white;" class="postDate"><i class="fa fa-clock"></i> ' . $timeAgo[0] . ' ' . $timeAgo[1] . '</span>';
                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'large'));
                $post['imageWidth'] = 374;
                $post['imageHeight'] = 250;
            }
            $post['imageAltText'] = esc_attr(get_post_meta($post['imageId'], 'altText', true));
            if (!$post['imageAltText'] || $post['imageAltText'] === '') {
                $post['imageAltText'] = esc_attr($post['postTitle']);
            }
            echo \GfWpPluginContainer\Gutenberg\Blocks\CategoryShowcase\CategoryShowcase::renderArticle(get_permalink($post['postId']), $post['postTitle'], $post['imageUrl'], $post['imageWidth'],
                $post['imageHeight'], $post['postType'], $post['categoryUrl'], $post['categoryName'] , $postDateHtml, $post['imageAltText']);
            ?>
        <?php endforeach; ?>
        </section>
    </div>
</div>