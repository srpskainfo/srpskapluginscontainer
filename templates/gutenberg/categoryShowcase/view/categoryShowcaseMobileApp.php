<?php

use Carbon\Carbon;

?>
<div class="gray">
    <div class="container">
        <h3 class="box__title"><a href="<?=parseAppUrl('page',$data['posts'][0]['categoryUrl'])?>" title="<?=esc_attr($data['posts'][0]['categoryName'])?>"><?=$data['posts'][0]['categoryName']?></a></h3>
        <section class="news news--over-img items__4">
            <?php
            Carbon::setLocale('bs');
            foreach ($data['posts'] as $key => $post):
                $postDate = new Carbon($post['publishDate'], new \DateTimeZone('Europe/Sarajevo'));
                $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());
                $postDateHtml = '<span class="postDate"><i class="fa fa-clock"></i> ' . $timeAgo[0] . ' ' . $timeAgo[1] . '</span>';
                if ($key < 1) {
                    $postDateHtml = '<span style="color: white" class="postDate"><i class="fa fa-clock"></i> ' . $timeAgo[0] . ' ' . $timeAgo[1] . '</span>';
                    $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'large'));
                    $post['imageWidth'] = 374;
                    $post['imageHeight'] = 250;
                } else {
                    $postDateHtml = '<span style="color: grey" class="postDate"><i class="fa fa-clock"></i> ' . $timeAgo[0] . ' ' . $timeAgo[1] . '</span>';
                    $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'medium'));
                    $post['imageWidth'] = 120;
                    $post['imageHeight'] = 82;
                }
            ?>
                <article class="news__item">
                    <a href="<?=parseAppUrl('article',$post['url'])?>" title="<?=esc_attr($post['postTitle'])?>">
                        <figure>
                            <picture>
                                <source media="(max-width: 1023px)" srcset="<?= wp_get_attachment_image_srcset($post['imageId'], [$post['imageWidth'] ,$post['imageHeight']]) ?>">
                                <source media="(min-width: 1024px)" srcset="<?= wp_get_attachment_image_srcset($post['imageId'],[$post['imageWidth'], $post['imageHeight']]) ?>">

                                <img src="<?= $post['imageUrl'] ?>" height="<?=$post['imageHeight']?>" width="<?=$post['imageWidth']?>" alt="<?=esc_attr($post['postTitle'])?>" />
                                <?php if ($post['postType'] === 'video') : ?>
                                    <span class="icon"><i class="fas fa-play"></i></span>
                                <?php endif; ?>
                                <?php if ($post['postType'] === 'audio') : ?>
                                    <span class="icon"><i class="fas fa-microphone"></i></span>
                                <?php endif; ?>
                            </picture>
                        </figure>
                    </a>
                    <div>
                        <span class="news__category"><a href="<?=parseAppUrl('page',$post['categoryUrl'])?>" title="<?=$post['categoryName']?>"><?=$post['categoryName']?></a>
                        <?=$postDateHtml?>
                        </span>
                        <h2><a href="<?= parseAppUrl('article',$post['url'])?>" title="<?=$post['postTitle']?>"><?=$post['postTitle']?></a></h2>
                    </div>
                </article>
            <?php endforeach; ?>
        </section>
    </div>
</div>