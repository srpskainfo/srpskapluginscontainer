<?php use Carbon\Carbon;

if($data['selectedCategory'] != '-1') : ?>
<h3 class="box__title <?= $data['selectedCategory'] === '6' ? 'sportCategoryTitle' : '' ?>"><a href="<?=$data['posts'][0]['categoryUrl']; ?>"><?=($data['posts'][0]['categoryName']); ?></a></h3>
<?php endif; ?>
<section class="news news--over-img items__3 <?= $data['selectedCategory'] === '6' ? 'sportCategory' : '' ?>">
    <?php
    foreach($data['posts'] as $key => $post):
        $post['authorName']= $data['multipleAuthors']->getOwnersForPost($post['postId'])[0]->getAuthorDisplayName();
        $post['authorUrl'] = get_author_posts_url($data['multipleAuthors']->getOwnersForPost($post['postId'])[0]->getAuthorId());
        $urls = [
            'siprod.djavolak.info', 'srpska.greenfriends.systems', 'srpska.local'
        ];
        $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'list-small'));
        $post['imageWidth'] = 275;
        $post['imageHeight'] = 188;

        $post['imageAltText'] = esc_attr(get_post_meta($post['imageId'], 'altText', true));
        if (!$post['imageAltText'] || $post['imageAltText'] === '') {
            $post['imageAltText'] = esc_attr($post['postTitle']);
        }

        $authorId=get_post_field('post_author',$post['postId']);
        $post['authorName']= get_the_author_meta('display_name',$authorId);
        $post['authorUrl'] = get_author_posts_url($authorId);

        Carbon::setLocale('bs');
        $postDate = new Carbon($post['publishDate'], new \DateTimeZone('Europe/Sarajevo'));
        $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());
//        $post['imageUrl'] = str_replace($urls, 'srpskainfo.com', $post['imageUrl']);
    ?>
    <article class="news__item">
        <a title="<?=esc_attr($post['postTitle'])?>" href="<?=$post['url'] ?>">
            <figure>
                <picture>
                    <source media="(max-width: 1023px)" srcset="<?= wp_get_attachment_image_srcset($post['imageId'],[$post['imageWidth'], $post['imageHeight']]) ?>">
                    <source media="(min-width: 1024px)" srcset="<?= wp_get_attachment_image_srcset($post['imageId'],[$post['imageWidth'], $post['imageHeight']]) ?>">
                    <img alt="<?=$post['imageAltText']?>" src="<?= $post['imageUrl']?>" height="<?=$post['imageHeight']?>" width="<?=$post['imageWidth']?>">
                    <?php if ($post['postType'] === 'video') :?>
                        <span class="icon"><i class="fas fa-play"></i></span>
                    <?php endif; ?>
                    <?php if ($post['postType'] === 'audio') :?>
                        <span class="icon"><i class="fas fa-microphone"></i></span>
                    <?php endif; ?>
                </picture>
            </figure>
        </a>
        <?php $color = 'white';
        if (wp_is_mobile()){
            $color = $key < 1 ? 'white': 'grey';
        }
        ?>
        <div class="test <?= $data['selectedCategory'] === '6' ? 'sportCategoryName' : '' ?>">
            <span class="news__category">
                <span class="categoryAuthor">
                     <?php if (mb_strtoupper($data['posts'][0]['categoryName'])=='KOLUMNE' ) : ?>
                         <a class="categoryKolumneAuthor" title="<?=$post['authorName']?>" href="<?=$post['authorUrl']?>" ><?=$post['authorName'] ?></a>
                     <?php else : ?>
                         <a title="<?=$data['posts'][0]['categoryName']?>" class="categoryKolumneAuthor" href="<?=$data['posts'][0]['categoryUrl'] ?>"><?=$data['posts'][0]['categoryName'] ?></a>
                     <?php endif;?>
                </span>
                <span style="color: <?=$color?>" class="postDate"><i class="fa fa-clock"></i><?= $timeAgo[0]?> <?= $timeAgo[1] ?></span>
            </span>
            <h2><a title="<?=esc_attr($post['postTitle'])?>" href="<?=$post['url'] ?>"><?=$post['postTitle']?></a></h2>
        </div>
    </article>
    <?php endforeach; ?>
</section>