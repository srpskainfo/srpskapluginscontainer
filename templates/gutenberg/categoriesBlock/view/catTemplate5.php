<?php if($data['selectedCategory'] != '-1') : ?>
<h3 class="box__title <?= $data['selectedCategory'] === '6' ? 'sportCategoryTitle' : '' ?>"><a href="<?=$data['posts'][0]['categoryUrl']; ?>" title="<?=($data['posts'][0]['categoryName']); ?>"><?=($data['posts'][0]['categoryName']); ?></a></h3>
<?php endif; ?>
<section class="news items__3 <?= $data['selectedCategory'] === '6' ? 'sportCategory' : '' ?>">
    <?php
    foreach($data['posts'] as $post):
        $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'list-small'));
        $post['imageWidth'] = 275;
        $post['imageHeight'] = 188;
        $post['imageAltText'] = esc_attr(get_post_meta($post['imageId'], 'altText', true));
        if (!$post['imageAltText'] || $post['imageAltText'] === '') {
            $post['imageAltText'] = esc_attr($post['postTitle']);
        }
        include('articleItem.phtml');
    endforeach; ?>
</section>
