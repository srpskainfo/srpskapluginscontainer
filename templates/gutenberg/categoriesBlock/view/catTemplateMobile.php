<?php use Carbon\Carbon;
Carbon::setLocale('bs');

if($data['selectedCategory'] != '-1') :
	$catLink = $data['posts'][0]['categoryUrl'];
	$catLink = str_replace('/category','',$catLink);
	?>
    <h3 class="box__title <?= $data['selectedCategory'] === '6' ? 'sportCategoryTitle' : '' ?>">
        <a href="<?=$catLink?>" title="<?=esc_attr($post['categoryName'])?>"><?=($data['posts'][0]['categoryName']) ?></a>
    </h3>
<?php endif;
?>
<section class="news items__5 <?= $data['selectedCategory'] === '6' ? 'sportCategory' : '' ?>">
	<?php
	$i = 1;
	foreach($data['posts'] as $post):
        $post['authorName']= $data['multipleAuthors']->getOwnersForPost($post['postId'])[0]->getAuthorDisplayName();
        $post['authorUrl'] = get_author_posts_url($data['multipleAuthors']->getOwnersForPost($post['postId'])[0]->getAuthorId());
        $postDate = new Carbon($post['publishDate'], new \DateTimeZone('Europe/Sarajevo'));
        $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());

        $authorId=get_post_field('post_author',$post['postId']);
        $authorName= get_the_author_meta('display_name',$authorId);
        $authorUrl = get_author_posts_url($authorId);

		if ($data['selectedCategory'] === '-1') {
			$postCatLink = get_category_link($post['categoryId']);
			$post['categoryUrl'] = str_replace('/category','',$postCatLink);
		}
        $categories = get_the_category($post['postId']);
		foreach($categories as $category) {
            if($data['selectedCategory'] !== '6') {
                if($category->parent === 0) {
                    $post['categoryName'] = $category->name;
                    $post['categoryUrl'] = get_home_url() . '/' . $category->slug . '/';
                }
            } elseif($category->parent === 6) {
                $post['categoryName'] = $category->name;
                $post['categoryUrl'] = get_home_url() . '/' . $category->slug . '/';
            }

        }
        if ($i < 2 ) {
            $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'large'));
            $post['imageWidth'] = 374;
            $post['imageHeight'] = 250;
        } else {
//            $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId']), 'medium');
            $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'medium'));
            $post['imageWidth'] = 120;
            $post['imageHeight'] = 82;
        }
        $post['imageAltText'] = esc_attr(get_post_meta($post['imageId'], 'altText', true));
        if (!$post['imageAltText'] || $post['imageAltText'] === '') {
            $post['imageAltText'] = esc_attr($post['postTitle']);
        }
		?>
        <article class="news__item">
            <a href="<?=$post['url']?>" title="<?=esc_attr($post['postTitle'])?>">
                <figure>
                    <img src="<?= $post['imageUrl']?>" height="<?=$post['imageHeight']?>" width="<?=$post['imageWidth']?>" alt="<?= $post['imageAltText'] ?>" /><?php if ($post['postType'] === 'video') {
					    echo '<span class="icon"><i class="fa fa-play"></i></span>';
					} elseif ($post['postType'] === 'audio') {
					    echo '<span class="icon"><i class="fa fa-microphone"></i></span>';
					}?></figure>
            </a>
            <div class="<?= $data['selectedCategory'] === '6' ? 'sportCategoryName' : '' ?>">
                <span class="news__category">
                    <span class="categoryAuthor">
                          <?php if (mb_strtoupper($post['categoryName'])=='KOLUMNE' ) : ?>
                              <a  title="<?=$post['authorName']?>" href="<?=$post['authorUrl']?>"><?=$post['authorName']?></a>
                          <?php else : ?>
                              <a title="<?=$post['categoryName']?>" href="<?=$post['categoryUrl']?>"><?=$post['categoryName']?></a>
                          <?php endif;?>
                    </span>
                    <span class="postDate"><i class="fa fa-clock"></i> <?=$timeAgo[0]?> <?=$timeAgo[1]?></span>
                </span>
                <h1><a href="<?=$post['url']?>" title="<?=esc_attr($post['postTitle'])?>"><?=$post['postTitle'] ?></a></h1>
            </div>
        </article>
		<?php $i++; endforeach;?>
</section>
