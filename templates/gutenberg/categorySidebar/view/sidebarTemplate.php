<section>
    <?php if ($data['selectedCategory'] != '-1') : ?>
        <h3 class="box__title"><a href="<?= $data['posts'][0]['categoryUrl']; ?>"><?= ($data['posts'][0]['categoryName']); ?></a></h3>
    <?php endif ?>
    <?php if ($data['selectedCategory'] === '-1') : ?>
        <h3 class="box__title">Sve Kategorije</h3>
    <?php endif ?>
    <section class="news">
        <?php foreach ($data['posts'] as $post) {
            $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'medium'));
//            $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'list-small'));
            $post['imageWidth'] = 120;
            $post['imageHeight'] = 82;
            $post['imageAltText'] = esc_attr(get_post_meta($post['imageId'], 'altText', true));
            if (!$post['imageAltText'] || $post['imageAltText'] === '') {
                $post['imageAltText'] = esc_attr($post['postTitle']);
            }
            include(__DIR__ . '/../../categoriesBlock/view/articleItem.phtml');
        } ?>
    </section>
</section>
