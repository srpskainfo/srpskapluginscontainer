<section>
	<?php use Carbon\Carbon;

    if ( $data['selectedCategory'] != '-1' ) : ?>
        <h3 class="box__title">
            <a href="<?= $data['posts'][0]['categoryUrl']?>" title="<?=esc_attr($data['posts'][0]['categoryName'])?>"><?=$data['posts'][0]['categoryName']?></a>
        </h3>
	<?php endif ?>
	<?php if ( $data['selectedCategory'] === '-1' ) : ?>
        <h3 class="box__title">Sve Kategorije</h3>
	<?php endif ?>
    <section class="news">
		<?php foreach ( $data['posts'] as $key => $post ):
            $postDate = new Carbon($post['publishDate'], new \DateTimeZone('Europe/Sarajevo'));
            $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());

            $multipleAuthors = new \GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors();
            $authorId=get_post_field('post_author',$post['postId']);
            $authorName= $multipleAuthors->getOwnersForPost($post['postId'])[0]->getAuthorDisplayName();
            $authorUrl = get_author_posts_url($multipleAuthors->getOwnersForPost($post['postId'])[0]->getAuthorId());

//            if ($key === 0) {
//                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'large'));
//                $post['imageWidth'] = 374;
//                $post['imageHeight'] = 250;
//            } else {
                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'medium'));
                $post['imageWidth'] = 120;
                $post['imageHeight'] = 82;
//            }
            $post['imageAltText'] = esc_attr(get_post_meta($post['imageId'], 'altText', true));
            if (!$post['imageAltText'] || $post['imageAltText'] === '') {
                $post['imageAltText'] = esc_attr($post['postTitle']);
            }
        ?>
            <article class="news__item news__item--list testing">
                <a href="<?= $post['url'] ?>" title="<?=esc_attr($post['postTitle'])?>">
                    <figure>
                        <img src="<?= $post['imageUrl'] ?>" height="<?=$post['imageHeight']?>" width="<?=$post['imageWidth']?>" alt="<?=$post['imageAltText']?>" />
                    </figure>
                </a>
                <div>
                    <span class="news__category">
                        <span class="categoryAuthor">
                             <?php if (mb_strtoupper($post['categoryName'])=='KOLUMNE' ) : ?>
                                 <a  title="<?=$authorName?>" href="<?=$authorUrl?>"><?=$authorName?></a>
                             <?php else : ?>
                                 <a title="<?=$post['categoryName']?>" href="<?=$post['categoryUrl']?>"><?=$post['categoryName']?></a>
                             <?php endif;?>
                        </span>
                        <span class="postDate"><i class="fa fa-clock"></i> <?=$timeAgo[0]?> <?=$timeAgo[1]?></span>
                    </span>
                    <h2><a href="<?=$post['url']?>" title="<?=esc_attr($post['postTitle'])?>"><?= $post['postTitle'] ?></a></h2>
                </div>
            </article>
		<?php endforeach ?>
    </section>
</section>
