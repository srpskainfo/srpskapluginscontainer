<section>
    <?php use Carbon\Carbon;
    Carbon::setLocale('bs');
    if ($data['selectedCategory'] != '-1') : ?>
        <h3 class="box__title"><a href="<?=parseAppUrl('page',$data['posts'][0]['categoryUrl']) ?>" title="<?= esc_attr($data['posts'][0]['categoryUrl']) ?>"><?= ($data['posts'][0]['categoryName']) ?></a></h3>
    <?php endif ?>
    <?php if ($data['selectedCategory'] === '-1') : ?>
        <h3 class="box__title">Sve Kategorije</h3>
    <?php endif ?>
    <section class="news">
        <?php foreach ($data['posts'] as $key => $post):
            $postDate = new Carbon($post['publishDate'], new \DateTimeZone('Europe/Sarajevo'));
            $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());

            $multipleAuthors = new \GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors();
            $authorName= $multipleAuthors->getOwnersForPost($post['postId'])[0]->getAuthorDisplayName();
            $authorUrl = get_author_posts_url($multipleAuthors->getOwnersForPost($post['postId'])[0]->getAuthorId());

            $post['url'] = parseAppUrl('article',$post['url']);
//            if ($key === 0) {
//                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'large'));
//                $post['imageWidth'] = 374;
//                $post['imageHeight'] = 250;
//            } else {
                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($post['imageId'], 'medium'));
                $post['imageWidth'] = 120;
                $post['imageHeight'] = 82;
//            }
        ?>
        <article class="news__item news__item--list">
            <a href="<?= $post['url'] ?>" title="<?=esc_attr($post['postTitle'])?>">
                <figure>
                    <img src="<?= $post['imageUrl'] ?>" height="<?=$post['imageHeight']?>" width="<?=$post['imageWidth']?>" alt="<?=esc_attr($post['postTitle'])?>" />
                </figure>
            </a>
            <div>
                <span class="news__category">
                    <span class="categoryAuthor">
                         <?php if (mb_strtoupper($post['categoryName'])=='KOLUMNE' ) : ?>
                             <a  title="<?=$authorName?>" href="<?=parseAppUrl('author',$authorUrl)?>"><?=$authorName?></a>
                         <?php else : ?>
                             <a title="<?=$post['categoryName']?>" href="<?=parseAppUrl('page', $post['categoryUrl'])?>"><?=$post['categoryName']?></a>
                         <?php endif;?>
                    </span>
                    <span class="postDate"><i class="fa fa-clock"></i> <?=$timeAgo[0]?> <?=$timeAgo[1]?></span>
                </span>
                <h2><a href="<?=$post['url']?>" title="<?=esc_attr($post['postTitle'])?>"><?= $post['postTitle'] ?></a></h2>
            </div>
        </article>
        <?php endforeach ?>
    </section>
</section>
