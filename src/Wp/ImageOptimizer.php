<?php

namespace GfWpPluginContainer\Wp;

class ImageOptimizer
{
    private $wpdb;
    private $tableName;
    private $queueTableName;
    private $itemsFromQueuePerRun = 500;
    private $itemsToQueuePerRun = 50;

    public function __construct(\wpdb $wpdb)
    {
        $this->wpdb = $wpdb;
        $this->tableName = $wpdb->prefix . 'gfImageOptimizer';
        $this->queueTableName = $wpdb->prefix . 'gfImageOptimizerQueue';
    }

    public function syncImagesFromMediaLibrary()
    {
        foreach ($this->getImagesFromLibrary() as $item) {
            $this->saveAttachmentToQueue($item->ID);
        }
    }

    /**
     * add new item to queue
     */
    public function saveAttachmentToQueue($id)
    {
        $orgImagePath = mb_strstr(wp_get_original_image_path($id), 'wp-content');
        if (!$this->wpdb->insert($this->queueTableName, ['postId' => $id, 'path' => $orgImagePath])) {
            $this->log("could not add postId {$id} to queue");
        }
    }

    /**
     * Fetch n items from queue and process them
     */
    public function processQueue()
    {
        $items = $this->wpdb->get_results("SELECT * FROM {$this->queueTableName} ORDER BY queueId LIMIT {$this->itemsFromQueuePerRun}");
        foreach ($items as $item) {
            if (substr($item->path,-4) === '.svg') {
                continue;
            }
            $this->processImage($item->path);
//            die('brejk');
            $this->addToImageOptimizerTable($item);
            $this->removeFromQueue($item->queueId);
        }
    }

    /**
     * Goes through all images sizes for given image
     */
    public function processImage($imagePath)
    {
//        var_dump($imagePath);
        foreach (wp_get_additional_image_sizes() as $size) {
            try {
                $this->optimize($imagePath, $size);
            } catch (\Exception $e) {
                if ($e->getMessage() === 'image type not supported.') {
                    continue;
                }
                throw $e;
            }
        }
    }

    /**
     *  Generates a more optimized version of given image
     */
    public function optimize($path, $size)
    {
        try {
            $image = $this->getBestFitCrop($path, $size);
        } catch (\Exception $e) {
            return;
        }
//        echo sprintf('saved crop %dx%d for %dx%d', $size['width'], $size['height'], $width, $height) . PHP_EOL;
        $savePath = dirname($path);
        $filename = explode('.', basename($path))[0];
//        $filename = sprintf('%s-%dx%d-copy', $filename, $size['width'], $size['height']);
        $filename = sprintf('%s-%dx%d', $filename, $size['width'], $size['height']);
        switch ($image->getImageMimeType()) {
            case 'image/jpeg':
                $filename .= '.jpg';
                break;
            case 'image/png':
                $filename .= '.png';
                break;
            default:
                $this->log('image type not supported.: ' . $image->getImageMimeType());
                return;
        }
        if (!$image->writeImage($savePath .'/'. $filename)) {
            $this->log('failed to write image: ' . $savePath .'/'. $filename);
        }
    }

    /**
     * Fetches images from media library.
     *
     * @return array|null|object
     */
    private function getImagesFromLibrary()
    {
        if ($this->wpdb->get_var("SELECT COUNT(*) FROM wp_gfImageOptimizer") == 0) {
            $sql = "SELECT ID FROM wp_posts WHERE post_type = 'attachment' ORDER BY ID desc LIMIT {$this->itemsToQueuePerRun}";
        } else {
            $sql = "SELECT ID FROM wp_posts WHERE post_type = 'attachment' 
              #AND ID > (SELECT MAX(postId) FROM wp_gfImageOptimizer)
              AND ID NOT IN (SELECT postId FROM wp_gfImageOptimizer)
              AND ID NOT IN (SELECT postId FROM wp_gfImageOptimizerQueue)
              ORDER BY ID desc LIMIT {$this->itemsToQueuePerRun}";
        }

        return $this->wpdb->get_results($sql);
    }

    /**
     * Save item to optimized images log
     * @param $id
     */
    private function addToImageOptimizerTable($item)
    {
        if (!$this->wpdb->insert($this->tableName, ['postId' => $item->postId])) {
            $this->removeFromQueue($item->queueId);
            $this->log("Could not add postId {$item->postId} to imageOptimizerTable.");
        }
    }

    /**
     * Remove item from queue storage.
     * @param $queueId
     */
    private function removeFromQueue($queueId)
    {
        $this->wpdb->delete($this->queueTableName, ['queueId' => $queueId]);
    }

    /**
     * Tries to calculate best possible image for a given image and crop size.
     *
     * @param \Imagick $image
     * @param int $width
     * @param int $height
     * @param array $size
     * @return \Imagick
     * @throws \Exception
     */
    private function getBestFitCrop($path, $size)
    {
        $path = __DIR__ . '/../../../../../' . $path;
        $image = new \Imagick($path);
        $width = $image->getImageWidth();
        $height = $image->getImageHeight();
        $imgRatio = $width / $height;
        $cropRatio = $size['width'] / $size['height'];
        if ($height < $size['height'] || $width < $size['width']) {
            throw new \Exception('image too small');
        }
//        $maxSize = min($width, $height);
        if ($width > $height) { // landscape, cube
            if ($cropRatio >= $imgRatio) {
                $maxWidth = $width;
                $maxHeight = ceil($size['height'] * ($width / $size['width'])); // img width divided by crop width for resize ratio
                $hOffset = floor(($height - $maxHeight) / 2);
                $wOffset = 0;
            } else {
                $maxHeight = $height;
                $maxWidth = ceil($size['width'] * ($height / $size['height']));
                $wOffset = floor(($width - $maxWidth) / 2);
                $hOffset = 0;
            }
        } else { // portrait
            if ($cropRatio > $imgRatio) {
                $maxWidth = $width;
                $maxHeight = ceil($size['height'] * ($width / $size['width']));
                $hOffset = floor(($height - $maxHeight) / 2);
                $wOffset = 0;
            } else {
                $maxHeight = $height;
                $maxWidth = ceil($size['width'] * ($height / $size['height']));
                $wOffset = floor(($width - $maxWidth) / 2);
                $hOffset = 0;
            }
        }

        try {
            $image->setImageCompression(\Imagick::COMPRESSION_LOSSLESSJPEG);
            $image->setImageCompressionQuality(60);
            $image->cropImage($maxWidth, $maxHeight, $wOffset, $hOffset);
//        $image->resizeImage($size['width'], $size['height'], \Imagick::FILTER_TRIANGLE, 1);
            $image->resizeImage($size['width'], $size['height'], \Imagick::FILTER_CATROM, 1);
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            $msg = '%s path: %s maxWidth: %s maxHeight %s hOffset %s woffset %s';
            $msg = sprintf($msg, $path, $e->getMessage(), $maxWidth, $maxHeight, $hOffset, $wOffset);
//            var_dump($maxWidth);
//            var_dump($maxHeight);
//            var_dump($hOffset);
//            var_dump($wOffset);
            $this->log($msg);
            throw new \Exception('image not optimized');
        }
        return $image;
    }

    private function log($msg)
    {
        $path = __DIR__ . '/../../../../imageOptimizerError.log';
        $msg = date('Y-m-d') . ' --- ' . $msg . PHP_EOL;
        file_put_contents($path, $msg, FILE_APPEND);
    }

    public function undoWatermarkImage($postId)
    {
        $post = get_post($postId);
        $path = mb_strstr($post->guid, 'uploads');
        $extension = substr(basename($path), -4);
        $filename = substr(basename($path), 0, -4);
        $path = __DIR__ . '/../../../../' . dirname($path) . '/';
        copy($path . $filename ."-original". $extension, $path . $filename . $extension);
        $fullsizepath = get_attached_file($post->ID);
        wp_update_attachment_metadata($post->ID, wp_generate_attachment_metadata($post->ID, $fullsizepath));
    }

    public function watermarkImage($imageArray, $originalImageId)
    {

            $watermarkImagePath = __DIR__ . '/../../../../uploads/2021/03/srpska-info-logo-BELO-30-posto-1.png';
            $wmImage = new \Imagick($watermarkImagePath);
            $path = mb_strstr($imageArray['guid'], 'uploads');
            $extension = substr(basename($path), -4);
            $filename = substr(basename($path), 0, -4);
            $path = __DIR__ . '/../../../../' . dirname($path) . '/';
            $image = new \Imagick($path . $filename . $extension);
            $wCropImageRatio = $wmImage->getImageWidth() / $image->getImageWidth();

            if ($wCropImageRatio > 0.8) {
                $resizeRatio = 1 - ($wCropImageRatio - 0.8);
                $wmImage->scaleImage($resizeRatio * $wmImage->getImageWidth(),
                    $resizeRatio * $wmImage->getImageHeight());
            }
        if ($wCropImageRatio < 0.3 && $image->getImageWidth() > $wmImage->getImageWidth()) {
                $resizeRatio = 2.5;
                $wmImage->scaleImage($resizeRatio * $wmImage->getImageWidth(),
                    $resizeRatio * $wmImage->getImageHeight());
            }
            $wOffset = ($image->getImageWidth() - $wmImage->getImageWidth()) / 2;
            $hOffset = ($image->getImageHeight() - $wmImage->getImageHeight()) / 2;
            $image->compositeimage($wmImage, \Imagick::COMPOSITE_OVER, $wOffset, $hOffset);
            $image->writeImage($path . $filename . $extension);

            $newPath = wp_get_upload_dir()['path'] . '/';
            $newImageName = $filename . $extension;
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            $attachmentId = wp_insert_attachment($imageArray,$newPath . $newImageName);
            $attachData = wp_generate_attachment_metadata( $attachmentId, $newPath . $newImageName );
            wp_update_attachment_metadata( $attachmentId, $attachData );
            // Use the original image caption (we use it as author) and use it for the new image
            $this->updatePostCaptionByAttachmentId($attachmentId,$imageArray, $originalImageId);
    }

    public function makeImageDuplicate($postId)
    {
        $image = get_post($postId);
        $image = $image->to_array();
        $oldImagePath =  wp_get_original_image_path($postId);
        $oldImageUrl = $image['guid'];
        $imageInfo = pathinfo($oldImagePath);
        $extension = $imageInfo['extension'];
        $newPath = wp_get_upload_dir()['path'] . '/';
        $newImageName = $imageInfo['filename'] . '-watermarked.' . $extension;
        copy($oldImageUrl,$newPath . $newImageName);

        $image['ID'] = '';
        $image['guid'] = wp_get_upload_dir()['url'] . '/' . $newImageName;

        return $image;

    }
    private function updatePostCaptionByAttachmentId($attachmentId,$imageData, $originalImageId)
    {
        $altText = get_post_meta($originalImageId, 'altText', true);
        $args = [
            'ID' => $attachmentId,
            'post_excerpt' => $imageData['post_excerpt']
        ];
        wp_update_post($args);
        update_post_meta($attachmentId, 'altText', $altText ?? '');
    }
}


// wp_generate_attachment_metadata


$sql = "CREATE TABLE `wp_gfImageOptimizer` (
  `imageId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `postId` int(10) unsigned NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`imageId`),
  UNIQUE KEY `postId_UNIQUE` (`postId`)
) ENGINE=InnoDB;
";

$sql2 = "CREATE TABLE `wp_gfImageOptimizerQueue` (
  `queueId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `postId` int(10) unsigned NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`queueId`),
  UNIQUE KEY `postId_UNIQUE` (`postId`)
) ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
