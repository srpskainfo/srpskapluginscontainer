<?php

namespace GfWpPluginContainer\Wp\AklamatorRss\Repository;
class AklamatorRss
{
    /**
     * @var \wpdb
     */
    private $db;
    /**
     * @var string
     */
    private $tableName;

    /**
     * Repo constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db;
        $this->tableName = $this->db->prefix . 'aklamatorRssFeedPosts';
    }

    /**
     * Insert a post
     * @param $data
     */
    public function insert($data): void
    {
        $sql = "INSERT INTO {$this->tableName} (`postId`, `title`, `author`,`url`,`publishedAt`,`category`,`tags`,`featuredImage`,`position`)
            VALUES (
                    {$data['postId']},
                    '{$data['title']}',
                    '{$data['author']}',
                    '{$data['url']}',
                    '{$data['publishedAt']}',
                    '{$data['category']}',
                    '{$data['tags']}',
                    '{$data['featuredImage']}',
                    {$data['position']}
                    )";
        $this->db->query($sql);
    }


    /**
     * Deletes all the posts
     */
    public function deleteAll(): void
    {
        $sql = "DELETE FROM {$this->tableName}";
        $this->db->query($sql);
    }

    /**
     * Returns the posts in the DB ordered by position
     * @return array|object
     */
    public function fetchAklamatorPosts($filter)
    {
        $sql = "SELECT * FROM {$this->tableName} ORDER BY position {$filter}";
        return $this->db->get_results($sql, ARRAY_A) ?? [];
    }

    /**
     * Retrieve the number of posts in the DB
     * @return string|null
     */
    public function countAklamatorPosts(): ?string
    {
        return $this->db->get_var("SELECT COUNT(*) FROM {$this->tableName}");
    }

    /**
     * Retrieves the last position in the list of posts, if no posts are present returns -1
     * @return int|string
     */
    public function getLastPosition()
    {
        return $this->db->get_var("SELECT MAX(`position`) FROM wp_aklamatorRssFeedPosts") ?? -1;
    }

    /**
     * Count posts by ID. Can be used to see if the post exists.
     * @param $postId
     * @return string|null
     */
    public function countAklamatorPostsById($postId): ?string
    {
        return $this->db->get_var("SELECT COUNT(*) FROM {$this->tableName} WHERE `postId` = {$postId}");
    }

    /**
     * Fetch the position for the given post ID
     * @param $postId
     * @return string|null
     */
    public function getAklamatorPostPositionByPostId($postId): ?string
    {
        return $this->db->get_var("SELECT `position` FROM {$this->tableName} WHERE `postId` = {$postId}");
    }

    /**
     * Delete a post by it's ID
     * @param $postId
     */
    public function deleteAklamatorPostByPostId($postId): void
    {
        $sql = "DELETE FROM {$this->tableName} WHERE `postId` = {$postId}";
        $this->db->query($sql);
    }

    /**
     * Deletes the oldest (position 0) aklamator post and returns it's ID
     * @return string|null
     */
    public function deleteOldestAklamatorPost()
    {
        $sql = "DELETE FROM {$this->tableName} WHERE `position` = 0";
        $this->db->query($sql);
    }

    /**
     * Gets the first post. The first post is the one with the 0 position
     * @return array|object
     */
    public function getOldestAklamatorPostId()
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE `position` = 0";
        return $this->db->get_results($sql, ARRAY_A) ?? [];
    }

    /**
     * Reindex all the positions based on the deleted position.
     * Every post with the higher position than the deleted one gets it's position decremented by 1
     * @param $position
     */
    public function reIndexPositionsByDeletedPosition($position): void
    {
        $sql = "UPDATE {$this->tableName}
                SET `position` = `position`-1
                WHERE `position` > {$position}";
        $this->db->query($sql);
    }

    /**
     * Decrement all the positions by 1
     */
    public function decrementPositions()
    {
        $sql = "UPDATE {$this->tableName}
                SET `position` = `position`-1";
        $this->db->query($sql);
    }

    /**
     * Fetches all the post IDs
     * @return array|object
     */
    public function getAklamatorPostIds()
    {
        $sql = "SELECT `postId` FROM {$this->tableName}";
        return $this->db->get_results($sql, ARRAY_N) ?? [];
    }
}