<?php

namespace GfWpPluginContainer\Wp\AklamatorRss;
use GfWpPluginContainer\Wp\AklamatorRss\Service\PostDataHandler;
use GfWpPluginContainer\Wp\AklamatorRss\Service\Setup;
use GfWpPluginContainer\Wp\PostHelper;

class AklamatorRss
{
    private $setup;

    private $repo;

    private $postDataHandler;

    private const MAXIMUM_NUMBER_OF_AKLAMATOR_POSTS = 30;

    private const MAXIMUM_POSITION_INDEX_AKLAMATOR_POST = self::MAXIMUM_NUMBER_OF_AKLAMATOR_POSTS -1;

    private const acceptedCategoriesForAutoPopulate = [
        'magazin',
        'zabava'
    ];


    public function init()
    {
        $this->setup = new Setup();
        $this->setup->setup();
        $this->hooksAndFilters();
        global $wpdb;
        $this->repo = new Repository\AklamatorRss($wpdb);
        $this->postDataHandler = new PostDataHandler();
    }

    private function hooksAndFilters()
    {
        add_action('wp_ajax_saveAklamatorPosts', [$this, 'saveData']);
        add_action('wp_ajax_populateNewPost', [$this, 'populateNewPostInForm']);
        add_filter('manage_posts_columns', [$this, 'addAklamatorPostAdd']);
        add_action('manage_posts_custom_column', [$this, 'addIsAklamatorPostValue'], 10, 2);
        add_action('wp_ajax_handleAklamatorOnButtonClick',[$this,'handleAklamatorOnButtonClick']);
        add_action('transition_post_status',[$this,'setPostToAklamatorOnPublish'],10, 3);
    }

    public function setPostToAklamatorOnPublish($newStatus,$oldStatus, $post)
    {
        $categorySlug = get_the_category($post->ID)[0]->slug;
        if (in_array($categorySlug, self::acceptedCategoriesForAutoPopulate, true)) {
            if ($newStatus === 'future') {
                global $cache;
                $postDate = $post->post_date;
                $futureAklamatorPosts = unserialize($cache->get('futureAklamatorPosts'));
                $futureAklamatorPosts[$post->ID] = $postDate;
                $cache->set('futureAklamatorPosts', serialize($futureAklamatorPosts), 0);
            }

            if ($newStatus === 'publish' && $oldStatus !== 'future') {
                $this->handleAklamatorOnButtonClick($post->ID, 'add', null, false);
            }
        }
    }

    /**
     * Saves the data through the ADMIN form
     */
    public function saveData(): void
    {
        // Get the amount of posts the user chose
        $postCount = count($_POST['posts']);
        // Fetch the ids of the posts that the user chose so we exclude later when fetching additional posts
        $restrictedPosts = $this->postDataHandler->getRestrictedPosts($_POST['posts']);
        // If there are less than MAX_NUM_OF_AKLAMATOR_POSTS from the user get additional posts and push them into the posts array
        if($postCount < self::MAXIMUM_NUMBER_OF_AKLAMATOR_POSTS) {
            $additionalPosts = get_posts(['numberposts' => self::MAXIMUM_NUMBER_OF_AKLAMATOR_POSTS-$postCount, 'post_status' => 'publish', 'exclude' => $restrictedPosts]);
            // Position is set as the count of the posts, increment this position for each additional post
            $position = self::MAXIMUM_POSITION_INDEX_AKLAMATOR_POST-$postCount;
            foreach($additionalPosts as $additionalPost) {
                $_POST['posts'][] = [(string)$additionalPost->ID, (string)$position];
                $position--;
            }
        }

        $this->repo->deleteAll();
        $cacheData = [];
        if(isset($_POST['posts'])) {
            foreach ($_POST['posts'] as $post) {
                $data = $this->postDataHandler->handlePostData($post[0], $post[1]);
                $cacheData[] = $data;
                $this->repo->insert($data);
            }
        }
        global $cache;
        $cache->set('aklamatorPosts',serialize($cacheData),0);
        $this->setLastModifiedDateCache();
        wp_send_json_success('Update complete');
    }

    /**
     * Fetches the post based on the ID in the admin form input
     */
    public function populateNewPostInForm(): void
    {
        $post = get_post($_POST['id']);
        if($post) {
            $data = [
                'postImageUrl' => get_the_post_thumbnail_url($_POST['id']),
                'postTitle' => $post->post_title,
                'postStatus' => $post->post_status
            ];
            wp_send_json_success($data);
        } else {
            wp_send_json_error('No posts');
        }
    }

    /**
     *  Adds a new column to the post list
     * @param $defaults
     * @return mixed
     */
    public function addAklamatorPostAdd($defaults)
    {
        if(checkForLoggedInUserRole('editor') || checkForLoggedInUserRole('administrator')) {
            $defaults['aklamatorPost'] = 'Aklamator post';
            return $defaults;
        }
        return $defaults;
    }

    /**
     * Populates the custom aklamator column in the post list based on whether the post ID matches any record in the DB
     * @param $column_name
     * @param $postId
     */
    public function addIsAklamatorPostValue($column_name, $postId): void
    {
        if(checkForLoggedInUserRole('editor') || checkForLoggedInUserRole('administrator')) {
            $postStatus = get_post_status($postId);
            if ($column_name === 'aklamatorPost' && $postStatus === 'publish') {
                $aklamatorButton = new \GfWpPluginContainer\Wp\AklamatorRss\Service\AklamatorRssButton();
                echo $aklamatorButton->generateAklamatorButton($postId);
            }
        }
    }

    /**
     * Handle clicking on add / remove post to the aklamator list from the post list page
     */
    public function handleAklamatorOnButtonClick($postId = null, $action = null, $cache = null, $ajax = true): void
    {
        if(!$cache) {
            global $cache;
        }
        if(!$postId) {
            $postId = $_POST['postId'] ?? '';
        }
        if(!$action) {
            $action = $_POST['buttonAction'] ?? '';
        }
        $countPostsInDb = $this->repo->countAklamatorPosts();
        // If there is room for new posts check for duplicate entries, then add the post with a new position
        if ($countPostsInDb < self::MAXIMUM_NUMBER_OF_AKLAMATOR_POSTS && $action === 'add') {
            if($this->repo->countAklamatorPostsById($postId) > 0) {
                if($ajax) {
                    wp_send_json_error('Duplicate Entry');
                }
                return;
            }
            $this->repo->decrementPositions();
            $this->insertNewAklamatorPost($postId,$cache);
            // If there are still less than MAX_NUM_OF_AKLAMATOR_POSTS after adding the new post fetch newest articles to populate the posts
            if($countPostsInDb+1 < self::MAXIMUM_NUMBER_OF_AKLAMATOR_POSTS) {
                $posts = [];
                $cachedData = unserialize($cache->get('aklamatorPosts'));
                // Get restricted post IDS from the db
                $restrictedPosts = $this->postDataHandler->getRestrictedPosts($this->repo->getAklamatorPostIds());
                // Number of additional posts after adding 1 because the user inserted a post
                $numberOfAdditionalPosts = self::MAXIMUM_NUMBER_OF_AKLAMATOR_POSTS-($countPostsInDb+1);
                $additionalPosts = get_posts(['numberposts' => $numberOfAdditionalPosts, 'post_status' => 'publish', 'exclude' => $restrictedPosts]);
                // Position for the post
                $position = self::MAXIMUM_POSITION_INDEX_AKLAMATOR_POST-($countPostsInDb+1);
                // Additional post ids used to send the json response, to change the - signs to + for these posts
                $additionalPostIds = [];
                // Format the array
                foreach($additionalPosts as $additionalPost) {
                    $posts[] = [(string)$additionalPost->ID, (string)$position];
                    $additionalPostIds[] = $additionalPost->ID;
                    $position--;
                }
                // Insert into DB
                foreach ($posts as $post) {
                    $data = $this->postDataHandler->handlePostData($post[0], $post[1]);
                    $cachedData[] = $data;
                    $this->repo->insert($data);
                }

                // Set the cache
                $cache->set('aklamatorPosts',serialize($cachedData),0);
                $this->setLastModifiedDateCache($cache);
                if($ajax) {
                    wp_send_json_success([
                        'message' => 'Post and additional posts inserted',
                        'additionalPosts' => $additionalPostIds
                    ]);
                }
                return;
            }
            $this->setLastModifiedDateCache($cache);
            if($ajax) {
                wp_send_json_success('Post Inserted');
            }
            return;
        }
        /* If there is a maximum number of posts and the user wants to add a new one,
        delete the first one from the DB and add the new one */
        if($countPostsInDb >= self::MAXIMUM_NUMBER_OF_AKLAMATOR_POSTS && $action === 'add') {
            $cachedData = unserialize($cache->get('aklamatorPosts'));
            unset($cachedData[self::MAXIMUM_POSITION_INDEX_AKLAMATOR_POST]);
            $cachedData = array_values($cachedData);
            $cache->set('aklamatorPosts',serialize($cachedData),0);
            // The oldest post is the one with the 0 position
            $position = 0;
            $oldestPostId = $this->repo->getOldestAklamatorPostId();
            $this->repo->deleteOldestAklamatorPost();
            $this->repo->reIndexPositionsByDeletedPosition($position);
            $this->insertNewAklamatorPost($postId,$cache);
            $this->setLastModifiedDateCache($cache);
            if($ajax) {
                wp_send_json_success(['message' => 'FIFO insert completed','deletedPostId'=> $oldestPostId]);
            }
            return;
        }
        // Delete the aklamator post from the DB and reindex the positions of other posts, add additional so that there are 12 in the DB
        if($action === 'delete') {
            $position = $this->repo->getAklamatorPostPositionByPostId($postId);
            $this->repo->deleteAklamatorPostByPostId($postId);
            $this->repo->reIndexPositionsByDeletedPosition($position);
            // Add 1 additional post to make it 12 after deleting one
            $restrictedPosts = $this->postDataHandler->getRestrictedPosts($this->repo->getAklamatorPostIds());
            $additionalPosts = get_posts(['numberposts' => 1, 'post_status' => 'publish', 'exclude' => $restrictedPosts]);
            $data = $this->postDataHandler->handlePostData($additionalPosts[0]->ID, self::MAXIMUM_POSITION_INDEX_AKLAMATOR_POST);
            $this->repo->insert($data);
            $cachedData = unserialize($cache->get('aklamatorPosts'));
            foreach($cachedData as $key => $postData) {
                if((int)$postData['postId'] === (int)$postId) {
                    unset($cachedData[$key]);
                    break;
                }
            }
            $cachedData = array_values($cachedData);
            array_unshift($cachedData,$data);
            $cache->set('aklamatorPosts',serialize($cachedData),0);
            $this->setLastModifiedDateCache();
            if($ajax) {
                wp_send_json_success(['message' => 'Post Deleted', 'additionalPost' => $additionalPosts[0]->ID]);
            }
        }
    }

    private function insertNewAklamatorPost($postId,$cache): void
    {
        $data = $this->postDataHandler->handlePostData($postId, self::MAXIMUM_POSITION_INDEX_AKLAMATOR_POST);
        $this->repo->insert($data);
        $cachedData = unserialize($cache->get('aklamatorPosts'));
        array_unshift($cachedData,$data);
        $cache->set('aklamatorPosts',serialize($cachedData),0);
    }

    private function setLastModifiedDateCache($cache = null): void
    {
        if(!$cache) {
            global $cache;
        }
        $lastModified = date('D, d M Y H:i:s +0000');
        $cache->set('lastModifiedAklamator',$lastModified,0);
    }

}