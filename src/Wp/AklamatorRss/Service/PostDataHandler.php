<?php


namespace GfWpPluginContainer\Wp\AklamatorRss\Service;

use GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors;

class PostDataHandler
{
    public function handlePostData($postId, $position)
    {
        $multipleAuthors = new MultipleAuthors();
        $postOwners = $multipleAuthors->getOwnersForPost($postId)[0];
        $post = get_post($postId);
        $tags = [];
        $category = get_the_category($postId);
        foreach(get_the_tags($postId) as $tag) {
            $tags[] = $tag->name;
        }
        return [
            'postId' => $postId,
            'title' => $post->post_title,
            'author' => $postOwners->getAuthorDisplayName(),
            'url' => get_permalink($postId),
            'publishedAt' => $post->post_date,
            'category' => $category[0]->name,
            'tags' => serialize($tags),
            'featuredImage' => get_the_post_thumbnail_url($postId,'large'),
            'position' => $position
        ];
    }

    public function getRestrictedPosts($posts)
    {
        foreach($posts as $post) {
            $restrictedPosts[] = (int)$post[0];
        }
        return $restrictedPosts;
    }
}