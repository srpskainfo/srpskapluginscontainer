<?php


namespace GfWpPluginContainer\Wp\AklamatorRss\Service;


use GfWpPluginContainer\Wp\AklamatorRss\Repository\AklamatorRss;

class AklamatorRssButton
{
    /**
     * @var AklamatorRss
     */
    private $repo;

    public function __construct()
    {
        global $wpdb;
        $this->repo = new AklamatorRss($wpdb);
    }

    /**
     * Returns the HTML for the aklamator is/isn't "button" depending on whether the post ID matches any record in the DB
     * @param $postId
     * @return string
     */
    public function generateAklamatorButton($postId): string
    {
        $addOrDelete = 'add';
        $button = '<i style="color:#2271b1; font-size:15px;" class="fas fa-plus"></i>';
        if($this->repo->countAklamatorPostsById($postId) > 0) {
            $button = '<i style="color:red; font-size:15px;" class="fas fa-minus"></i>';
            $addOrDelete = 'delete';
        }
        return '<div class="aklButton" style="width:max-content; cursor:pointer;" data-id="' . $postId .'" data-addordelete="' . $addOrDelete .'">' . $button .'</div>';
    }
}