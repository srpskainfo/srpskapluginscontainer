<?php

namespace GfWpPluginContainer\Wp\AklamatorRss\Service;

use GfWpPluginContainer\Wp\AklamatorRss\Repository\AklamatorRss;
use GfWpPluginContainer\Wp\WpEnqueue;

class Setup
{
    public $adminSlug;
    public $adminPageUrl;
    public function setup()
    {
        $this->hooksAndFilters();
        $this->enqueueJs();
        $this->enqueueCss();
        $this->createAklamatorRssTable();
        $this->adminSlug = 'aklamator-rss';
        $this->adminPageUrl = admin_url() . 'admin.php?page=' . $this->adminSlug;
        $this->removeW3CommentOnAklamatorPage();
    }

    private function hooksAndFilters(): void
    {
        add_action('admin_menu', [$this, 'aklamatorRssAdminMenu']);
    }

    private function enqueueJs()
    {
        WpEnqueue::addAdminScript('aklamatorRssAdminScript', 'aklamator-rss',
            PLUGIN_DIR_URI . 'src/Wp/AklamatorRss/view/admin/js/main.js', [], microtime());
        WpEnqueue::addAdminScript('jqueryUiSortableJs', 'aklamator-rss',
            '//code.jquery.com/ui/1.12.1/jquery-ui.js', [], microtime());
        WpEnqueue::addGlobalAdminScript('aklamatorButtonScript',
            PLUGIN_DIR_URI . 'src/Wp/AklamatorRss/view/admin/js/aklamatorButton.js', [], microtime());
    }

    private function enqueueCss()
    {
        WpEnqueue::addAdminStyle('aklamatorRssAdminStyle', 'aklamator-rss',
            PLUGIN_DIR_URI . 'src/Wp/AklamatorRss/view/admin/css/style.css',[], microtime());
        WpEnqueue::addAdminStyle('jqueryUiSortable', 'aklamator-rss',
            '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',[], microtime());
    }

    public function aklamatorRssAdminMenu(): void
    {
        add_menu_page(
            __('Aklamator Rss', 'gf'),
            __('Aklamator Rss', 'gf'),
            'manage_options',
            $this->adminSlug,
            [$this, 'aklamatorRssView'],
            ''
        );
    }
    public function aklamatorRssView(): void
    {
        global $wpdb;
        $repo = new AklamatorRss($wpdb);
        $posts = $repo->fetchAklamatorPosts('DESC');
        include_once(PLUGIN_DIR . 'src/Wp/AklamatorRss/view/admin/template/aklamatorRss.php');
    }

    private function removeW3CommentOnAklamatorPage()
    {
        $requestUri = $_SERVER['REQUEST_URI'];
        if($requestUri === '/aklamator-rss/' || $requestUri === '/aklamator-feed/') {
            add_filter('w3tc_can_print_comment', function ($w3tc_setting) {
                return false;
            },10,1);
        }
    }

    public function createAklamatorRssTable()
    {
//        global $wpdb;
//        $charset = $wpdb->get_charset_collate();
//        $tableName = $wpdb->prefix . 'aklamatorRssFeedPosts';
//        $sql = "create table IF NOT EXISTS $tableName(
//	    postId bigint,
//	    title varchar(255) not null,
//        author varchar(50) not null,
//	    url varchar(512) not null,
//        publishedAt DATETIME not null,
//        category varchar(50) not null,
//        tags varchar(512) not null,
//        featuredImage varchar(512) not null,
//        position TINYINT not null,
//	    createdAt DATETIME default CURRENT_TIMESTAMP null,
//		primary key (postId)){$charset}";
//        $wpdb->query($sql);
    }
}