jQuery(document).ready(function(){
    let sortable = jQuery('#sortable');
    let isNewPostPresent = false;
    const maxNumberOfAklamatorPosts = 30;
    const maxIndexNumberOfAklamatorPosts = maxNumberOfAklamatorPosts -1;
    // When changing the positions of the items change their data position depending on the index in the list
    sortable.sortable({
        update:function(event, ui) {
            let indexCount = maxIndexNumberOfAklamatorPosts;
            jQuery(this).children().each(function(index){
                   jQuery(this).attr('data-position',indexCount);
                   indexCount--;
            });
        }
    });
    sortable.disableSelection();

    // Make the ajax call and send the new data
    let addNewPostButton = jQuery('#addNewPost');
    addNewPostButton.on('click',addNewPost);

    addDeleteEventListeners();



    let aklamatorForm = jQuery('#aklamatorRssForm');
    aklamatorForm.on('submit',function(e){
        e.preventDefault();
        let posts = getPosts();
        jQuery.ajax({
            url: 'admin-ajax.php',
            type: 'POST',
            data: {
                posts: posts,
                action: 'saveAklamatorPosts',
            },
            success: function (response) {
               if(response.success === true) {
                   jQuery('#wpwrap').css('opacity','0.5');
                   let loader = '<div id="aklamatorLoader"></div>';
                   if (jQuery('#aklamatorLoader').val() === undefined) {
                       jQuery('body').append(loader);
                   }
                   location.reload();
               }
            },
            error: function () {
            }
        });
    });

    // Populate new post information
    let newPostIdInput = jQuery('#aklamatorPostId');
    newPostIdInput.on('input',function(){
        let postId = jQuery(this).val();
        if(postId.length > 3) {
            jQuery.ajax({
                url: 'admin-ajax.php',
                type: 'POST',
                data: {
                    action: 'populateNewPost',
                    id: postId,
                },
                success: function (response) {
                    let res = response.data;
                    if(res.postStatus !== 'publish') {
                        alert('Možete ubaciti samo objavljene članke');
                        clearNewPostForm();
                        return;
                    }
                    if(response.success === true) {
                        jQuery('#newPostTitle').text(res.postTitle);
                        jQuery('#newPostThumbnail').attr('src', res.postImageUrl);
                        isNewPostPresent = true;
                    } else {
                        clearNewPostForm();
                    }
                },
                error: function () {

                }
            });
        }
    })


    // HELPER FUNCTIONS

    function addDeleteEventListeners() {
        jQuery('.deleteAklamatorItem').on('click', function () {
            let dataId = jQuery(this).attr('data-id');
            deletePostItem(dataId);
        })
    }

    // Populate the posts object when the user submits the form
    function getPosts() {
        const posts = {};
        jQuery('.postItem').each(function(){
            posts[jQuery(this).attr('data-id')] = jQuery(this).attr('data-position');
        })
        // Return the object sorted by position
        let entries = Object.entries(posts);
        return entries.sort((a, b) => b[1] -a[1]);
    }
    // Populate the list with a new list item
    function addNewPost() {
        // If there is a new post in the add new post form
        if(isNewPostPresent === true) {
            // Check if the post is already in the list, if so, return.
            let postInput = jQuery('#aklamatorPostId');
            let postId = postInput.val();
            if(checkIfPostExistsInTheList(postId) === true) {
                alert('Ovaj članak se već nalazi u listi');
                return;
            }

            // If the list is full do FIFO
            if(jQuery('#sortable').find('li').length >= maxNumberOfAklamatorPosts) {
                jQuery('#sortable').find(`li:eq(${maxIndexNumberOfAklamatorPosts})`).remove();
                reIndexItems(true);
            } else {
                decrementIndexes();
            }
            let position = maxIndexNumberOfAklamatorPosts;
            let postTitle = jQuery('#newPostTitle').text();
            let postImageUrl = jQuery('#newPostThumbnail').attr('src');
            jQuery('#sortable').prepend(`
            <li class="ui-state-default postItem ui-sortable-handle" data-id="${postId}" data-position="${position}">
            <img src="${postImageUrl}" alt="featured image">
            <h3>${postTitle}</h3>
            <div data-id="${postId}" class="deleteAklamatorItem">
               <i class="far fa-trash-alt"></i>
            </div>
            </li>`)
            clearNewPostForm();
            // Clear the ID input when the user adds a new post
            postInput.val('');
            addDeleteEventListeners();
        }
    }
    // Clears the add new post form
    function clearNewPostForm() {
        jQuery('#newPostTitle').text('');
        jQuery('#newPostThumbnail').attr('src', 'https://via.placeholder.com/300x200.png?text=Istaknuta+slika');
        isNewPostPresent = false;
    }
    // Checks if the post that the user is trying to add already exists
    function checkIfPostExistsInTheList(postId) {
        return jQuery('#sortable').find(`[data-id="${postId}"]`).length > 0;

    }

    function deletePostItem(dataId) {
        jQuery('#sortable').find(`[data-id="${dataId}"]`).remove();
        reIndexItems();
    }

    function reIndexItems(isFifo = false) {
        let length = jQuery('#sortable').find('li').length;
        let counter = maxIndexNumberOfAklamatorPosts;
        if(isFifo === true) {
            counter = maxIndexNumberOfAklamatorPosts -1;
        }
        for(let i = 0; i <= length; i++) {
            jQuery('#sortable').find(`li:eq(${i})`).attr('data-position',counter);
            counter--;
        }
    }

    function decrementIndexes() {
        jQuery('#sortable').find('li').each(function(value,index){
           let currentIndex = jQuery(this).attr('data-position');
           jQuery(this).attr('data-position',currentIndex-1);
        });
    }
})