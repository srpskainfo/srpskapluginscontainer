jQuery(document).ready(function(){
    let aklButtons = jQuery('.aklButton');
    let clickDisabled = false;
   if(aklButtons.length > 0) {
       const addBtn = '<i style="color:#2271b1; font-size:15px;" class="fas fa-plus"></i>';
       const deleteBtn = '<i style="color:red; font-size:15px;" class="fas fa-minus"></i>';
       aklButtons.on('click',function(){
           if(clickDisabled === true) {
               console.log('nope');
               return;
           }
           let postId = jQuery(this).attr('data-id');
           let addOrDeleteAction = jQuery(this).attr('data-addordelete');
           jQuery.ajax({
               url: 'admin-ajax.php',
               type: 'POST',
               data: {
                   postId: postId,
                   buttonAction: addOrDeleteAction,
                   action: 'handleAklamatorOnButtonClick',
               },
               success: function (response) {
                   if(handleNewPost(response,postId,deleteBtn)) return;
                   if(handleDeletePost(response,postId,addBtn)) return;
                   if(!handleDuplicateEntry(response)) {
                       alert('Članak se već nalazi u listi');
                       return;
                   }
               },
               error: function () {
               }
           });
           preventButtonClicks();
       })
   }

   // Changes the button from add to delete and changes the data attribute accordingly
   function handleNewPost(response,postId,btn) {
       if(response.success === true && response.data.message === 'Post and additional posts inserted') {
           // Get the additional posts from the response and change their values and icons
           let additionalPostIds = response.data.additionalPosts;
           additionalPostIds.forEach(function(additionalPostId,index){
              let targetBtn = jQuery(`.aklButton[data-id="${additionalPostId}"]`);
              if(targetBtn.length > 0) {
                  targetBtn.html('<i style="color:red; font-size:15px;" class="fas fa-minus"></i>');
                  targetBtn.attr('data-addordelete','delete');
              }
           });
           let targetBtnNew = jQuery(`.aklButton[data-id="${postId}"]`)
           targetBtnNew.html(btn);
           targetBtnNew.attr('data-addordelete','delete');
           return;
       }
       if(response.success === true && response.data === 'Post Inserted') {
           let targetBtn = jQuery(`.aklButton[data-id="${postId}"]`)
           targetBtn.html(btn);
           targetBtn.attr('data-addordelete','delete');
           return true;
       }
       if(response.success === true && response.data.message === 'FIFO insert completed') {
           // Get the deleted post and change its button and attr
           let targetBtn = jQuery(`.aklButton[data-id="${response.data.deletedPostId[0].postId}"]`);
           if(targetBtn.length > 0) {
               targetBtn.html('<i style="color:#2271b1; font-size:15px;" class="fas fa-plus"></i>');
               targetBtn.attr('data-addordelete','add');
           }
           // Handle the new inserted button
           let targetBtnNew = jQuery(`.aklButton[data-id="${postId}"]`)
           targetBtnNew.html(btn);
           targetBtnNew.attr('data-addordelete','delete');
       }
       return false;
   }

   function handleDeletePost(response,postId,btn) {
       if(response.success === true && response.data.message === 'Post Deleted') {
           let targetBtn = jQuery(`.aklButton[data-id="${postId}"]`)
           targetBtn.html(btn);
           targetBtn.attr('data-addordelete','add');
           let newTargetBtn = jQuery(`.aklButton[data-id="${response.data.additionalPost}"]`);
           if(newTargetBtn.length > 0) {
               newTargetBtn.html('<i style="color:red; font-size:15px;" class="fas fa-minus"></i>');
               newTargetBtn.attr('data-addordelete','delete');
           }
           return true;
       }
       return false;
   }

   function handleDuplicateEntry(response) {
       return response.data !== 'Duplicate Entry';
   }

   function preventButtonClicks()
   {
       clickDisabled = true;
       setTimeout(function(){
           clickDisabled = false;
       },2000)
   }
});