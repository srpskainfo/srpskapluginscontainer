<h2>Aklamator Rss Feed</h2>
<div class="aklamatorContainer">
    <form id="aklamatorRssForm" action="" method="post">
        <ul id="sortable">
        <?php foreach($posts as $post):?>
            <li class="ui-state-default postItem ui-sortable-handle" data-id="<?=$post['postId']?>" data-position="<?=$post['position']?>">
                <img src="<?=$post['featuredImage']?>" alt="featured image">
                <h3><?=$post['title']?></h3>
                <div data-id="<?=$post['postId']?>" class="deleteAklamatorItem">
                    <i class="far fa-trash-alt"></i>
                </div>
            </li>
        <?php endforeach ?>
        </ul>
        <input class="buttonAklamator" type="submit" value="Sačuvaj">
    </form>
    <div class="addNewPost">
        <h2>Dodaj novi članak</h2>
        <input id="aklamatorPostId" type="number" name="id" placeholder="ID" aria-label="post id">
        <h3 id="newPostTitle"></h3>
        <img id="newPostThumbnail" src="https://via.placeholder.com/300x200.png?text=Istaknuta+slika" alt="post image">
        <button class="buttonAklamator" type="button" id="addNewPost">Dodaj članak</button>
    </div>
</div>