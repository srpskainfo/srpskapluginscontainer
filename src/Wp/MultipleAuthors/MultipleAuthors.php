<?php


namespace GfWpPluginContainer\Wp\MultipleAuthors;

use GfWpPluginContainer\Wp\MultipleAuthors\Service\Logger;
use GfWpPluginContainer\Wp\MultipleAuthors\Service\Setup;

class MultipleAuthors
{
    /**
     * @var Repository\MultipleAuthors
     */
    private $repository;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * MultipleAuthors constructor.
     */
    public function __construct()
    {
        global $wpdb;
        $this->repository = new Repository\MultipleAuthors(new Mapper\MultipleAuthors($wpdb));
        $this->logger = new Logger($wpdb);
    }

    public function activate()
    {
        $this->hooksAndFilters();
        $setup = new Setup();
        $setup->setup();
    }

    private function hooksAndFilters()
    {
        add_action('add_meta_boxes', [$this, 'addMetaBox']);
        add_action('save_post', [$this, 'saveData']);
        add_filter('post_row_actions', [$this, 'removeDefaultActions'], 10, 1);
        add_action('pre_get_posts', [$this, 'customQueryFilter']);
        add_action('admin_footer-edit.php', [$this, 'customScript']);
        add_action('manage_users_columns',[$this,'replacePostCountColumn']);
        add_action('manage_users_custom_column',[$this, 'populateNewPostCountColumn'], 10, 3);
        //This should be used only once on activate of main plugin
//        add_action('init', [$this, 'changeUserPermissions']);
        add_action('wp_loaded', [$this, 'removeBulkActions']);
        add_filter('manage_posts_columns', [$this, 'replaceAuthorColumn'], 99, 2);
        add_action('manage_posts_custom_column', [$this, 'populateNewAuthorColumn'], 5, 2);
        add_action('admin_head-post.php', [$this, 'checkPermission']);
        add_action('admin_notices', [$this, 'showErrorWhenNoPermission']);
        add_action('init', [$this, 'preventAuthorPublish']);
        add_action('init', [$this, 'blockGuestAuthorFromLogin']);
        add_action('admin_print_styles',[$this, 'hidePostListFiltersInAdmin']);
    }

    public function replacePostCountColumn($columns)
    {
        global $current_screen;
        if ($current_screen->id === 'users' & isset($_GET['role']) && $_GET['role'] === 'guest_author') {
            unset($columns['posts']);
            $columns['postCount'] = 'Članci';
        }
        return $columns;
    }

    public function populateNewPostCountColumn($value, $columnName, $userId)
    {
        if ($columnName === 'postCount' && isset($_GET['role']) && $_GET['role'] === 'guest_author') {
            $value = '<a href="edit.php?author=' . $userId .  '">' . $this->repository->getNumberOfPostsForAuthor($userId) .'</a>';
        }
        return $value;
    }

    public function populateNewAuthorColumn($columnName, $postId)
    {
        if ($columnName === 'autors') {
            foreach ($this->repository->getOwnersForPost((int)$postId) as $postOwner):?>
                <a href="edit.php?post_type=post&author=<?=$postOwner->getAuthorId()?>"><?=$postOwner->getAuthorDisplayName()?></a>
                <br>
            <?php
            endforeach;
        }
    }

    public function replaceAuthorColumn($columns, $postType)
    {
        if ($postType === 'post') {
            // Unset the default author column
            unset($columns['author']);
            // Use array slice to arrange the array the way we want it and define the new custom column
            $firstThreeColumns = array_slice($columns, 0, 3);
            $restOfTheColumns = array_slice($columns, 3);
            $newAuthorColumn = ['autors' => 'Autori'];
        }
        return array_merge($firstThreeColumns, $newAuthorColumn, $restOfTheColumns);
    }

    public function removeBulkActions()
    {
        if ($this->checkForLoggedInUserRole('author')) {
            add_filter('bulk_actions-edit-post', '__return_empty_array');
        }
    }

    private function checkForLoggedInUserRole($needle): bool
    {
        $currentUser = wp_get_current_user();
        $currentUserRole = $currentUser->roles;
        foreach ($currentUserRole as $role) {
            if ($role === $needle) {
                return true;
            }
        }
        return false;
    }

    public function changeUserPermissions()
    {
        $authorRole = get_role('author');
        $authorRole->remove_cap('edit_pages');
        $authorRole->remove_cap('edit_others_pages');
        $authorRole->remove_cap('edit_published_pages');
        $authorRole->add_cap('edit_published_posts');
        $authorRole->add_cap('edit_others_posts');
        $authorRole->add_cap('publish_posts');
        $authorRole->remove_cap('delete_published_posts');
        $authorRole->add_cap('manage_categories');
    }

    public function removeDefaultActions($actions)
    {
        if (get_post_type() === 'post' && checkForLoggedInUserRole('author')) {
            unset($actions['inline hide-if-no-js']);
            $authors = $this->repository->getOwnerIdsForPost(get_the_ID());
            $currentUserId = get_current_user_id();
            if (get_the_author_meta('id') === $currentUserId || in_array($currentUserId, $authors, true)) {
                if (get_post_status() === 'publish') {
                    unset($actions['trash']);
                }
            } else {
                unset($actions['edit'], $actions['trash']);
            }
        }
        return $actions;
    }

    /**
     * @param \WP_Query $query
     */
    public function customQueryFilter(\WP_Query $query)
    {
        global $pagenow;
        if ($pagenow === 'edit.php' && isset($_GET['author']) && is_admin()) {
            $authorPosts = $this->repository->getPostIdsForAuthor((int)$_GET['author']);
            if (count($authorPosts) > 0) {
                $query->set('author', '');
                $query->set('post__in', $authorPosts);
            }
        }
    }

    /**
     * My posts should list only posts if the user is the author (not the wp core author), so we print the counter for
     * those posts accordingly
     */
    public function customScript()
    {
        global $current_screen;
        $myCustomAuthorPosts = $this->repository->getPostIdsForAuthor(get_current_user_id());
        $myPostsCount = count($myCustomAuthorPosts);
        $rowClasses = [];
        foreach ($myCustomAuthorPosts as $post){
            $rowClasses[] = 'post-'.$post;
        }
        $rowsToExclude = json_encode($rowClasses);
        if ($current_screen->id === 'edit-post' && (checkForLoggedInUserRole('author'))):?>
            <script>
                let titles = document.getElementsByClassName('row-title');
                let rowsToExclude = <?=$rowsToExclude?>;
                Array.prototype.forEach.call(titles, function (elem) {
                    if (!rowsToExclude.includes(elem.parentElement.parentElement.parentElement.id)) {
                        elem.style.color = 'black';
                        elem.addEventListener('click', function (e) {
                            e.preventDefault();
                        })
                        elem.addEventListener("auxclick", (e) => {
                            if (e.button === 1) e.preventDefault();
                        });
                        elem.addEventListener('contextmenu', e => e.preventDefault());
                        elem.addEventListener('focus', function (e) {
                            elem.style.boxShadow = 'none';
                        })
                    }
                });
                let minePosts = document.getElementsByClassName('mine')[0];
                if (minePosts) {
                    minePosts.getElementsByTagName('span')[0].textContent = '(' + <?=$myPostsCount?> + ')';
                }
            </script>
        <?php
        endif;
        if($current_screen->id === 'edit-post' && (checkForLoggedInUserRole('editor'))):?>
            <script>
                let minePosts = document.getElementsByClassName('mine')[0];
                if (minePosts) {
                    minePosts.getElementsByTagName('span')[0].textContent = '(' + <?=$myPostsCount?> + ')';
                }
            </script>
        <?php endif;
    }

    public function addMetaBox()
    {
        $data = ['users' => get_users(['role__in' => ['author', 'editor', 'guest_author']])];
        add_meta_box('gfMultipleAuthors', 'Autori', [$this, 'metaBoxView'], 'post', 'side', 'high', $data);
    }

    /**
     * @param \WP_Post $post
     * @param $data
     */
    public function metaBoxView(\WP_Post $post, $data)
    {
        global $pagenow;
        $users = $data['args']['users'];
        $postOwners = $this->repository->getOwnersForPost($post->ID);
        $ownersIds = [];
        foreach ($postOwners as $postOwner) {
            $ownersIds[] = $postOwner->getAuthorId();
        }
        ?>
        <label for="authorSearch">Pretraga Autora</label><input id="authorSearch" type="text" name="authorSearch">
        <div class="checkboxWrapper">
            <?php
            /** @var \WP_User $user */
            foreach ($users as $user) :
                $checked = '';
                if (($pagenow === 'post-new.php') && ($user->ID === get_current_user_id() && $user->roles[0] !== 'editor')) {
                    $checked = 'checked';
                }
                if ($pagenow !== 'post-new.php' && in_array($user->ID, $ownersIds, true)) {
                    $checked = 'checked';
                }
                ?>
                <div>
                    <input <?=$checked?> data-displayName="<?=$user->display_name?>" class="authorSelect"
                                         id="<?=$user->ID?>" type="checkbox" name="authors[<?=$users->Id?>]"
                                         value="<?=$user->ID . '|' . $user->display_name?>">
                    <label for="<?=$user->ID?>"><?=$user->display_name?></label>
                </div>
            <?php
            endforeach; ?>
        </div>
        <?php
    }

    public function saveData($postId)
    {
        if (isset($_POST['authors']) && count($_POST['authors']) > 0) {
            $oldData = $this->repository->getOwnersForPost($postId, 'array');
            $newData = [];
            $action = 'create';
            //if there is data for post delete it
            if (count($oldData) > 0) {
                $this->repository->delete($postId);
                $action = 'update';
            }
            $data = [
                'postId' => (int)$postId
            ];
            foreach ($_POST['authors'] as $author) {
                [$authorId, $authorDisplayName] = explode('|', $author);
                $data['authorId'] = (int)$authorId;
                $data['authorDisplayName'] = $authorDisplayName;
                $newData[] = $data;
                $this->repository->insert($data);
            }
            $this->logger->log($action, get_current_user_id(), $postId, $newData, $oldData);
        }
    }

    public function getOwnersForPost(int $postId)
    {
        return $this->repository->getOwnersForPost($postId);
    }

    public function getPostIdsForAuthor(int $authorId)
    {
        return $this->repository->getPostIdsForAuthor($authorId);
    }

    public function checkPermission()
    {
        global $pagenow;
        if ($pagenow === 'post.php') {
            if (checkForLoggedInUserRole('editor') || checkForLoggedInUserRole('administrator')) {
                return;
            }

            // Allow core author to edit drafts because of auto save
            if (get_the_author_meta('ID') === get_current_user_id() && (get_post_status() === 'draft' || get_post_status() === 'pending')) {
                return;
            }

            $allowedPostIds = $this->getPostIdsForAuthor(get_current_user_id());
            if (in_array(get_the_ID(), $allowedPostIds, false)) {
                return;
            }
            wp_safe_redirect(get_admin_url(null, '/edit.php?permissionError=true'));
        }
    }

    public function showErrorWhenNoPermission(): void
    {
        if (isset($_GET['permissionError'])) {
            echo '<div class="notice notice-error is-dismissible"><p>Nemate priviligeije da pristupite ovom sadržaju</p></div>';
        }
    }

    /**
     * This prevents authors from publishing posts,
     * but enables them to change the post status between draft and pending freely
     *
     */
    public function preventAuthorPublish()
    {
        add_filter('wp_insert_post_data', function ($data, $postData) {
            if (checkForLoggedInUserRole('author')) {
                // If the post was draft before editing
                if (get_post_status($postData['ID']) === 'draft') {
                    // If the new post data is pending change to pending else change to draft
                    if ($data['post_status'] === 'pending') {
                        $data['post_status'] = 'pending';
                    } else {
                        $data['post_status'] = 'draft';
                    }
                }
                // If the post was pending before editing
                if (get_post_status($postData['ID']) === 'pending') {
                    // If the new post data is draft change to draft else change to pending
                    if ($data['post_status'] === 'draft') {
                        $data['post_status'] = 'draft';
                    } else {
                        $data['post_status'] = 'pending';
                    }
                }
            }
            return $data;
        }, 10, 2);
    }

    function blockGuestAuthorFromLogin()
    {
        if (is_admin() && current_user_can('guest_author') && !(defined('DOING_AJAX') && DOING_AJAX)) {
            wp_logout();
            wp_redirect(home_url());
            exit;
        }
    }

    public function hidePostListFiltersInAdmin() {
        if ($this->checkForLoggedInUserRole('author')) {
            global $pagenow;

            if ($pagenow === 'edit.php') {
                echo "<style>
                .subsubsub .sticky {
                display:none !important;
                }
            </style>";
            }
        }
        if ($this->checkForLoggedInUserRole('editor')) {
            global $pagenow;

            if ($pagenow === 'edit.php') {
                echo "<style>
                .subsubsub .sticky {
                display:none !important;
                }
                .subsubsub .draft {
                display:none !important;
                }
            </style>";
            }
        }
    }
}

