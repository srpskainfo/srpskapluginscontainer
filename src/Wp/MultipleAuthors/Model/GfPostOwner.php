<?php
namespace GfWpPluginContainer\Wp\MultipleAuthors\Model;

class GfPostOwner
{
    private $id;
    private $postId;
    private $authorId;
    private $authorDisplayName;


    /**
     * gfAuthorModel constructor.
     * @param $id
     * @param int $postId
     * @param int $authorId
     * @param string $authorDisplayName
     * @param null $createdAt
     * @param null $updatedAt
     */
    public function __construct(
        $id,
        int $postId,
        int $authorId,
        string $authorDisplayName,
        $createdAt = null,
        $updatedAt = null
    ) {
        if ($id) {
            $this->id = $id;
        }
        $this->postId = $postId;
        $this->authorId = $authorId;
        $this->authorDisplayName = $authorDisplayName;
    }

    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @return int
     */
    public function getPostId(): int
    {
        return $this->postId;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @return string
     */
    public function getAuthorDisplayName(): string
    {
        return $this->authorDisplayName;
    }
}