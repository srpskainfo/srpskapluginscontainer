<?php


namespace GfWpPluginContainer\Wp\MultipleAuthors\Service;


use GfWpPluginContainer\Wp\WpEnqueue;

class Setup
{

    /**
     * Setup constructor.
     */
    public function setup()
    {
       $this->enqueueJs();
       $this->enqueueCss();
//       $this->createNewUserRole();
//        $this->createTables();
    }

    private function enqueueCss()
    {
        WpEnqueue::addAdminStyle('postEditCss', 'post.php', PLUGIN_DIR_URI . '/src/Wp/MultipleAuthors/css/postEditScreen.css');
        WpEnqueue::addAdminStyle('postEditCss', 'post-new.php', PLUGIN_DIR_URI . '/src/Wp/MultipleAuthors/css/postEditScreen.css');
    }

    private function enqueueJs()
    {
        WpEnqueue::addAdminScript('postEditJs','post.php', PLUGIN_DIR_URI . '/src/Wp/MultipleAuthors/js/postEditScreen.js',
                                  '', '', true);
        WpEnqueue::addAdminScript('postEditJs','post-new.php', PLUGIN_DIR_URI . '/src/Wp/MultipleAuthors/js/postEditScreen.js',
                                  '', '', true);
    }
    private function createTables()
    {
        $charset = $this->db->get_charset_collate();
        $sql = "CREATE TABLE {$this->tableName}(
	        id int auto_increment,
	        postId bigint(20) null,
	        authorId bigint(20) null,
	        authorDisplayName varchar(128) null,
	        createdAt TIMESTAMP default CURRENT_TIMESTAMP not null,
	        updatedAt TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP not null,
		    primary key (id)
            ){$charset}";
        $this->db->query($sql);
    }

    private function createNewUserRole()
    {
        add_action('init', function (){
            add_role( 'guest_author', 'Guest Author', array(
                'read' => true, // True allows that capability
            ) );
        });

    }
}