document.getElementById('authorSearch').addEventListener('keyup', filterAuthorCheckbox)

function filterAuthorCheckbox (e){
    let searchInputVal = e.target.value;
    let authorSelects = document.getElementsByClassName('authorSelect');
    Array.prototype.forEach.call(authorSelects, function (elem) {
        let displayName = elem.getAttribute('data-displayName');
        if (searchInputVal === '') {
            elem.parentElement.classList.remove('hidden');
        }
        if (!displayName.toUpperCase().includes(searchInputVal.toUpperCase())){
            elem.parentElement.classList.add('hidden')
        } else {
            elem.parentElement.classList.remove('hidden')
        }
    })
}