<?php

namespace GfWpPluginContainer\Wp;


use GfWpPluginContainer\Wp\PushNotifications\Actions\MarkTokenAsUnsubscribed;
use GfWpPluginContainer\Wp\PushNotifications\Actions\SaveTokenToDatabase;

class FbNotification
{
    const SERVER_KEY = 'AAAARl0YBZY:APA91bEfB2_zfTT2vES6Diu1w-PrnW0RaCxvEl5IH0uPmjsgF8BRPsfuywBCqDtKJw2iwcwrUGtzeejBvrMYptkpJNSizJllGye-hfMWhUWeBK3mIODPcebu9X6XkH1rLAMqZEGKTDlS';

    /**
     * Send push to topic
     *
     * @param $topic
     * @param $title
     * @param $subtitle
     * @param $image
     * @param $icon
     * @param $actionUrl
     * @return bool
     */
    public function sendPushToTopic($topic, $title, $subtitle, $image, $icon, $actionUrl)
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        $headers = [
            'Authorization: key=' . static::SERVER_KEY,
            'Content-Type: application/json',
        ];
        $dt = new \DateTime('now',new \DateTimeZone('Europe/Sarajevo'));
        $data = [
            'data' => [
                'title' => $title,
                'body' => $subtitle,
                'image' => $image,
                'requireInteraction' => true,
                'icon' => $icon,
                'click_link' => $actionUrl . '?utm_source=fbPush',
                'dateSent' => $dt->getTimestamp(),
                // Add the icon badge when we get it
//                'badge' => 'https://firebasestorage.googleapis.com/v0/b/fcm-on-web.appspot.com/o/icons8-notification-24.png?alt=media'
            ],
            'to' => '/topics/' . $topic,
        ];
        if ($this->send($url, $headers, $data)) {
            $this->saveNotificationHistory($data['data']);
            return true;
        }
        return false;
    }

    /**
     * Unsubscribe token from given topic
     * @param $token
     * @param $topic
     * @return bool
     */
    public function unsubscribeTokenToTopic($token, $topic)
    {
        $url = "https://iid.googleapis.com/v1/web/iid/" . $token;
        $headers = [
            'Authorization: key=' . static::SERVER_KEY,
        ];
//        $data = [
//            'to' => '/topics/' . $topic,
//            'registration_tokens' => $token
//        ];
        if ($this->send($url, $headers, null, true)) {
            try {
                MarkTokenAsUnsubscribed::markTokenAsUnsubscribed($token);
            } catch (\Exception $e) {
                wp_send_json_error(['err' => $e->getMessage()]);
            }
            return true;
        }
        return false;
    }

    /**
     * Subscribe token to given topic
     * @param $token
     * @return bool
     */
    public function subscribeTokenToTopic($token, $topic)
    {
        $url = sprintf("https://iid.googleapis.com/iid/v1/%s/rel/topics/%s", $token, $topic);
        $headers = [
            'Authorization: key=' . static::SERVER_KEY,
            'Content-Type: application/json',
            'Content-Length: 0',
        ];
        if ($this->send($url, $headers)) {
            try {
                SaveTokenToDatabase::saveToken($token);
            } catch (\Exception $e) {
                wp_send_json_error(['err' => $e->getMessage()]);
            }
            return true;
        }
        return false;
    }

    /**
     * Send request to FB
     * @param $url
     * @param $headers
     * @param null $data
     * @return bool
     */
    private function send($url, $headers, $data = null, $delete = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if (!$delete) {
            curl_setopt($ch, CURLOPT_POST, 1);
        } else {
            curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        if ($data) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode !== 200 && $httpCode !== 404) {
            echo 'response from service ' . PHP_EOL;
            var_dump($response);
            die();
        }
        return true;
    }

    private function saveNotificationHistory($data): void
    {
        global $cache;

        $history = unserialize($cache->get('notificationHistory'), ['allowed_classes' => false]) ?? [];
        $history[] = $data;
        if (count($history) > 10) {
            unset($history[0]);
            $history = array_values($history);
        }

        $cache->set('notificationHistory',serialize($history),0);
    }
}