<?php

namespace GfWpPluginContainer\Wp;

class PostHelper
{
    /**
     * @param int $postId
     * @param string $timeFrame
     * @param array|null $exclude
     * @param int|null $numberOfTags
     * @return WP_Post[]
     */
    public static function getPostsWithMatchingTag($postId, array $exclude = null, $limit = 4): array
    {
        $postTagIds = [];
        /** @var  $postTags \WP_Term[] */
        $postTags = wp_get_post_tags($postId);
        foreach ($postTags as $postTag) {
            $postTagIds[] = $postTag->term_id;
        }

        $posts = get_posts([
            'tag__in' => $postTagIds,
            'numberposts' => $limit,
            'post__not_in' => $exclude,
            'orderby' => 'date',
            'order' => 'DESC',
            'suppress_filters' => false,
            'category__not_in' => [55687]
        ]);
        foreach ($posts as $post) {
            $restrictedPosts[] = $post->ID;
        }

        if(count($posts) < 4) {
            $postsSecondQuery = get_posts([
                'numberposts' => ($limit - count($posts)),
                'category' => get_the_category($postId)[0]->term_id,
                'post__not_in' => $exclude,
                'orderby' => 'date',
                'order' => 'DESC',
                'suppress_filters' => false,
                'category__not_in' => [55687]
            ]);
	        foreach ($postsSecondQuery as $post) {
		        $restrictedPosts[] = $post->ID;
		        $posts[] = $post;
	        }
        }

        return $posts;
    }

    public static function getPopularPosts($numberOfPosts = null, $category = null)
    {
        global $restrictedPosts, $cache;
        $postNumber = isset($numberOfPosts) ? $numberOfPosts : 5;
        if(isset($category)) {
            $key = 'popularPostsQueryCatId#' . (int)$category . $numberOfPosts;
        } else {
            $key = 'popularPosts' . $numberOfPosts;
        }
        $posts = $cache->get($key);
        $after = 12;
        if ($category) {
            $after = 36;
            if ($category === 16) {
                $after = 240;
            }
            if ($category === 35359) {
                $after = 120;
            }
        }
        if ($posts === false) {
            $posts = get_posts([
                'numberposts' => $postNumber,
                'post_type' => 'post',
                'category' => (int) $category,
                'exclude' => $restrictedPosts,
                'ignore_sticky_posts'=> true,
                'meta_key' => 'gfPostViewCount',
                'orderby' => 'meta_value_num',
                'order' => 'DESC',
                'post_status' => 'publish',
                'date_query' => [
                    ['after' => sprintf('-%s hours', $after)],
                ],
                'category__not_in' => [55687]
            ]);
            $cache->set($key, serialize($posts), 60 * 10);
        } else {
            $posts = unserialize($posts);
        }
        foreach ($posts as $post) {
            $restrictedPosts[] = $post->ID;
        }

        return $posts;
    }

    public static function getNewestPosts($numberOfPosts = null, $category = null)
    {
        global $restrictedPosts;
        $postNumber = 5;
        if (isset($numberOfPosts)) {
            $postNumber = $numberOfPosts;
        }
        // If category is set, get posts from that category
        if(isset($category)) {
            return get_posts(['numberposts' => $postNumber,'post_status' => 'publish', 'category__not_in' => [55687],'category' => (int) $category]);
        }
//        return get_posts(['numberposts' => $postNumber, 'exclude' => $restrictedPosts]);
        return get_posts(['numberposts' => $postNumber,'post_status' => 'publish', 'category__not_in' => [55687]]);
    }
    
    public static function postPublishChecklist()
    {
    	WpEnqueue::addGlobalAdminScript('postChecklist', PLUGIN_DIR_URI. 'assets/js/global/postChecklist.v4.js',[],'',true);
        WpEnqueue::addGlobalAdminScript('gutenbergShortcuts', PLUGIN_DIR_URI. 'assets/js/global/gutenbergShortcuts.js',[],'',true);
    }

    /**
     * @param $post
     * @return bool
     */
    public static function isSportOrChildPage($post): bool
    {
        if(is_tag() || is_author()) {
            return false;
        }
        $subSportCategories = get_categories(['parent' => 6]);
        $subSportCategoryNames = ['Sport'];
        foreach($subSportCategories as $subSportCategory) {
            $subSportCategoryNames[] = $subSportCategory->name;
        }
        if($post->post_type === 'post') {
            return count(array_intersect(wp_get_post_categories($post->ID,['fields' => 'names']),$subSportCategoryNames)) > 0;

        }
        return $post->post_title === 'Sport' || in_array($post->post_title, $subSportCategoryNames,false);
    }

    public static function isSportOrChildPageApp($post = null, $refererUrl = false) {
        $uri =  $_SERVER['REQUEST_URI'];
        if($refererUrl) {
            $uri = $refererUrl;
        }
        if(strpos($uri,'type=tag')) {
            return false;
        }
        if($post) {
            if(($post->post_type === 'post' && get_the_category($post->ID)[0]->category_parent === 6) ||
                ($post->post_type === 'post' && get_the_category($post->ID)[0]->slug === 'sport')) {
                return true;
            }
        }
        if(strpos($uri,'url=sport&type=page')) {
            return true;
        }
        $subSportCategories = get_categories(['parent' => 6]);
        foreach($subSportCategories as $subSportCategory) {
            if(strpos($uri,"url=$subSportCategory->slug&type=page")) {
                return true;
            }
        }
        return false;
    }
}