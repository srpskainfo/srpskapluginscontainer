<?php


namespace GfWpPluginContainer\Wp;


class ImageEdit
{
    public function init()
    {
        $this->addHooksAndFilters();
    }



    public function enqueueStylesAndScripts()
    {
        WpEnqueue::addAdminScript('cropSelectJs','toplevel_page_gf-image-editor',PLUGIN_DIR_URI . 'assets/js/cropSelect/crop-select-js.min.js',['cropJsCdn']);
        WpEnqueue::addAdminScript('cropJsCdn','toplevel_page_gf-image-editor','//code.jquery.com/jquery.min.js');
    }

    private function addHooksAndFilters()
    {
        add_action('wp_ajax_imageEdit', [$this,'pixelateImage']);
        add_action('init',function() {
            $this->enqueueStylesAndScripts();
        });
        add_filter( 'attachment_fields_to_edit', [$this,'addEditImageField'], null, 2 );
    }

    public function pixelateImage()
    {
        try {
            $savePath = wp_upload_dir()['path'];
            // Id of the image that is being pixelated
            $originalImageId = $_POST['originalImageId'];
            // Get the post object of the image being pixelated
            $wpOriginalImage = get_post($originalImageId);
            // Get the path of the image being pixelated
            $originalImagePath = wp_get_attachment_image_url($originalImageId, 'full');
            // Get the extension from the original image info
            $originalImageInfo = pathinfo($originalImagePath);
            $extension = $originalImageInfo['extension'];

            // Make a new instance from the original to be cropped
            $originalCrop = new \Imagick($originalImagePath);
            $originalCropName = $originalImageInfo['filename'] . '-originalCrop.' . $extension;

            // Crop the original image and save it as a separate image with the coordinates sent by the user
            $cropWidth = $_POST['selectionWidth'];
            $cropHeight = $_POST['selectionHeight'];
            $selectionX = $_POST['selectionX'];
            $selectionY = $_POST['selectionY'];

            $originalCrop->cropImage($cropWidth, $cropHeight, $selectionX, $selectionY);
            $originalCrop->writeImage($savePath . '/' . $originalCropName);
            $originalCropAttachmentId = $this->saveAttachment($originalCropName, $savePath . '/' . $originalCropName);

            // TODO: make the createfrom function work for all extensions
            // Pixelate the cropped image
            $pixelateCropImage = $this->cloneWpImageWithNewIdAndGuid($wpOriginalImage, $originalCropName);
            switch($extension) {
                case 'jpg':
                    $croppedImage = imagecreatefromjpeg($pixelateCropImage['guid']);
                    break;
                case 'png':
                    $croppedImage = imagecreatefrompng($pixelateCropImage['guid']);
                    break;
                default:
                    throw new \Exception('image not supported: .' . $extension);

            }
            imagefilter($croppedImage, IMG_FILTER_PIXELATE, 20, true);
            imagejpeg($croppedImage, $savePath . '/pixelated-' . $originalCropName);

            // Make an Imagick instance of the cropped image
            $blurredSegment = new \Imagick($savePath . '/pixelated-' . $originalCropName);

            // Delete the cropped image since we are not using it anymore
            wp_delete_attachment($originalCropAttachmentId);

            // Combine the two images
            $originalImage = new \Imagick($originalImagePath);
            $newImageName = $originalImageInfo['filename'] . '-pixelated.' . $extension;
            $originalImage->compositeimage($blurredSegment, \Imagick::COMPOSITE_OVER, $selectionX, $selectionY);
            $originalImage->writeImage($savePath . '/' . $newImageName);

            // Save the end result and delete the original image
            $newImage = $this->cloneWpImageWithNewIdAndGuid($wpOriginalImage, $newImageName);
            $attachmentId = $this->saveAttachment($newImage, $savePath . '/' . $newImageName);
            // Update the new image caption (we use it as author) based on the original image value
            $this->updatePostCaptionByAttachmentId($attachmentId,$originalImageId);
            // Delete the original image
            wp_delete_attachment($originalImageId);

            // Delete the pixelated crop
            wp_delete_file($savePath . '/pixelated-' . $originalCropName);

            // Send the new attachment id so we can redirect the user to the attachment
            wp_send_json_success(['attachmentId' => $attachmentId]);

        } catch (\Exception $e) {
            wp_send_json_error(['message' => 'There has been an error: ' . $e->getMessage()]);
            $this->log($e->getMessage());
            throw new \Exception('There has been an error: ' . $e->getMessage());
        }
    }

    private function updatePostCaptionByAttachmentId($attachmentId,$originalImageId)
    {
        $originalImage = get_post($originalImageId);
        $args = [
            'ID' => $attachmentId,
            'post_excerpt' => $originalImage->post_excerpt
        ];
        wp_update_post($args);
    }

    private function log($msg)
    {
        $path = __DIR__ . '/../../../../imageEditError.log';
        $msg = date('Y-m-d') . ' --- ' . $msg . PHP_EOL;
        file_put_contents($path, $msg, FILE_APPEND);
    }

    private function cloneWpImageWithNewIdAndGuid($wpImage,$newImageName)
    {
        $newImage = $wpImage->to_array();
        $newImage['ID'] = '';
        $newImage['guid'] = wp_get_upload_dir()['url'] . '/' . $newImageName;
        return $newImage;
    }

    private function saveAttachment($image,$path)
    {
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        $attachmentId = wp_insert_attachment($image, $path);
        $attachData = wp_generate_attachment_metadata( $attachmentId, $path);
        wp_update_attachment_metadata($attachmentId, $attachData);
        return $attachmentId;
    }


    public function addEditImageField( $fields, $post )
    {
        $gfEditImage = (bool) get_post_meta($post->ID, 'gf_image_edit', true);
        $fields['gf_image_edit'] = array(
            'label' => 'Pikselizuj deo slike',
            'input' => 'html',
            'html' => '<a title="Gf Image Editor" href="' . admin_url('?page=gf-image-editor') . '&imageId=' . $post->ID . '">' . __('Pikselizuj deo slike','gfImageEdit') .  '</a>',
            'value' => $gfEditImage,
            'helps' => ''
        );
        return $fields;
    }

}