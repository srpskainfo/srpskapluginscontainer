<?php


namespace GfWpPluginContainer\Wp\CustomPostApi\Service;


class Setup
{
    /**
     * @var \QM_DB|\wpdb
     */
    private $db;

    /**
     * @var string
     */
    private $tableName;

    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
        $this->tableName = $this->db->prefix . 'fetchedCreatedPosts';
    }

    public function setup()
    {
//        $this->createFetchedCreatedPostsTable();
//        $this->createPostIdIndex();
    }

    private function createPostIdIndex() :void
    {
        $indexName = '_customPostApiPostIdIndex';
        $sql = "CREATE index {$indexName}
	        on $this->tableName (`postId`);";
        $this->db->query($sql);
    }

    private function createFetchedCreatedPostsTable(): void
    {
        $charset = $this->db->get_charset_collate();
        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableName}(
	        id int auto_increment,
	        postData TEXT not null,
	        createdAt TIMESTAMP default CURRENT_TIMESTAMP not null,
	        postId bigint(20) not null,
		    primary key (id)
            ){$charset}";
        $this->db->query($sql);
    }
}