<?php

namespace GfWpPluginContainer\Wp\CustomPostApi\Service;

class PostDataFormatter
{
    public static function formatPostData($postData): array
    {
        $title = $postData['title'] ?? null;
        $body = null;
        foreach($postData['body'] as $paragraph) {
            $body .= '<!-- wp:paragraph --> <p>' . $paragraph . '</p><!-- /wp:paragraph -->';
        }
        $categoryId = [$postData['categoryId']] ?? null;
        $userId = null;
        if(isset($postData['authorId']) && $postData['authorId'] !== '') {
            $userId = $postData['authorId'];
        } else {
            $userId = 48;
        }
//        elseif(isset($postData['userId']) && $postData['userId'] !== '') {
//            $userId = $postData['userId'];
//        }
        $publish = false;
        // If publish is set, set to publish if true and to draft if false
        if(isset($postData['publish'])) {
            if($postData['publish']) {
                $publish = 'publish';
            } else {
                $publish = 'pending';
            }
        }

        return [
            'post_title' => $title,
            'post_content' => $body,
            'post_category' => $categoryId,
            'post_author' => $userId,
            'post_status' => $publish

        ];
    }
}