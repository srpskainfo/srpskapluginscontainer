<?php


namespace GfWpPluginContainer\Wp\CustomPostApi\Service;


class PostDataValidator
{
    public static function validatePostData($data,$secret)
    {
        $message = [];
        if ($data['secret'] !== $secret) {
            $message['authErr'] =  'You are not authorized for this action.';
        }
        if(!isset($data['userId']) && !isset($data['authorId'])) {
            $message['userErr'] =  'User or author ID is required. ';
        } else {
            if($data['authorId'] === '' || $data['authorId'] === null) {
                $userId = 48; //srpskainfo author
            } else {
                $userId = $data['authorId'];
            }
            $user = get_user_by('id',$userId);
            if(!$user) {
                $message['userErr'] = 'User with the given ID was not found';
            }
        }
        if(!isset($data['categoryId'])) {
            $message['catErr'] = 'Category ID is required';
        } else {
            $category = get_category($data['categoryId']);
            if(!$category) {
                $message['catErr'] = 'Category with the given ID was not found';
            }
        }
        if(count($message) > 0) {
            return $message;
        }
        return true;
    }
}