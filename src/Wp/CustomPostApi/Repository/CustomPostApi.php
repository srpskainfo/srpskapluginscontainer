<?php

namespace GfWpPluginContainer\Wp\CustomPostApi\Repository;

class CustomPostApi
{
    /**
     * @var \wpdb
     */
    private $db;
    /**
     * @var string
     */
    private $tableName;

    /**
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db;
        $this->tableName = $this->db->prefix . 'fetchedCreatedPosts';
    }

    public function insert($data,$postId): void
    {
        $postData = ['postData' => $data,'postId' => $postId];
        $this->db->insert($this->tableName,$postData);
    }

    public function fetchEntriesByPostData($data) {
        return $this->db->get_var("SELECT COUNT(*) FROM {$this->tableName} WHERE `postData` = '{$data}'");
    }

    public function deletePost($postId) {
        $sql = "DELETE FROM {$this->tableName} WHERE `postId` = {$postId} ";
        $this->db->query($sql);
    }
}