<?php

namespace GfWpPluginContainer\Wp\CustomPostApi;

use GfWpPluginContainer\Wp\CustomPostApi\Service\PostDataFormatter;
use GfWpPluginContainer\Wp\CustomPostApi\Service\PostDataValidator;
use GfWpPluginContainer\Wp\CustomPostApi\Service\Setup;

class CustomPostApi
{
    private $secret = '_ergtzf@JmR7@4Khg7NND7NsuHbrZ+6jEHT!S_Gqg_rq5*BZ';

    /**
     * @var Repository\CustomPostApi
     */
    private $repository;


    public function init(): void
    {
        global $wpdb;
        $setup = new Setup();
        $setup->setup();
        $this->repository = new Repository\CustomPostApi($wpdb);
        $this->hooksAndFilters();

    }

    private function hooksAndFilters(): void
    {
        add_action('rest_api_init',function(){
            register_rest_route('customPostApi', '/article', [
                'methods' => 'POST',
                'callback' => [$this,'handleRequest']
            ]);
        });
        add_action('wp_trash_post', [$this,'deletePostFromTableOnPostDelete'],10,1);
    }

    /**
     * The callback function the the rest route.
     * The callback validates the request and handles it accordingly.
     * Returns an array with the response message
     * @param $requestData
     * @return array
     */
    public function handleRequest($requestData): array
    {
        $requestDataParams = $requestData->get_params();

        // Validate the data
        $isDataValid = PostDataValidator::validatePostData($requestDataParams,$this->secret);
        if($isDataValid !== true) {
            $data['response'] = $isDataValid;
            return $data;
        }
        // Format the data to be ready for the post insertion
        $args = PostDataFormatter::formatPostData($requestDataParams);

        /*
         * Check if there is a duplicate entry in our custom table in the DB, if there is, return.
         * If there are not duplicate entries, try to insert the wp post and add it to our custom table
         * */
        if((int)$this->repository->fetchEntriesByPostData(serialize($args)) === 0) {
            $postId = wp_insert_post($args);
            if($postId) {
                $this->repository->insert(serialize($args),$postId);
                $data['response'] = 'Successfully inserted the post.';
            } else {
                $data['response'] = 'There has been an error inserting the post.';
            }
        } else {
            $data['response'] = 'Duplicate entry';
            return $data;
        }

        return $data;
    }

    public function deletePostFromTableOnPostDelete($postId)
    {
        $this->repository->deletePost($postId);
    }
}