<?php
namespace GfWpPluginContainer\Wp;

use \GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;

class PostCount
{
    const META_KEY = 'gfPostViewCount';
    const SAVE_TRESHOLD = 30;

    public function init ()
    {
//        add_action('wp_head', [$this, 'trackPostViews']);
        //add_action( 'wp_ajax_setAjaxViewCount', 'setAjaxViewCount'); // for logged in user
        add_action( 'wp_ajax_nopriv_setAjaxViewCount', [$this, 'setAjaxViewCount']);
        //To keep the count accurate, lets get rid of pre fetching
        remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10);
    }

    public function setAjaxViewCount()
    {
        global $cache;

        $postId = (int) $_POST['postId'];
        $count = $cache->get(self::META_KEY .'#'. $postId);
        if ($count === false) {
            $count = (int) get_post_meta($postId, self::META_KEY, true);
        }
        //First post visit
        if ($count == '') {
            add_post_meta($postId, self::META_KEY, 1);
        } else {
            $this->updateCount($postId, $count);
        }
        exit();
    }

    private function updateCount($postId, $count)
    {
        global $cache;
        $key = self::META_KEY .'#'. $postId;
        $count++;
        $cache->set($key, $count, 0);
        if ($count % self::SAVE_TRESHOLD === 0) {
            update_post_meta($postId, self::META_KEY, $count);
            ArticleRepo::syncItemToElastic(get_post($postId));

        }
    }
}