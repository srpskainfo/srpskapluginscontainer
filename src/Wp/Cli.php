<?php

namespace GfWpPluginContainer\Wp;

use DateTime;
use GfWpPluginContainer\Indexer\Factory\ElasticClientFactory as ElasticFactory;
use GfWpPluginContainer\Wp\AklamatorRss\AklamatorRss;


class Cli
{
    private $cache;
    public function __construct($cache)
    {
        $this->cache = $cache;
        ini_set('max_execution_time', 1200);
        try {
            \WP_CLI::add_command('createElasticIndex', [$this, 'createElasticIndex']);
            \WP_CLI::add_command('createImagesIndex', [$this, 'createImagesIndex']);
            \WP_CLI::add_command('createArticlesIndex', [$this, 'createArticlesIndex']);
            \WP_CLI::add_command('syncArticleIndex', [$this, 'syncArticleIndex']);
            \WP_CLI::add_command('syncImagesIndex', [$this, 'syncImagesIndex']);
            \WP_CLI::add_command('importAuthors', [$this, 'importCoreAuthorsForMultipleAuthorsPlugin']);
            \WP_CLI::add_command('createRole', [$this, 'createRole']);
            \WP_CLI::add_command('syncMediaLibToImageOptimizer', [$this, 'syncMediaLibToImageOptimizer']);
            \WP_CLI::add_command('processImageOptimizerQueue', [$this, 'processImageOptimizerQueue']);
            \WP_CLI::add_command('addScheduledPostsToAklamator',[$this,'addScheduledPostsToAklamator']);
//            \WP_CLI::add_command('watermarkImage', [$this, 'watermarkImage']);
//            \WP_CLI::add_command('undoWatermarkImage', [$this, 'undoWatermarkImage']);
        } catch (\Exception $e) {
            // cannot catch wp errors ?
        }
    }

    public function addScheduledPostsToAklamator()
    {
        date_default_timezone_set('Europe/Belgrade');
        $posts = unserialize($this->cache->get('futureAklamatorPosts'));
        $dateNow = new DateTime();
        if($posts && count($posts) > 0) {
            $aklamatorRss = new AklamatorRss();
            $aklamatorRss->init();
            foreach ($posts as $postId => $postPublishDate) {
                $postDate = DateTime::createFromFormat('Y-m-d H:i:s', $postPublishDate);
                if ($postDate <= $dateNow) {
                    $aklamatorRss->handleAklamatorOnButtonClick($postId, 'add', $this->cache, false);
                    unset($posts[$postId]);

                }
            }
            $this->cache->set('futureAklamatorPosts', serialize($posts), 0);
        }
    }

//    public function undoWatermarkImage()
//    {
//        global $wpdb;
//        $imageOptimizer = new \GfWpPluginContainer\Wp\ImageOptimizer($wpdb);
//        $imageOptimizer->undoWatermarkImage();
//    }
//
//    public function watermarkImage()
//    {
//        global $wpdb;
//        $imageOptimizer = new \GfWpPluginContainer\Wp\ImageOptimizer($wpdb);
//        $imageOptimizer->watermarkImage();
//    }

    public function processImageOptimizerQueue()
    {
        global $wpdb;
        $imageOptimizer = new \GfWpPluginContainer\Wp\ImageOptimizer($wpdb);
        $imageOptimizer->processQueue();
    }

    public function syncMediaLibToImageOptimizer()
    {
        global $wpdb;
        $imageOptimizer = new \GfWpPluginContainer\Wp\ImageOptimizer($wpdb);
        $imageOptimizer->syncImagesFromMediaLibrary();
    }

    public function createRole()
    {
        add_role( 'guest_author', 'Guest Author', array(
            'read' => true, // True allows that capability
        ) );
    }

    public function importCoreAuthorsForMultipleAuthorsPlugin()
    {
        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}posts WHERE `post_type` = 'post' AND ID NOT IN (
SELECT postId FROM {$wpdb->prefix}gfPostOwners
) AND post_status IN ('publish', 'draft') LIMIT 100000;";
        $data = $wpdb->get_results($sql);

        $values = [];
        foreach ($data as $datum) {
            $values[] = sprintf("(%s, %s, '%s')", $datum->ID, $datum->post_author, get_the_author_meta('display_name', $datum->post_author));
        }

        $insert = "INSERT INTO {$wpdb->prefix}gfPostOwners (`postId`, `authorId`, `authorDisplayName`)
            VALUES " . implode(',', $values);
        if (!$wpdb->query($insert)) {
            \WP_CLI::error('could not insert data: ' . $insert);
        } else {
            \WP_CLI::success(sprintf('%s items imported.', count($data)));
        }
    }

    public function createElasticIndex($arg)
    {
        if (!isset($arg[0])) {
            $recreate = false;
        } else {
            $recreate = $arg[0];
        }

        $msg = '';
        $msg .= $this->createArticlesIndex($recreate);
        $msg .= $this->createImagesIndex($recreate);
        \WP_CLI::success($msg);
    }

    public function createArticlesIndex($recreate)
    {
        $setupFactory = new \GfWpPluginContainer\Indexer\Service\Setup(
            ElasticFactory::make(),
            new \GfWpPluginContainer\Indexer\Config\Article()
        );
        try {
            return $setupFactory->createIndex($recreate);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            \WP_CLI::error($msg);
        }
    }

    public function createImagesIndex($recreate)
    {
        $setupFactory = new \GfWpPluginContainer\Indexer\Service\Setup(
            ElasticFactory::make(),
            new \GfWpPluginContainer\Indexer\Config\Image()
        );
        try {
            return $setupFactory->createIndex($recreate);
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            \WP_CLI::error($msg);
        }
    }

    public function syncArticleIndex()
    {
        $config = new \GfWpPluginContainer\Indexer\Config\Article();
        $indexer = new \GfWpPluginContainer\Indexer\Service\Indexer(
            \GfWpPluginContainer\Indexer\Factory\ElasticClientFactory::make()->getIndex($config->getIndex())
        );
        $indexer->indexAll();
    }

    public function syncImagesIndex()
    {
        $config = new \GfWpPluginContainer\Indexer\Config\Image();
        $indexer = new \GfWpPluginContainer\Indexer\Service\Indexer(
            \GfWpPluginContainer\Indexer\Factory\ElasticClientFactory::make()->getIndex($config->getIndex())
        );
        $indexer->indexImages();
    }
}