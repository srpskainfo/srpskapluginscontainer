<?php


namespace GfWpPluginContainer\Wp;


class Logger
{
    public static function ajaxLog($value)
    {
        file_put_contents(ABSPATH . 'wp-content/uploads/' . 'ajaxLog.txt', $value . PHP_EOL, FILE_APPEND);
    }

    public static function webNotificationLog($value)
    {
        file_put_contents(ABSPATH . 'wp-content/uploads/' . 'webNotificationLog.txt', $value . PHP_EOL, FILE_APPEND);
    }

    public static function generateDebugLog($fileName, $value)
    {
        file_put_contents(ABSPATH . 'wp-content/uploads/' . $fileName . '.txt', $value . PHP_EOL, FILE_APPEND);
    }
}