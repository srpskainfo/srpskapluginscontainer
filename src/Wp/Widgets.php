<?php


namespace GfWpPluginContainer\Wp;

class Widgets
{
    /**
     * Register all widgets inside the widgets folder, but the id of the widget must be in format gf_Filename
     *
     */
    public function registerWidgets()
    {
        $dir = new \DirectoryIterator(PLUGIN_DIR . 'src/Widgets');
        /** @var \DirectoryIterator $file */
        foreach ($dir as $file) {
            if (is_file($file->getPathname())){
            $name = $file->getBasename('.php');
            add_action('widgets_init', static function () use ($name) {
                register_widget('\GfWpPluginContainer\Widgets\\'. $name);
            });
        }}
    }
}