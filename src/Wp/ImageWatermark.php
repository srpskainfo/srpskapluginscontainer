<?php


namespace GfWpPluginContainer\Wp;


class ImageWatermark
{

    public function addHooksAndFilters()
    {
        add_filter( 'attachment_fields_to_edit', [$this,'addWatermarkToImageEditField'], null, 2 );
        add_filter( 'attachment_fields_to_save', [$this,'addWatermarkToImageSave'], null, 2 );
    }


    public function addWatermarkToImageEditField( $fields, $post )
    {
        $watermarkAdded = (bool) get_post_meta($post->ID, 'image_watermark', true);
        $fields['image_watermark'] = array(
            'label' => 'Dodaj watermark na sliku',
            'input' => 'html',
            'html' => '<input class="watermarkImage" type="checkbox" id="attachments-' . $post->ID . '-image_watermark" name="attachments[' . $post->ID . '][image_watermark]" value="1"' . ($watermarkAdded ? ' checked="checked"' : '') . ' /> ',
            'value' => $watermarkAdded,
            'helps' => ''
        );
        return $fields;
    }



    public function addWatermarkToImageSave($post, $attachment) {
        global $imageOptimizer;
        if (isset($attachment['image_watermark'])) {
            update_post_meta($post['ID'], 'image_watermark', sanitize_text_field($attachment['image_watermark']));
            if ($attachment['image_watermark'] === '1') {
                $imageDuplicate = $imageOptimizer->makeImageDuplicate($post['ID']);
                $imageOptimizer->watermarkImage($imageDuplicate, $post['ID']);
            }
        } else {
            delete_post_meta($post['ID'], 'image_watermark');
            $imageOptimizer->undoWatermarkImage($post['ID']);
        }
        return $post;
        }


}