<?php

namespace GfWpPluginContainer\Wp;


class Cache
{
    const CACHE_KEY_DEV = 'si#dev#';

    const CACHE_KEY_PROD = 'si#prod#';

    /**
     * @var \Memcached|\Redis
     */
    private $engine;

    private $prefix;

    /**
     * Cache constructor.
     * @param $engine
     */
    public function __construct($engine)
    {
        $this->engine = $engine;
        $this->prefix = 'development';
        if (ES_INDEX_IMAGE === 'image') {
            $this->prefix = 'prod';
        }
    }

    public function get($key)
    {
        return $this->engine->get($this->prefix . $key);
    }

    public function set($key, $data, $ttl = 60)
    {
        if ($ttl) {
            $this->engine->set($this->prefix . $key, $data, $ttl);
            return;
        }
        $this->engine->set($this->prefix . $key, $data);
    }

    public function remove($key)
    {
        if($this->engine instanceof \Redis) {
            return $this->engine->del($key);
        }
        return $this->engine->delete($key);
    }

    public function flush()
    {
        if($this->engine instanceof \Redis) {
            return $this->engine->flushAll(); // careful, this always returns true
        }
        return $this->engine->flush();

    }
}