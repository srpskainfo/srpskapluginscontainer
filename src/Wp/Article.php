<?php

namespace GfWpPluginContainer\Wp;


class Article
{
    private $postId;

    private $category;

    private $createdAt;

    private $updatedAt;

    private $publishedAt;

    private $body;

    private $title;

    private $thumbnail;

    private $permalink;

    private $status;

    private $viewCount;

    private $synced;

    private $author;

    private $tags;

    private $type;

    /**
     * Article constructor.
     * @param $postId
     * @param $categories
     * @param $createdAt
     * @param $updatedAt
     * @param $publishedAt
     * @param $body
     * @param $thumbnail
     * @param $permalink
     * @param $status
     * @param $viewCount
     * @param $synced
     */
    public function __construct(
        $postId, $category, $createdAt, $updatedAt, $publishedAt, $body, $thumbnail, $permalink, $status, $viewCount,
        $title, $synced, $author, $tags, $type)
    {
        $this->postId = $postId;
        $this->category = $category;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->publishedAt = $publishedAt;
        $this->body = $body;
        $this->title = $title;
        $this->type = $type;
        $this->thumbnail = $thumbnail;
        $this->permalink = $permalink;
        $this->status = $status;
        $this->viewCount = $viewCount;
        $this->synced = $synced;
        $this->author = $author;
        $this->tags = $tags;
    }

    /**
     * Parse wp post to array
     *
     * @param \WP_Post $post
     * @return array
     */
    public static function WpPostToArray(\WP_Post $post)
    {
        $cats = [];
        foreach (wp_get_post_categories($post->ID) as $category_id) {
            $cat = get_category($category_id);
            if ($cat->parent === 0){
                $cat_lvl = 1;
            } else {
                if (get_category($cat->parent)->parent === 0){
                    $cat_lvl = 2;
                } else{
                    $cat_lvl = 3;
                }
            }
            $cats[] = [
                'id'    => $cat->term_id,
                'name'  => $cat->name,
                'slug'  => $cat->slug,
                'parent'=> $cat->parent,
                'level' => $cat_lvl
            ];
        }
        $authors = [];
        $authorService = new \GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors();
        /** @var $postOwner \GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner */
        foreach ($authorService->getOwnersForPost($post->ID) as $postOwner) {
            $authors[] = [
                'id'    => $postOwner->getAuthorId(),
                'name'  => $postOwner->getAuthorDisplayName()
            ];
        }
        $tags = [];
        foreach (wp_get_post_tags($post->ID) as $tag) {
            $tags[] = [
                'id'    => $tag->term_id,
                'name'  => $tag->name,
                'slug'  => $tag->slug
            ];
        }
        $thumbnail = '';
        if (has_post_thumbnail($post->ID)) {
            $thumbnail = get_the_post_thumbnail_url($post->ID, 'list-big');
        }
        $createdDt = new \DateTime($post->post_date, new \DateTimeZone('Europe/Sarajevo'));
        $updatedDt = new \DateTime(get_post_modified_time(get_option('date_format'), false, $post->ID), new \DateTimeZone('Europe/Sarajevo'));

        return [
            'postId' => $post->ID,
            'category' => $cats,
            'author' => $authors,
            'tag' => $tags,
            'title' => $post->post_title,
            'type' => $post->post_type,
            'createdAt' => $createdDt->format(\Datetime::ATOM),
            'updatedAt' => $updatedDt->format(\Datetime::ATOM),
            'publishedAt' => $createdDt->format(\Datetime::ATOM),
            'body' => $post->post_content,
            'thumbnail' => $thumbnail,
            'permalink' => get_permalink($post->ID),
            'status' => (int) ($post->post_status === 'publish'),
            'viewCount' => (int) get_post_meta($post->ID, 'gfPostViewCount', true),
        ];
    }

    public static function fromPost(\WP_Post $post)
    {
        return static::fromArray(static::WpPostToArray($post));
    }

    /**
     * ES result set data compatible
     *
     * @param $data
     * @return Article
     */
    public static function fromArray($data)
    {
        if (!isset($data['synced'])) {
            $data['synced'] = 0;
        }
        return new Article($data['postId'], $data['category'], $data['createdAt'], $data['updatedAt'], $data['publishedAt'],
            $data['body'], $data['thumbnail'], $data['permalink'], $data['status'], $data['viewCount'], $data['title'],
            $data['synced'], $data['author'], $data['tag'], $data['type']);
    }

    /**
     * @return mixed
     */
    public function getPostId()
    {
        return $this->postId;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        if($this->hasSecondLevelCategory()) {
            return $this->category;
        }
        return $this->category[0];
    }

    public function hasSecondLevelCategory()
    {
        return count($this->category) === 2;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @return mixed
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }

    /**
     * @return mixed
     */
    public function getSynced()
    {
        return $this->synced;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
