<?php


namespace GfWpPluginContainer\Wp\PushNotifications\Service;


use GfWpPluginContainer\Wp\WpEnqueue;

class Setup
{

    public function setup()
    {
        $this->hooksAndFilters();
        $this->enqueueJs();
        $this->enqueueCss();
//        $this->createTokensTable();
    }

    public static function isApp(): bool
    {
        return false !== strpos($_SERVER['REQUEST_URI'], 'mobile-home/') || strpos($_SERVER['REQUEST_URI'],
                '/app/') !== false;
    }

    private function enqueueJs()
    {
        WpEnqueue::addAdminScript('pushNotificationsAdminScript', 'push-notifications',
            PLUGIN_DIR_URI . 'src/Wp/PushNotifications/View/admin/js/main.js', [], microtime());

        if (!self::isApp()) {
            WpEnqueue::addFrontendScript('pushNotificationsFrontScript',
                PLUGIN_DIR_URI . 'src/Wp/PushNotifications/View/public/js/main.js', ['jquery', 'firebaseMessaging'],
                microtime());

            WpEnqueue::addFrontendScript('firebaseMain',
                'https://www.gstatic.com/firebasejs/8.4.2/firebase-app.js', [], microtime(), true);
            WpEnqueue::addFrontendScript('firebaseMessaging',
                'https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js', ['firebaseMain'], microtime(),
                true);
            WpEnqueue::addFrontendScript('firebaseAnalytics',
                'https://www.gstatic.com/firebasejs/7.16.1/firebase-analytics.js', ['firebaseMain'], microtime(),
                true);
        }
    }

    private function enqueueCss()
    {
        WpEnqueue::addAdminStyle('pushNotificationsAdminStyle', 'push-notifications',
            PLUGIN_DIR_URI . 'src/Wp/PushNotifications/View/admin/css/style.css',[], microtime());
        WpEnqueue::addFrontendStyle('pushNotificationsFrontStyle',
            PLUGIN_DIR_URI . 'src/Wp/PushNotifications/View/public/css/style.css', [], microtime());
    }

    private function hooksAndFilters()
    {
        add_action('admin_menu', [$this, 'pushNotificationsAdminMenu']);
    }

    public function pushNotificationsAdminMenu()
    {
        add_menu_page(
            __('Push Notifikacije', 'gf'),
            __('Push Notifikacije', 'gf'),
            'manage_options',
            'push-notifications',
            [$this, 'pushNotificationsView'],
            ''
        );
    }

    public function pushNotificationsView()
    {
        include_once(PLUGIN_DIR . '/src/Wp/PushNotifications/View/admin/template/pushNotifications.php');
    }

    public function createTokensTable(): void
    {
        global $wpdb;
        $tableName = $wpdb->prefix . 'webNotificationTokens';
        $sql = "create table IF NOT EXISTS $tableName(
	    id bigint auto_increment,
	    token varchar(255) not null,
	    createdAt DATETIME default CURRENT_TIMESTAMP null,
	    unsubscribedAt DATETIME,
		primary key (id))";
        $wpdb->query($sql);

        //Index
        $sql = "create unique index IF NOT EXISTS {$tableName}_token_uindex
	            on $tableName (token);";
        $wpdb->query($sql);
    }
}