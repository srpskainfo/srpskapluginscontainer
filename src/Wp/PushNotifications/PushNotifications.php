<?php


namespace GfWpPluginContainer\Wp\PushNotifications;


use GfWpPluginContainer\Wp\FbNotification;
use GfWpPluginContainer\Wp\PushNotifications\Service\Setup;


class PushNotifications
{
    /**
     * @var FbNotification
     */
    private $fbNotification;
    /**
     * @var Setup
     */
    private $setup;
    public function init()
    {
        $this->setup = new Setup();
        $this->setup->setup();
        $this->hooksAndFilters();
        $this->fbNotification = new FbNotification();
    }

    /**
     * Adds the ajax hooks for handling the form submission when pushing a notification
     */
    public function hooksAndFilters(): void
    {
        add_action( 'wp_ajax_handleNotificationPush', [$this, 'handleNotificationPush']);
        add_action( 'wp_ajax_populateFieldsBasedOnId', [$this, 'populateFieldsBasedOnId']);
        add_action('wp_ajax_subscribeToNotifications',[$this,'subscribeToNotifications']);
        add_action('wp_ajax_nopriv_subscribeToNotifications',[$this,'subscribeToNotifications']);
        add_action('wp_ajax_unSubscribeToNotifications',[$this,'unSubscribeToNotifications']);
        add_action('wp_ajax_nopriv_unSubscribeToNotifications',[$this,'unSubscribeToNotifications']);

        // Update the notification history when a notification is sent through the old notification form
        add_action('wp_ajax_updateNotificationHistory',[$this,'updateNotificationHistory']);
        add_action('wp_ajax_nopriv_updateNotificationHistory',[$this,'updateNotificationHistory']);
        if(!$this->setup::isApp()) {
            add_action('wp_footer', [$this, 'renderNotificationBellIcon']);
        }

        //get notification history html for bell
        add_action('wp_ajax_getNotificationHistory',[$this,'getNotificationHistoryHtml']);
        add_action('wp_ajax_nopriv_getNotificationHistory',[$this,'getNotificationHistoryHtml']);
    }

    /**
     * The function which is called when the form is submitted when pushing a notification
     */
    public function handleNotificationPush()
    {
        $topic = NOTIFICATION_TOPIC;
        $icon = 'https://srpskainfo.com/wp-content/uploads/2021/06/apple-touch-icon-120x120-1.png';
        $title = stripslashes($_POST['title']);
        $subtitle = stripslashes($_POST['subtitle']);
        $image = $_POST['image'];
        $url = $_POST['actionUrl'];

        // App params

        // If there is no content, use the title and the subtitle as the body
        $content = $_POST['content'] ?? '';
        if($content === '') {
            $content = $title . PHP_EOL . $subtitle;
        }
        $auth = $_POST['auth'];
        $segment = $_POST['segment'];


        $this->fbNotification->sendPushToTopic($topic,$title,$subtitle,$image,$icon,$url);
        $this->sendPushToApp($content,$auth,$segment,$url);
        die();
    }

    public function subscribeToNotifications()
    {
        $topic = NOTIFICATION_TOPIC;
        $this->fbNotification->subscribeTokenToTopic($_POST['token'],$topic);
        die();
    }

    public function unSubscribeToNotifications()
    {
        $topic = NOTIFICATION_TOPIC;
        $this->fbNotification->unsubscribeTokenToTopic($_POST['token'],$topic);
        die();
    }

    /**
     * The function which is called when the user inputs a post ID, which then generates the image and title values
     * in the admin form
     */
    public function populateFieldsBasedOnId()
    {
        $post = get_post($_POST['postId']);
        if (!$post) {
            echo json_encode(['message' => 'Post not found']);
            die();
        }
        if($post->post_status !== 'publish') {
            echo json_encode(['message' => 'Post is not published']);
            die();
        }
        echo json_encode([
            'title' => $post->post_title,
            'image' => wp_get_attachment_image_url(get_post_thumbnail_id($_POST['postId']), 'large'),
            'url' => get_permalink($_POST['postId'])
        ]);
        die();
    }

    public function renderNotificationBellIcon()
    {
        include_once(PLUGIN_DIR . '/src/Wp/PushNotifications/View/public/template/notificationBellIcon.php');
    }

    public function sendPushToApp($content,$auth,$segment,$url): void
    {
        if(ES_INDEX_ARTICLE === 'article') { // Only on production
            if ($auth === 'd9HEiaCd5cnh6t6F7RZo') {
                $fields = [
                    'app_id' => 'b41623da-e2f2-4bb7-ad9b-3be7f12254c8',
                    'included_segments' => array($segment),
                    'contents' => array("en" => $content),
                    'headings' => array("en" => "Srpskainfo"),
                    'large_icon' => 'https://i.imgur.com/8OYxRwM.png',
                    'url' => $url . '?utm_source=appPush',
                ];

                $fields = json_encode($fields);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Authorization: Basic M2JkZGIzZjctMDExYi00ZmFmLWFjMGQtMTI3OTZkMTgyMzM1'
                ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $response = curl_exec($ch);
                curl_close($ch);

            }
        }
    }

    public function updateNotificationHistory()
    {
        global $cache;
        $dt = new \DateTime('now',new \DateTimeZone('Europe/Sarajevo'));
        $postId = url_to_postid($_GET['postUrl']);
        $post = get_post($postId);
        $history = unserialize($cache->get('notificationHistory'), ['allowed_classes' => false]) ?? [];
        $data = [
                'title' => $post->post_title,
                'body' => '',
                'image' => get_the_post_thumbnail_url($post),
                'click_link' => $_GET['postUrl'],
                'dateSent' => $dt->getTimestamp(),
        ];
        $history[] = $data;
        if (count($history) > 10) {
            unset($history[0]);
            $history = array_values($history);
        }

        $cache->set('notificationHistory',serialize($history),0);
        die();
    }

    public function getNotificationHistoryHtml()
    {
        global $cache;
        $history = [];
        if ($cache->get('notificationHistory')){
            $history = unserialize($cache->get('notificationHistory'));
        }
        $count = count($history);
        if ($count > 3) {
            $offset = $count - 3;
            //We are showing only last 3
            $last3 = array_reverse(array_splice($history, $offset),true);
        } else {
            $last3 = array_reverse($history);
        }

        ob_start();
        if(count($last3) > 0) :?>
        <h3 class="mainTitle">Najvažnije vijesti</h3>
        <?php foreach ($last3 as $notification):
            ?>
            <div class="notificationPost">
                <a href="<?=$notification['click_link']?>" title="<?=$notification['title']?>">
                    <div>
                        <img class="postImage" src="<?=$notification['image']?>" width="105" height="72" alt="News image">
                    </div>
                    <div>
                        <span class="notificationTitle"><?=$notification['title']?></span>
                        <span class="notificationTitle"><?= $notification['body']?></span>
                    </div>
                </a>
            </div>
        <?php endforeach;?>
        <?php endif;
        wp_send_json(ob_get_clean());
    }
}