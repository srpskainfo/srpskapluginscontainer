<?php


namespace GfWpPluginContainer\Wp\PushNotifications\Actions;


use GfWpPluginContainer\Wp\Logger;

class SaveTokenToDatabase
{
    /**
     * @param string $token
     * @return int
     * @throws \Exception
     */
    public static function saveToken(string $token): int
    {
         global $wpdb;
         if ($wpdb->insert($wpdb->prefix.'webNotificationTokens', ['token' => $token], ['%s'])){
             return $wpdb->insert_id;
         }
         $msg = sprintf('Error saving token: %s to database',$token);
         $dt = new \DateTime('now',new \DateTimeZone('Europe/Belgrade'));
         Logger::webNotificationLog('['.$dt->format('m/d/y h:i:s').']     '.$msg );
         throw new \Exception($msg);
    }
}