<?php


namespace GfWpPluginContainer\Wp\PushNotifications\Actions;


use GfWpPluginContainer\Wp\Logger;

class MarkTokenAsUnsubscribed
{
    /**
     * @param string $token
     * @return bool
     * @throws \Exception
     */
    public static function markTokenAsUnsubscribed(string $token = '', int $id = null): bool
    {
        global $wpdb;
        $field = 'id';
        if ($token !== '' && $id === null) {
            $field = 'token';
        }
        if ($token !== '' || $id) {
            $time = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
            if ($wpdb->update($wpdb->prefix.'webNotificationTokens', ['unsubscribedAt' => $time->format('Y-m-d H:i:s')],
                [$field => $token])){
                return true;
            }
            $value = $id;
            if ($field === 'token') {
                $value = $token;
            }
            $msg = sprintf('Error updating table where field %s = %s. Tried to insert value unsubscribedAt = %s',
                $field, $value, $time->format('Y-m-d H:i:s'));
            $dt = new \DateTime('now',new \DateTimeZone('Europe/Belgrade'));

            Logger::webNotificationLog('['.$dt->format('Y-m-d H:i:s').']     '.$msg );
            throw new \Exception($msg);
        }

        throw new \Exception('Token or id cannot be empty');
    }
}