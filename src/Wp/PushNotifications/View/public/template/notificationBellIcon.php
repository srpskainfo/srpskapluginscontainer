<div class="notificationBell">
    <i class="far fa-bell fa-lg"></i>
    <div id="notificationContainer">
        <i id="closeHistory" class="fas fa-times-circle"></i>
        <span class="subUnsub"><?=__('Prihvati notifikacije', 'gf')?></span>
    </div>
</div>
