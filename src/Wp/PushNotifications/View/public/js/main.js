const firebaseConfig = {
    apiKey: "AIzaSyCWh_rJP5mg8N6e8fbvt-Vv83SJfWq02IE",
    authDomain: "srpskainfo-38a4b.firebaseapp.com",
    appId: "1:302209566102:web:e0da267b768f1089b8c9b7",
    measurementId: "G-74LZR5M2TX",
    projectId: "srpskainfo-38a4b",
    storageBucket: "srpskainfo-38a4b.appspot.com",
    messagingSenderId: "302209566102"
};
let app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

//Sub unsub button inside of the notification bell
const subUnsub = jQuery('.subUnsub');


jQuery(document).ready(function () {
    let populate = false;
    let bell = jQuery('.notificationBell');
    handleForegroundMessages();
    populateNotificationHistory();


    /*
     If the cookie for the token is not set and we didn't try to prompt the notification,
     prompt the notification consent popup.
     */
    if (!getCookie('fbToken')) {
        if (!getCookie('fbNotificationTry')) {
            subUnsub.text('Prihvati notifikacije')
            if (navigator.userAgent.indexOf("Firefox") === -1) {
                promptNotificationsConsent();
            } else { // Firefox is a special snowflake so we need to treat it accordingly.
                showHistoryContainer(bell);
            }
        } else {
            subUnsub.text('Prihvati notifikacije')
            let cookieData = JSON.parse(getCookie('fbNotificationTry'));
            if (cookieData.unsub !== true) {
                messaging.getToken({vapidKey: 'AIzaSyCWh_rJP5mg8N6e8fbvt-Vv83SJfWq02IE'})
                    .then((currentToken) => {
                        subUnsub.text('Otkaži notifikacije')
                        setTokenCookie(currentToken);
                        eraseCookie('fbNotificationTry')
                    }).catch((err) => {
                    //We check if we should remind the user to subscribe
                    if (checkIfCookieIntervalPassed()) {
                        let cookieData = JSON.parse(getCookie('fbNotificationTry'));
                        updateCookieToNextInterval();
                        //if the user didn't unsub via our unsub button we remind him that he can subscribe via the browser popup
                        if (cookieData.unsub !== true) {
                            promptNotificationsConsent();
                        } else {//If the user unsub via our button we can't use the browser's default popup so we toggle the notification bell
                            showHistoryContainer(bell);
                        }
                    }
                })
            } else {
                if (checkIfCookieIntervalPassed()) {
                    let cookieData = JSON.parse(getCookie('fbNotificationTry'));
                    updateCookieToNextInterval();
                    //if the user didn't unsub via our unsub button we remind him that he can subscribe via the browser popup
                    if (cookieData.unsub !== true) {
                        promptNotificationsConsent();
                    } else {//If the user unsub via our button we can't use the browser's default popup so we toggle the notification bell
                        showHistoryContainer(bell);
                    }
                }
            }

        }
    } else {
        checkForTokenOrUnsubUserThatBlockedUs()
    }

    // Subscribe/Unsubscribe the user
    subUnsub.on('click', function () {
        if (getCookie('fbToken')) {
            deleteToken(JSON.parse(getCookie('fbToken')).token)
            subUnsub.text('Prihvati notifikacije')
        } else {
            promptNotificationsConsent();
        }
    })

    bell.on('click', function () {
        if (!bell.hasClass('clicked')) {
            showHistoryContainer(bell);
        } else {
            hideHistoryContainer(bell);
        }
    })

    jQuery('#closeHistory').on('click', function (e) {
        e.stopPropagation();
        hideHistoryContainer(bell);
    })

    function showHistoryContainer(bell) {
        bell.css('opacity', '1').addClass('clicked');
        jQuery('.fa-bell').css({width: '30px', height: '30px'});
        jQuery('#notificationContainer').css('transform', 'scale(1)');
    }

    function hideHistoryContainer(bell) {
        bell.removeClass('clicked').css('opacity', '');
        jQuery('.fa-bell').css({width: '', height: ''});
        jQuery('#notificationContainer').css('transform', '');
    }

    /*
     Updates the database with unsub date, creates the cookie so we can remind the user that he can sub again
     and deletes the token cookie.
     */
    function unsubAjax(token) {
        jQuery.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                action: 'unSubscribeToNotifications',
                token: token,
            },
            success: function (data) {
                let dt = new Date();
                dt.setTime(dt.getTime() + (7 * 24 * 60 * 60 * 1000))
                let cookieData = {
                    time: dt.getTime(),
                    interval: 0,
                    days: 7,
                    unsub: true,
                }
                setNotificationCookie(cookieData)
                eraseCookie('fbToken');
                localStorage.removeItem('notificationHistory')
            },
            error: function (data) {
            },
        });
    }
    function populateNotificationHistory(){
        jQuery.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                action: 'getNotificationHistory',
            },
            success: function (data) {
                jQuery('.subUnsub').before(data)
            },
            error: function (data) {
            },
        });
    }
    //Deletes the token in the fb database and sends an ajax to unsub that token in our database
    function deleteToken(token) {
        messaging.deleteToken()
            .then((promise) => {
                if (promise) {
                    unsubAjax(token)
                }
            })
    }

    /*
        On each page load when the user has the token stored in the cookie we need to check if we still have permissions to send
        notifications. if we don't, that means the user blocked us and we should unsub that token in our database.
     */
    function checkForTokenOrUnsubUserThatBlockedUs() {
        messaging.getToken({vapidKey: 'AIzaSyCWh_rJP5mg8N6e8fbvt-Vv83SJfWq02IE'})
            .then((currentToken) => {
                subUnsub.text('Otkaži notifikacije')
            })
            .catch((err) => {
                subUnsub.text('Prihvati notifikacije')
                if (err.code === 'messaging/permission-blocked') {
                    unsubAjax(JSON.parse(getCookie('fbToken')).token)
                }
            })
    }

    /*
       Create the token entry in our database, sets the cookie with the token value
       and erases the cookie that handles the retry intervals
       as we don't need it anymore because the user subbed for the notifications
     */
    function subAjax(token) {
        jQuery.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                action: 'subscribeToNotifications',
                token: token,
            },
            success: function (data) {
                setTokenCookie(token);
                eraseCookie('fbNotificationTry')
            },
            error: function (data) {
            },
        });
    }

    /*
       Prompts the notification consent popup, if the user allows it we start the sub process.
       If not, check if we should update the interval when we remind him,
       or just to set the interval to the default value
     */
    function promptNotificationsConsent() {
        messaging.getToken({vapidKey: 'AIzaSyCWh_rJP5mg8N6e8fbvt-Vv83SJfWq02IE'})
            .then((currentToken) => {
                if (currentToken) {
                    subAjax(currentToken)
                    subUnsub.text('Otkaži notifikacije')
                }
            })
            .catch((err) => {
                if (getCookie('fbNotificationTry')) {
                    if (checkIfCookieIntervalPassed()) {
                        updateCookieToNextInterval()
                    }
                } else {
                    setNotificationCookie();
                }
            })
    }
})

function handleForegroundMessages() {
    messaging.onMessage(function (payload) {
        let title = payload.data.title;
        let body = payload.data.body;
        let image = payload.data.image;
        let url = payload.data.click_link;

        const notificationTitle = title;
        const notificationOptions = {
            body: body,
            icon: payload.data.icon,
            image: image,
            requireInteraction: payload.data.requireInteraction,
            actions: [{action: "open_url", title: "Pročitaj odmah"}],
            badge: 'https://srpskainfo.com/wp-content/uploads/2021/06/Si-logo-48x48px.png',
            data: {
                url: url
            }
        };
        navigator.serviceWorker
            .getRegistrations()
            .then((registration) => {
                //Updates worker after each message to check if there where changes in worker file
                registration[0].update();
                registration[0].showNotification(notificationTitle, notificationOptions);
            });
    });
}

//Sets the retry notification cookie with the default value of 7 days
function setNotificationCookie(data = null) {
    let cookieName = 'fbNotificationTry';
    let expires = "";
    let date = new Date();
    //Expiry date is set to 365 days
    date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
    //First we set its value to 7 days if there is no data
    if (data === null) {
        let dt = new Date();
        dt.setTime(dt.getTime() + (7 * 24 * 60 * 60 * 1000))
        data = {
            time: dt.getTime(),
            interval: 0,
            days: 7
        }
    }
    document.cookie = cookieName + "=" + (JSON.stringify(data)) + expires + "; path=/";
}

//Sets the token cookie
function setTokenCookie(token) {
    let cookieName = 'fbToken';
    let expires = "";
    let date = new Date();

    //Expiry date is set to 10 years
    date.setTime(date.getTime() + (3650 * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
    document.cookie = cookieName + "=" + (JSON.stringify({token: token})) + expires + "; path=/";
}

//Checks if the interval passed so we can try to remind the user that he can subscribe to the web notifications
function checkIfCookieIntervalPassed() {
    let cookieData = JSON.parse(getCookie('fbNotificationTry'));
    return cookieData.time < new Date().getTime();
}

/*
    Updates the cookie value to the next interval
 */
function updateCookieToNextInterval() {
    let intervals = [30, 180, 365]
    let cookieData = JSON.parse(getCookie('fbNotificationTry'));
    let unsub = cookieData.unsub ?? false;
    if (cookieData.interval >= 3) {
        cookieData.interval = 0;
    }
    let days = intervals[cookieData.interval];
    let dt = new Date();
    dt.setTime(dt.getTime() + (days * 24 * 60 * 60 * 1000))
    let data = {
        time: dt.getTime(),
        interval: cookieData.interval + 1,
        days: days,
        unsub: unsub
    }
    setNotificationCookie(data);
}

function getCookie(name) {
    const nameEQ = name + "=";
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}