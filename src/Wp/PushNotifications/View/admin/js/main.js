jQuery(document).ready(function (){
    const form = jQuery('#notificationPush');
    const postIdInput = jQuery('.postId');
    const titleInput = jQuery('.title');
    const subtitleInput = jQuery('.subtitle');

    form.keydown(function (e){
        if (e.keyCode === 13) {
            e.preventDefault();
            return false;
        }
    })
    // Ajax for handling the notification push form
    form.on('submit',function(e){
        e.preventDefault();
        jQuery('#notificationPush :submit').attr("disabled", "disabled").css('background', 'silver');
        if (postIdInput.val() !== ''){
            jQuery.ajax({
                url: 'admin-ajax.php',
                type: 'POST',
                data: {
                    action: 'handleNotificationPush',
                    title: jQuery('.title').val(),
                    subtitle: jQuery('.subtitle').val(),
                    actionUrl: jQuery('.url').val(),
                    image: jQuery('.image').val(),
                    content: jQuery('#content').val(),
                    segment: jQuery('#segment').val(),
                    auth: jQuery('#auth').val(),
                },
                success: function (response) {
                    alert('Notifikacija je uspešno poslata')
                    location.reload();
                },
                error: function () {
                    alert('Dogodila se neočekivana greška')
                }
            });
        } else {
            alert('Morate uneti ID posta')
        }
    });

    // Populate fields based on the post id on change

    postIdInput.on('input',function(){
        if(jQuery(this).val().length < 4) {
            return;
        }
        jQuery.ajax({
            url: 'admin-ajax.php',
            type: 'POST',
            data: {
                action: 'populateFieldsBasedOnId',
                postId: postIdInput.val()

            },
            success: function (response) {
                let res = JSON.parse(response);
                let title = jQuery('.title');
                let url = jQuery('.url');
                let image = jQuery('.image');
                let imageWrapper = jQuery('.imageWrapper');
                if(res.message === 'Post is not published') {
                    title.val('');
                    url.val('');
                    image.val('');
                    imageWrapper.attr('src','https://via.placeholder.com/374x250?text=Istaknuta+Slika');
                    postIdInput.val('');
                    alert('Članak nije objavljen!')
                    return;
                }
                title.val(res.title);
                validateInputValueLength(title,34,'title');
                url.val(res.url);
                imageWrapper.attr('src', res.image);
                image.val(res.image);
            },
            error: function () {
            }
        });
    });

    titleInput.on('input',function(){
        validateInputValueLength(jQuery('.title'),34,'title');
    })
    subtitleInput.on('input',function(){
        validateInputValueLength(jQuery('.subtitle'),39,'subtitle');
    })

    function validateInputValueLength(elem,max,classPrefix) {
        let maxCharWarning = jQuery('.' +classPrefix + '-maxCharWarning');
        if(elem.val().length > max) {
            elem.css('border-color','red');
            if(maxCharWarning.length === 0) {
                elem.parent().append(`<span class="${classPrefix}-maxCharWarning" style="color:red;">Maksimalan broj karaktera je dostignut.</span>`);
            }
            return;
        }
        elem.css('border-color','#8c8f94');
        maxCharWarning.remove();
        jQuery('#notificationPush :submit').removeAttr('disabled').css('background', '#007cba');
    }
})