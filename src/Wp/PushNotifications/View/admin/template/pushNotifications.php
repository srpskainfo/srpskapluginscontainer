<?php
global $cache;

use Carbon\Carbon;

$notificationHistory = [];
if ($cache->get('notificationHistory')){
    $notificationHistory = array_reverse(unserialize($cache->get('notificationHistory')));
}

Carbon::setLocale('bs');

?>
<h2><?=__('Slanje Notifikacija','gfPushNotifications')?></h2>
<div id="mainWrapper">
    <form method="post" action="" id="notificationPush">
        <label>
            <span><?= __('ID posta','gfPushNotifications') ?></span>
            <input autocomplete="off" type="text" size="200" name="postId" class="postId"/>
        </label>
        <label>
            <span><?= __('Naslov','gfPushNotifications') ?></span>
            <input autocomplete="off"  type="text" size="200" name="title" class="title"/>
        </label>
        <label>
            <span><?= __('Podnaslov','gfPushNotifications') ?></span>
            <input autocomplete="off" type="text" size="200" name="subtitle" class="subtitle"/>
        </label>

        <input type="hidden" size="200" name="url" class="url"/><br/>
        <input type="hidden" size="200" name="image" class="image"/><br/>

        <!-- App params   -->
        <h3>Sadržaj poruke za aplikaciju.<br>
            Ako ne popunite ovo polje, naslov i podnaslov će biti sadržaj poruke.</h3>
        <label>
            <textarea placeholder="Sadržaj poruke..." name="content" id="content" cols="45" rows="8"></textarea>
        </label>
        <input type="hidden" id="segment" value="app" name="segment">
        <input type="hidden" id="auth" name="auth" value="d9HEiaCd5cnh6t6F7RZo"><br />

        <img src="http://via.placeholder.com/374x250?text=Istaknuta+Slika" class="imageWrapper" alt="Featured image"/>

        <input class="sendNotification" type="submit" value="<?=__('Pošalji','gfPushNotifications')?>"/>
    </form>
    <div id="notificationHistoryWrapper">
        <h3><?=__('Istorija notifikacija','gfPushNotifications')?></h3>
        <?php foreach ($notificationHistory as $notification):
            $date = new Carbon($notification['dateSent'], new \DateTimeZone('Europe/Sarajevo'));
            ?>
            <div class="notification">
                <div>
                    <img src="<?=$notification['image']?>" width="105" height="72" alt="News image">
                </div>
                <div>
                    <h4><?=$notification['title']?></h4>
                    <p><?=$notification['body']?></p>
                    <p>Poslato prije <?=$date->longAbsoluteDiffForHumans()?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
