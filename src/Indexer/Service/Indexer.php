<?php

namespace GfWpPluginContainer\Indexer\Service;

use \Elastica\Index;


class Indexer
{
    /**
     * @var Index
     */
    private $elasticaIndex;

    public function __construct(Index $elasticaIndex)
    {
        $this->elasticaIndex = $elasticaIndex;
    }

    public function deletePost(\WP_Post $article)
    {
        try {
            $response = $this->elasticaIndex->getType(ES_INDEX_ARTICLE)->deleteById($article->ID);
            if (!$response->isOk() || $response->hasError()) {
//                throw new \Exception($response->getError());
            }
            $this->elasticaIndex->refresh();
        } catch (\Exception $e) {
//            throw $e;
        }
        return true;
    }

    public function indexPost(\WP_Post $article)
    {
        try {
            $response = $this->elasticaIndex->getType(ES_INDEX_ARTICLE)->addDocument($this->parseWpPost($article));
            if (!$response->isOk() || $response->hasError()) {
                var_dump($response->getError());
                die();
            }
            unset($response);
            $this->elasticaIndex->refresh();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return true;
    }

    public function indexImage(\WP_Post $article)
    {
        try {
            $response = $this->elasticaIndex->getType(ES_INDEX_IMAGE)->addDocument($this->parseWpImage($article));
            if (!$response->isOk() || $response->hasError()) {
                var_dump($response->getError());
                die();
            }
            unset($response);
            $this->elasticaIndex->refresh();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return true;
    }

    public function indexImages()
    {
        global $wpdb;

        $perPage = 1000;
        $totalItems = 0;
        for ($i = 1; $i < 200; $i++) {
            $args = [
                'numberposts' => $perPage,
                'paged' => $i,
                'status' => 'publish',
                'post_type' => 'attachment',
                'orderby' => 'date',
                'order' => 'DESC',
            ];
            $posts = get_posts($args);
            $wpdb->flush();
            if (count($posts) > 0) {
                $documents = [];
                foreach ($posts as $post) {
                    if ($post->post_name === "AUTO-DRAFT") {
                        continue;
                    }
//                    $totalItems++;
                    $documents[] = $this->parseWpImage($post);
                }
                try {
                    $response = $this->elasticaIndex->getType(ES_INDEX_IMAGE)->addDocuments($documents);
                    if (!$response->isOk() || $response->hasError()) {
                        var_dump($response->getError());
                        die();
                    }
                    $totalItems += count($documents);
                    $documents = []; // reset var, clear memory
                    $this->elasticaIndex->refresh();
                    echo sprintf('stored %s items.', $totalItems);
                    unset($response);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            } else {
                break;
            }
        }
        echo "sync complete. $totalItems items indexed.";
    }

    /**
     * Batch index
     */
    public function indexAll()
    {
        global $wpdb;

        $perPage = 3000;
        $totalItems = 0;
        for ($i = 1; $i < 100; $i++) {
            $args = [
                'numberposts' => $perPage,
                'paged' => $i,
                'status' => 'publish',
                'post_type' => 'post',
                'orderby' => 'date',
                'order' => 'DESC',
            ];
            $posts = get_posts($args);
            $wpdb->flush();
            if (count($posts) > 0) {
                $documents = [];
                foreach ($posts as $post) {
                    if ($post->post_name === "AUTO-DRAFT") {
                        continue;
                    }
//                    $totalItems++;
                    $documents[] = $this->parseWpPost($post);
                }
                try {
                    $response = $this->elasticaIndex->getType(ES_INDEX_ARTICLE)->addDocuments($documents);
                    if (!$response->isOk() || $response->hasError()) {
                        var_dump($response->getError());
                        die();
                    }
                    $totalItems += count($documents);
                    $documents = []; // reset var, clear memory
                    $this->elasticaIndex->refresh();
                    echo sprintf('stored %s items.', $totalItems);
                    unset($response);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            } else {
                break;
            }
        }
        echo "sync complete. $totalItems items indexed.";
    }

    private function parseWpImage(\WP_Post $post)
    {
        global $wpdb;

        $author = get_user_by('id', $post->post_author);
        $user = [
            'id'    => $author->ID,
            'name'  => $author->user_nicename
        ];
        $createdDt = new \DateTime($post->post_date, new \DateTimeZone('Europe/Sarajevo'));
        $data = [
            'postId' => $post->ID,
            'author' => $user,
            'title' => str_replace('-',' ', $post->post_title),
            'createdAt' => $createdDt->format(\Datetime::ATOM),
            'potpis' => wp_get_attachment_caption($post->ID),
            'legenda' => get_post_meta($post->ID,'legenda',true),
            'filename' => $post->post_name,
            'synced' => 1,
        ];

        return new \Elastica\Document($post->ID, $data);
    }

    private function parseWpPost(\WP_Post $post)
    {
        $data = \GfWpPluginContainer\Wp\Article::WpPostToArray($post);
//        $categories = $data['category'];
//        foreach($categories as $category) {
//            if($category['parent'] > 0) {
//                $data['category'] = [];
//                $data['category'][] = $category;
//                break;
//            }
//        }
        $data['search_data']['full_text'] = $this->mergeTextFields($data['title'], $data['body'], $data['category']);

        return new \Elastica\Document($post->ID, $data);
    }

    /**
     * @param $title
     * @param $body
     * @param $cats
     * @param null $tags
     * @return string
     */
    private function mergeTextFields($title, $body, $cats, $tags = null)
    {
        $text = $title;
        $text .= ' ' . $body;
        $categories = '';
        if (count($cats) > 0) {
            foreach ($cats as $cat) {
                $categories .= ', '. $cat['name'];
            }
        }
        $text .= ' '. $categories;

        return $text;
    }
}
