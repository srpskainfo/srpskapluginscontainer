<?php

namespace GfWpPluginContainer\Indexer\Repository;

use GfWpPluginContainer\Wp\Article as ArticleModel;
use GfWpPluginContainer\Indexer\Factory\Mapper as MapperFactory;
use GfWpPluginContainer\Indexer\Mapper\Article as ArticleMapper;

class Article
{
    public static function relatedByTags($tags, $postDate, $restrictedPosts, $limit = 5, $sort = false, $catId = false)
    {
        $mapper = static::createMapper();

//        $dt = new \DateTime('now', new \DateTimeZone('Europe/Sarajevo'));
//        $mapper->relatedByTags($tags, $catId, $dt->format('Y-m-d H:i:s'), $restrictedPosts, $limit);
//        if ($mapper->getResultSet()->count() < $limit) { // not enough recent items, try with post date
            $mapper->relatedByTags($tags, $catId, $postDate, $restrictedPosts, $limit, $sort);


//        if ($mapper->getResultSet()->count() < $limit) { // not enough related items, try with category items
//            $articles = [];
//            foreach ($mapper->getResultSet() as $result) {
//                $articles[] = ArticleModel::fromArray($result->getData());
//            }
//            $mapper->searchLatestPopularFromCategory($catId, $limit - $mapper->getResultSet()->count());
//            foreach ($mapper->getResultSet() as $result) {
//                $articles[] = ArticleModel::fromArray($result->getData());
//            }
//
//            return ['totalCount' => $mapper->getResultSet()->getTotalHits(), 'articles' => $articles];
//        }

        return static::parseResultsFromMapper($mapper);
    }

    public static function relatedByTagsFromCategory($tags, $catId, $postDate, $restrictedPosts, $limit = 5)
    {
        $mapper = static::createMapper();

//        $dt = new \DateTime('now', new \DateTimeZone('Europe/Sarajevo'));
//        $mapper->relatedByTags($tags, $catId, $dt->format('Y-m-d H:i:s'), $restrictedPosts, $limit);
//        if ($mapper->getResultSet()->count() < $limit) { // not enough recent items, try with post date
        $mapper->relatedByTags($tags, $catId, $postDate, $restrictedPosts, $limit);
        if ($mapper->getResultSet()->count() < $limit) { // not enough related items, try with category items
            $articles = [];
            foreach ($mapper->getResultSet() as $result) {
                $articles[] = ArticleModel::fromArray($result->getData());
            }
            $mapper->searchLatestPopularFromCategory($catId, $limit - $mapper->getResultSet()->count());
            foreach ($mapper->getResultSet() as $result) {
                $articles[] = ArticleModel::fromArray($result->getData());
            }

            return ['totalCount' => $mapper->getResultSet()->getTotalHits(), 'articles' => $articles];
        }

        return static::parseResultsFromMapper($mapper);
    }

    /**
     * Returns article list from ES
     *
     * @param $type
     * @param $term
     * @param $currentPage
     * @param int $perPage
     * @return array
     */
    public static function getItemsFromElasticBy($type, $term, $currentPage, $perPage = PER_PAGE, $restrictedPosts = null)
    {
        $mapper = static::createMapper();
        if ($type === 'author') {
            $termId = $term->ID;
        } else {
            $termId = $term->term_id;
        }
        $mapper->searchByType($type, $termId, $perPage, $currentPage, $restrictedPosts);

        return static::parseResultsFromMapper($mapper);
    }

    public static function elasticSearch($input, $page = 1, $perPage = PER_PAGE)
    {
        $mapper = static::createMapper();
        $mapper->search($input, $perPage, $page);

        return static::parseResultsFromMapper($mapper);
    }

    /**
     * Returns article list from db (via get_posts). Very resource intensive.
     *
     * @param $params
     * @return array
     */
    public static function getItemsFromWp($params)
    {
        $posts = get_posts($params);
        $lastPage = 0;
        $articles = [];
        if ($posts) {
            $lastPage = $posts->max_num_pages;
            /* @var \WP_Post $post */
            foreach ($posts as $post) {
                $permalink = get_permalink($post->ID);
                $thumbnailSrc = get_the_post_thumbnail_url($post->ID);
                $authorName = get_the_author_meta('display_name', $post->post_author);
                $cat = get_category($post->post_category[0]);
                $articles[] = ArticleModel::fromArray([
                    'postId' => $post->ID,
                    'postType' => $post->post_type,
                    'category' => [['name' => $cat->name, 'slug' => $cat->slug]],
                    'author' => $authorName,
                    'synced' => 1,
                    'title' => $post->post_title,
                    'createdAt' => strtotime($post->post_date),
                    'updatedAt' => get_post_modified_time(get_option('date_format'), false, $post->ID),
                    'publishedAt' => get_the_date(get_option('date_format'), $post->ID),
                    'body' => $post->post_content,
                    'thumbnail' => $thumbnailSrc,
                    'permalink' => $permalink,
                    'viewCount' => get_post_meta($post->ID, 'gfPostViewCount', true),
                    'status' => (int) ($post->post_status === 'publish'),
                ]);
            }
        }

        return ['totalCount' => $lastPage * PER_PAGE, 'articles' => $articles];
    }

    /**
     * @return ArticleMapper
     */
    public static function createMapper()
    {
        return MapperFactory::make(ES_INDEX_ARTICLE);
    }

    public static function parseResultsFromMapper(ArticleMapper $mapper)
    {
        $articles = [];
        foreach ($mapper->getResultSet() as $result) {
            $articles[] = ArticleModel::fromArray($result->getData());
        }
        return ['totalCount' => $mapper->getResultSet()->getResponse()->getData()['hits']['total']['value'], 'articles' => $articles];
    }

    /**
     * Sync post data to elastic on save.
     *
     * @param \WP_Post $post
     */
    public static function syncItemToElastic(\WP_Post $post) {
        if ($post && strtolower($post->post_status) !== 'auto-draft' && strtolower($post->post_name) !== 'auto-draft'
            && $post->post_type === 'post') {
            $indexer = new \GfWpPluginContainer\Indexer\Service\Indexer(
                \GfWpPluginContainer\Indexer\Factory\ElasticClientFactory::make()->getIndex(ES_INDEX_ARTICLE)
            );
            $indexer->indexPost($post);
        }
    }

    public static function deleteItemFromElastic(\WP_Post $post) {
        $indexer = new \GfWpPluginContainer\Indexer\Service\Indexer(
            \GfWpPluginContainer\Indexer\Factory\ElasticClientFactory::make()->getIndex(ES_INDEX_ARTICLE)
        );
        $indexer->deletePost($post);
    }
}