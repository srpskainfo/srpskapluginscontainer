<?php

namespace GfWpPluginContainer\Indexer\Factory;

class Mapper
{
    public static function make($indexName, $debug = false)
    {
        $client = \GfWpPluginContainer\Indexer\Factory\ElasticClientFactory::make();
        $search = new \Elastica\Search($client);
        $search->addIndex($indexName);
        $className = '\\GfWpPluginContainer\\Indexer\\Mapper\\' . ucfirst(str_replace('-test', '', $indexName));

        return new $className($search, $debug);
    }

}