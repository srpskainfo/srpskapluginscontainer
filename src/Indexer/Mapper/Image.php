<?php

namespace GfWpPluginContainer\Indexer\Mapper;

use Elastica\Query\BoolQuery;

class Image extends ElasticBase
{
    public function search($keywords, $limit = 0, $currentPage = 1)
    {
        $boolQuery = new BoolQuery();

        if (strlen($keywords)) {
            $q = new \Elastica\Query\Match();
            $q->setFieldQuery('title', $keywords);
            $q->setFieldOperator('title', 'and');
            $q->setFieldFuzziness('title', 0);
            $boolQuery->addShould($q);

            $q = new \Elastica\Query\Match();
            $q->setFieldQuery('potpis', $keywords);
            $q->setFieldOperator('potpis', 'and');
            $q->setFieldFuzziness('potpis', 0);
            $boolQuery->addShould($q);

            $q = new \Elastica\Query\Match();
            $q->setFieldQuery('legenda', $keywords);
            $q->setFieldOperator('legenda', 'and');
            $q->setFieldFuzziness('legenda', 0);
            $boolQuery->addShould($q);

//            $q = new \Elastica\Query\Match();
//            $q->setFieldQuery('filename', $keywords);
//            $q->setFieldFuzziness('filename', 1);
//            $boolQuery->addShould($q);
        }

        $this->query($boolQuery, $limit, $currentPage, 'createdAt');
    }
}