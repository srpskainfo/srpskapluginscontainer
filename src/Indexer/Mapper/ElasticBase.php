<?php
/**
 * Created by PhpStorm.
 * User: djavolak
 * Date: 9/30/2020
 * Time: 2:30 PM
 */

namespace GfWpPluginContainer\Indexer\Mapper;

use Elastica\Response;
use Elastica\Result;
use Elastica\ResultSet;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Search;
use GfWpPluginContainer\Wp\Logger;

abstract class ElasticBase
{
    private $search;

    /**
     * @var ResultSet
     */
    private $resultSet;

    private $debug;

    /**
     * ElasticBase constructor.
     * @TODO add logger
     *      *
     * @param Search $search configured Search object
     * @param bool $debug
     */
    public function __construct(Search $search, $debug = false)
    {
        $this->search = $search;
        $this->debug = $debug;
    }

    /**
     * @return array
     */
    public function getIds()
    {
        $ids = [];
        foreach ($this->resultSet->getResults() as $result) {
            $ids[] = $result->getDocument()->getId();
        }

        return $ids;
    }

    /**
     * Executes given query on ES server
     *
     * @param BoolQuery $boolQuery
     * @param $limit
     * @param $page
     * @param $order
     */
    protected function query(BoolQuery $boolQuery, $limit, $page, $sort = null)
    {
        $mainQuery = new Query();
        $mainQuery->setQuery($boolQuery);
        $mainQuery->setTrackScores(true);
        $mainQuery->setParam('track_total_hits', true);
//        $mainQuery->setTrackTotalHits();
        if ($sort !== false) {
            $mainQuery->setSort(['publishedAt' => ['order' => 'desc']]);
            if ($sort) {
                $mainQuery->setSort([$sort => ['order' => 'desc']]);
            }
        }
        $this->search->setQuery($mainQuery);
        $this->search->setOption('size', 20);
        if ($limit) {
            $this->search->setOption('size', $limit);
        }
        $this->search->setOption('from', 0);
        if ($page > 1) {
            $this->search->setOption('from', ($page - 1) * $limit);
        }

        $this->resultSet = $this->search->search();
        if ($this->debug) {
            var_dump(json_encode($mainQuery->toArray()));
            $this->printDebug();
            die();
        }
    }

    /**
     * @return \Elastica\ResultSet
     */
    public function getResultSet()
    {
        return $this->resultSet;
    }

    public function printDebug()
    {
        $totalResults = $this->resultSet->getTotalHits();
        var_dump('total results: ' . $totalResults);
        var_dump('paged results: ' . count($this->resultSet->getResults()));

        echo '<table><tr><th></th><th>score</th><th width="250px">name</th><th width="150px">desc</th><th>cat</th>
            </tr>';
        /* @var \Elastica\Result $result */
        $i=0;
        foreach ($this->resultSet->getResults() as $result) {
            $i++;
            echo '<tr>';
            echo '<td>' . $i . '</td>';
            echo '<td>' . $result->getScore() . '</td>';
            echo '<td>' . $result->getDocument()->getData()['title'] . '</td>';
            echo '<td><textarea>' . $result->getDocument()->getData()['description'] . '</textarea></td>';
            echo '<td>';
            foreach ($result->getDocument()->getData()['category'] as $cat) {
                echo $cat['name'] . ', ';
            }
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}