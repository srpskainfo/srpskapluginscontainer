<?php

namespace GfWpPluginContainer\Indexer\Mapper;

use Elastica\Query\BoolQuery;
use Elastica\Query\Term;
use Elastica\Query\Range;

class Article extends ElasticBase
{
    /**
     * Returns articles that match given tags.
     *
     * @param $tags array tags to match
     * @param $catId int if > 0 match category
     * @param $postDate string 7 days before this date will be starting point (from date)
     * @param $restrictedPosts array ids of articles to ignore
     * @param $limit
     */
    public function relatedByTags($tags, $catId, $postDate, $restrictedPosts, $limit, $sort = 'publishedAt')
    {
        $boolQuery = new BoolQuery();
        $boolQuery->addMust(new Term(['status' => 1]));
        if ($catId) {
            $boolQuery->addMust(new Term(['category.id' => $catId]));
        }
        $dt = new \DateTime($postDate, new \DateTimeZone('Europe/Sarajevo'));
        $dt->modify('-3 month');
        $q = new Range();
        $q->setParam('publishedAt', ["gte" => $dt->format(\DATE_ATOM)]);
        $boolQuery->addMust($q);
        $tagsBool = new BoolQuery();
        $tagsBool->setMinimumShouldMatch(1);
        foreach ($tags as $tag) {
            $tagsBool->addShould(new Term(['tag.id' => $tag['tagId']]));
//            $boolQuery->addShould(new Term(['tags' => $tag['name']]));
            $boolQuery->addShould(new Term(['body' => $tag['name']]));
        }
        $tagsBool->setBoost(1.5);
        $boolQuery->addMust($tagsBool);

        foreach ($restrictedPosts as $postId) {
            $boolQuery->addMustNot(new Term(['postId' => $postId]));
        }
        $boolQuery->addMustNot(New Term(['category.id' => 55687])); // exclude hpg category

        $this->query($boolQuery, $limit, 1, $sort);
    }

    public function searchLatestPopularFromCategory($catId, $limit)
    {
        $boolQuery = new BoolQuery();
        $boolQuery->addMust(new Term(['status' => 1]));
        $dt = new \DateTime('now', new \DateTimeZone('Europe/Sarajevo'));
        $dt->modify('-7 day');
        $q = new Range();
        $q->setParam('publishedAt', ["gte" => $dt->format(\DATE_ATOM)]);
        $boolQuery->addMust($q);
        $boolQuery->addMust(new Term(['categoryId' => $catId]));

        $this->query($boolQuery, $limit, 1, 'viewCount');
    }

    /**
     * Returns article lists for pages.
     *
     * @param $type
     * @param $id
     * @param int $limit
     * @param int $currentPage
     */
    public function searchByType($type, $id, $limit = 0, $currentPage = 1, $restrictedPosts = null)
    {
        $boolQuery = new BoolQuery();
        $boolQuery->addMust(new Term(['status' => 1]));
        $sort = null;
        // category id
        if ($type !== 'sve-vijesti' && $type !== 'popular') {
            $q = new Term();
            $q->setParam($type . '.id', $id);
            $boolQuery->addMust($q);
        }
        if ($type === 'popular') {
            $sort = 'viewCount';
            $dt = new \DateTime('now', new \DateTimeZone('Europe/Sarajevo'));
            $dt->modify('-1 day');
            $q = new Range();
            $q->setParam('publishedAt', ["gte" => $dt->format(\DATE_ATOM)]);
            $boolQuery->addMust($q);
        }
        if ($restrictedPosts) {
            foreach ($restrictedPosts as $postId) {
                $boolQuery->addMustNot(new Term(['postId' => $postId]));
            }
        }

        $this->query($boolQuery, $limit, $currentPage, $sort);
    }

    /**
     * Performs search.
     *
     * @param $keywords
     * @param int $limit
     * @param int $currentPage
     */
    public function search($keywords, $limit = 0, $currentPage = 1)
    {
        $boolQuery = new BoolQuery();

        $boolQuery->addMust(new Term(['status' => 1]));

        $q = new \Elastica\Query\Match();
        $q->setFieldQuery('title', $keywords);
        $q->setFieldOperator('title', 'and');
        $q->setFieldFuzziness('title', 1);
        $q->setFieldBoost('title', 25);
        $boolQuery->addShould($q);

        $q = new \Elastica\Query\Match();
        $q->setFieldQuery('body', $keywords);
        $q->setFieldOperator('body', 'and');
        $q->setFieldFuzziness('body', 1);
        $q->setFieldBoost('body', 15);
        $boolQuery->addMust($q);

        $q = new \Elastica\Query\Match();
        $q->setFieldQuery('author', $keywords);
        $q->setFieldOperator('author', 'and');
        $q->setFieldFuzziness('author', 1);
        $q->setFieldBoost('author', 10);
        $boolQuery->addShould($q);

        $q = new \Elastica\Query\Match();
        $q->setFieldQuery('entity.category.name', $keywords);
        $q->setFieldFuzziness('entity.category.name', 1);
        $q->setFieldBoost('entity.category.name', 15);
        $boolQuery->addShould($q);

        $q = new \Elastica\Query\Match();
        $q->setFieldQuery('entity.tag.name', $keywords);
        $q->setFieldFuzziness('entity.tag.name', 1);
        $q->setFieldBoost('entity.tag.name', 25);
        $boolQuery->addShould($q);

        $this->query($boolQuery, $limit, $currentPage);
    }
}