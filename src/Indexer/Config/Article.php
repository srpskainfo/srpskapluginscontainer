<?php

namespace GfWpPluginContainer\Indexer\Config;

class Article implements ConfigInterface
{
    private $type = \ES_INDEX_ARTICLE;

    private $index = \ES_INDEX_ARTICLE;

    private $setupConfig = [
        'settings' => [
            'number_of_shards' => 3,
            'number_of_replicas' => 1,
            'analysis' => [
                'analyzer' => array(
                    'default' => array(
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => array('lowercase', 'stop', 'trim') // custom_ascii_folding
                    ),
                    'search' => array(
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => array('lowercase', 'trim') // custom_ascii_folding @TODO install icu_folding
                    )
                ),
                'filter' => array(
                    'mySnowball' => array(
                        'type' => 'snowball',
                        'language' => 'German'
                    ),
                    'custom_ascii_folding' => array(
                        'type' => 'asciifolding',
                        'preserve_original' => true
                    ),
                    'test' => array(
                        'type' => 'stemmer',
                        'language' => 'Russian'
                    ),
                )
            ]
        ]
    ];

    private $mapping = [
//        'article' => [
            'entity' => [
                'properties' => [
                    'postId' => array('type' => 'integer'),
                    'category' => array(
                        'type' => 'nested',
                        'properties' => array(
                            'id' => array('type' => 'integer'),
                            'parent' => array('type' => 'integer'),
                            'level' => array('type' => 'integer'),
                            'slug' => array('type' => 'text'),
                            'name' => ['type' => 'text', 'boost' => 4]
                        ),
                    ),
                    'author' => array(
                        'type' => 'nested',
                        'properties' => array(
                            'id' => array('type' => 'integer'),
                            'name' => array('type' => 'text'),
                            'url' => array('type' => 'text'),
                        ),
                    ),
                    'tag' => array(
                        'type' => 'nested',
                        'properties' => array(
                            'id' => array('type' => 'integer'),
                            'name' => array('type' => 'text', 'boost' => 6),
                            'slug' => array('type' => 'text'),
                        ),
                    ),
                    'tags' => array('type' => 'text', 'boost' => 6),
                    'title' => array('type' => 'text', 'boost' => 5, 'fielddata' => true),
                    'createdAt' => array('type' => 'date'),
                    'updatedAt' => array('type' => 'date'),
                    'publishedAt' => array('type' => 'date'),
                    'thumbnail' => array('type' => 'text'),
                    'permalink' => array('type' => 'text'),
                    'body' => array('type' => 'text'),
                    'status' => array('type' => 'integer'),
                    'synced' => array('type' => 'integer'),
                    'viewCount' => array('type' => 'integer'),
                ]
            ],
            'search_data' => [
                'properties' => [
                    'full_text' => array('type' => 'text'),
                    'full_text_boosted' => array('type' => 'text'),
                ]
            ],
//            "completion_terms" => [
//                "type" => "text",
//                "analyzer" => "search"
//            ],
//        ]
//            "suggestion_terms" => [
//                "type" => "text",
//                "index_analyzer" => "search",
//                "search_analyzer" => "search"
//            ]
    ];

    public function getMapping()
    {
        return $this->mapping;
    }

    public function getSetupConfig()
    {
        return $this->setupConfig;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getIndex()
    {
        return $this->index;
    }
}