<?php


namespace GfWpPluginContainer\Gutenberg;


use GfWpPluginContainer\Gutenberg\Blocks\Ahtrefile\Ahtrefile;
use GfWpPluginContainer\Gutenberg\Blocks\Banner\BannerBlock;
use GfWpPluginContainer\Gutenberg\Blocks\BannerMobile\BannerMobileBlock;
use GfWpPluginContainer\Gutenberg\Blocks\BlicBlock\BlicBlock;
use GfWpPluginContainer\Gutenberg\Blocks\Blockquote\Blockquote;
use GfWpPluginContainer\Gutenberg\Blocks\CategoryBlock\CategoryBlock;
use GfWpPluginContainer\Gutenberg\Blocks\CategoryShowcase\CategoryShowcase;
use GfWpPluginContainer\Gutenberg\Blocks\CategorySidebar\CategorySidebar;
use GfWpPluginContainer\Gutenberg\Blocks\HpgBlock\HpgBlock;
use GfWpPluginContainer\Gutenberg\Blocks\NewsBlock\NewsBlock;
use GfWpPluginContainer\Gutenberg\Blocks\NewsLeftColumn\NewsLeftColumn;
use GfWpPluginContainer\Gutenberg\Blocks\NewsletterBlock\NewsletterBlock;
use GfWpPluginContainer\Gutenberg\Blocks\NewsRightColumn\NewsRightColumn;
use GfWpPluginContainer\Gutenberg\Blocks\NewPopular\NewPopular;
use GfWpPluginContainer\Gutenberg\Blocks\PulseEmbed\PulseEmbed;
use GfWpPluginContainer\Gutenberg\Blocks\RelatedArticles\RelatedArticles;
use GfWpPluginContainer\Gutenberg\Blocks\GalleryBlock\GalleryBlock;
use GfWpPluginContainer\Wp\WpEnqueue;

class Gutenberg {
	/**
	 * Initiate custom blocks
	 *
	 */
	public function init() {
	    if (is_admin()) {
            $this->registerScripts();
        }
        $this->registerGfBlockCategory();
        $this->registerStyles();
        $this->registerBlocks();
	}

	private function registerScripts() {
		//Auto generated file when using wp-scripts
		$assetFile = include( PLUGIN_DIR . 'src/Gutenberg/build/index.asset.php' );
		//         Register JavasScript File build/index.js
		WpEnqueue::addGutenbergBlockScript( 'gfBlocks', PLUGIN_DIR_URI . 'src/Gutenberg/build/index.v12.js', $assetFile['dependencies'], 10 );
        WpEnqueue::addGutenbergBlockScript('gfBlocksJqueryUi', PLUGIN_DIR_URI . 'assets/js/jquery.datetimepicker.full.min.js');

		// Enqueue custom data to be used in blocks in edit pages
		add_action('enqueue_block_assets', function (){
            wp_localize_script('gfBlocks','gfBlockData',[
                'blicNaslovna' => get_option('blicNaslovnaImg'),
                'ajaxUrl' => admin_url() . 'admin-ajax.php'
            ]);
        });

	}

	private function registerStyles() {
		// Register editor style build/css/editor.css
		WpEnqueue::addGlobalAdminStyle( 'gfBlocksEditor', PLUGIN_DIR_URI . 'src/Gutenberg/src/scss/editor/editor.css',
			array( 'wp-edit-blocks' ) );
        WpEnqueue::addGlobalAdminStyle( 'jqueryUi', PLUGIN_DIR_URI . 'assets/css/jquery.datetimepicker.min.css');
	}

	private function registerBlocks() {
		new CategoryBlock();
		new NewsBlock();
		new NewsLeftColumn();
		new NewsRightColumn();
		new BannerBlock();
		new BannerMobileBlock();
		new BlicBlock();
		new HpgBlock();
		new NewPopular();
		new CategorySidebar();
		new CategoryShowcase();
		new NewsletterBlock();
		new Blockquote();
		new RelatedArticles();
		new GalleryBlock();
		new Ahtrefile();
		new PulseEmbed();

	}

	// Register Custom Block Category
	private function registerGfBlockCategory() {
		add_filter( 'block_categories', function ( $categories ) {
			return array_merge(
				$categories,
				array(
					array(
						'slug'  => 'gfBlocks',
						'title' => 'GF Blocks',
					),
				)
			);
		} );
	}
}