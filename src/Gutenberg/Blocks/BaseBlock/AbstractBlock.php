<?php
namespace GfWpPluginContainer\Gutenberg\Blocks\BaseBlock;

use GfWpPluginContainer\Wp\Logger;

abstract class AbstractBlock
{
	protected $isMobile;

    public function __construct()
    {
        add_action('init', [$this, 'register']);
        add_action('after_setup_theme', [$this, "setIsMobile"]);
    }
    public function setIsMobile()
    {
        $this->isMobile = (int)wp_is_mobile();
    }

    /**
     * Prepare html to be printed
     *
     * @param $attributes
     * @return mixed
     */
    abstract function prepareHtml($attributes);

    /**
     * Unified render method, for easier caching.
     *
     * @param $attributes
     * @return bool|mixed|string
     */
    public function render($attributes)
    {
        global $cache;

        if ($this->isMobileApp($_SERVER)) {
            $key = sprintf('blockHtmlFragmentMobileApp#%s', get_class($this) . md5(serialize($attributes)));
        } else {
            if ($this->isMobile) {
                $key = sprintf('blockHtmlFragmentMobile#%s', get_class($this) . md5(serialize($attributes)));
            } else {
                $key = sprintf('blockHtmlFragment#%s', get_class($this) . md5(serialize($attributes)));
            }
        }
        $html = $cache->get($key);
        if ($html === false) {
            $html = $this->prepareHtml($attributes);
            $cache->set($key, $html);
        }
        return $html;
    }

    /**
     * Checks if request is coming from mobile app
     *
     * @param $request
     * @return bool
     */
    public function isMobileApp($request) :bool
    {
        global $isApp;

        return $isApp;
    }

    /**
     * Checks whether the post index input is empty
     * @param $attributes
     * @param $postIndex
     * @return bool
     */
    protected function isPostIndexEmpty($attributes,$postIndex)
    {
        return !(isset($attributes[$postIndex]) && $attributes[$postIndex] !== '');
    }

    /**
     * Checks whether the post expiry input is set
     * @param $attributes
     * @param $postExpiryIndex
     * @return bool
     */
    protected function isSetPostExpiry($attributes,$postExpiryIndex)
    {
        return isset($attributes[$postExpiryIndex]) && $attributes[$postExpiryIndex];
    }

    /**
     * Checks whether the post replacement input is set
     * @param $attributes
     * @param $postReplacementIndex
     * @return bool
     */
    protected function isSetPostReplacement($attributes, $postReplacementIndex)
    {
        return $attributes[$postReplacementIndex] !== '';
    }
}
