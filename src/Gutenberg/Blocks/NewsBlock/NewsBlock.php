<?php


namespace GfWpPluginContainer\Gutenberg\Blocks\NewsBlock;


class NewsBlock
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public function register()
    {
        register_block_type('wpplugincontainer/gfnewsblock', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));
    }

    public function render($attributes , $content)
    {
        return '<div class="homepageNews container">' . $content . '</div>';
    }
}
