<?php

namespace GfWpPluginContainer\Gutenberg\Blocks\CategoryShowcase;

use Carbon\Carbon;
use GfWpPluginContainer\GfShopThemePlugins;
use GfWpPluginContainer\Gutenberg\Blocks\BaseBlock\AbstractBlock;

class CategoryShowcase extends AbstractBlock
{
    private function getPostsForTemplate($attributes, $numberOfPosts)
    {
        if (isset($attributes['selectedCategory']) && $attributes['selectedCategory'] != 0) {
            global $restrictedPosts;
            $categoryId = $attributes['selectedCategory'];
            $categoryName = $attributes['selectedCategoryName'];
            $postIds = [];
            //populates postIds array with user input and skips empty strings
            $postsDb = $this->parseFixedPosts($attributes,$numberOfPosts);

            /*If user didnt input enough post ids for template to work
                     exclude posts from input and get newest for the remaining posts
                     */
            if (count($postsDb) < $numberOfPosts) {
                $postsDb = array_merge($postsDb, get_posts([
                    'category' => $attributes['selectedCategory'],
                    'exclude' => $restrictedPosts,
                    'numberposts' => $numberOfPosts - count($postsDb),
                    'post_status' => 'publish'

                ]));
            }

            foreach ($postsDb as $key => $post) {
                $urls = [
                    'siprod.djavolak.info',
                    'srpska.greenfriends.systems',
                    'srpska.local'
                ];
//                $imageUrl = str_replace($urls, 'srpskainfo.com', get_the_post_thumbnail_url($post));
                $imageUrl = (get_the_post_thumbnail_url($post, 'list-small') ? : wp_get_attachment_image_url(get_option("defaultFeaturedImage")));
                $imageId = (get_post_thumbnail_id($post->ID)? : get_option("defaultFeaturedImage"));

                $restrictedPosts[] = $post->ID;
                $posts[$key] = [
                    'categoryName' => $categoryName,
                    'categoryId' => $categoryId,
                    'categoryUrl' => str_replace('category/', '', get_category_link($categoryId)),
                    'url' => get_permalink($post->ID),
                    'imageUrl' => $imageUrl,
                    'imageId' => $imageId,
                    'postType' => $post->post_type,
                    'postId' => $post->ID,
                    'postTitle' => $post->post_title,
                    'postContent' => $post->post_content,
                    'publishDate' => $post->post_date
                ];
            }

            if (!isset($posts)) {
                return null;
            }

            return [
                'posts' => $posts,
            ];
        }
        return null;
    }

    public function register()
    {
        register_block_type('wpplugincontainer/gfcategoryshowcase', [
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ]);
    }

    public function parseFixedPosts($attributes, $numberOfPosts)
    {
        global $restrictedPosts;
        // Prepare the array
        $postIds = $postsDb = [];
        // Set the current date
        $dtNow = new \DateTime('now', new \DateTimeZone('Europe/Sarajevo'));
        // Set the variables according the the number of posts
        for ( $i = 1; $i <= $numberOfPosts; $i ++ ) {
            $postIndex = 'post' . $i;
            $postExpiryIndex = 'post' . $i . 'Expiry';
            $postReplacementIndex = 'post' . $i . 'Replacement';

            // If there is user input for the fixed post
            if ($this->isPostIndexEmpty($attributes,$postIndex) === false) {
                // If the expiry index is set
                if ($this->isSetPostExpiry($attributes,$postExpiryIndex) === true) {
                    // Get the date of the expiry
                    $dt = new \DateTime($attributes[$postExpiryIndex], new \DateTimeZone('Europe/Sarajevo'));
                    // If the current datetime is greater than the expiry get the replacement index
                    if ($dtNow->getTimestamp() >= $dt->getTimestamp()) {
                        // If the replacement index is set get the id
                        if ($this->isSetPostReplacement($attributes,$postReplacementIndex) === true) {
                            $postIds[] = $attributes[$postReplacementIndex];
                        }
                    } else { // If the current datetime is NOT greater than the expiry get the post index
                        $postIds[] = $attributes[$postIndex];
                    }
                } else { // If the expiry index is not set
                    $postIds[] = $attributes[$postIndex];
                }
            //If there is no user input for the fixed post but the expiry and the replacement is set
            } elseif ($this->isPostIndexEmpty($attributes, $postIndex) === true
                && $this->isSetPostExpiry($attributes, $postExpiryIndex) === true
                && $this->isSetPostReplacement($attributes, $postReplacementIndex) === true) {

                $dt = new \DateTime($attributes[$postExpiryIndex], new \DateTimeZone('Europe/Sarajevo'));
                // If the current date is before the expiry date get the replacement post index
                if ($dtNow->getTimestamp() >= $dt->getTimestamp()) {
                    $postIds[] = $attributes[$postReplacementIndex];
                }
            }
        }
        // Get the posts and populate the restricted posts respectively
        foreach ($postIds as $postId) {
            $post = get_post($postId);
            if ($post) {
                $postsDb[] = $post;
                $restrictedPosts [] = $postId;
            }
        }
        return $postsDb;
    }


    public function prepareHtml($attributes)
    {
        $mobileApp = $this->isMobileApp($_SERVER);
        ob_start();
        $data = $this->getPostsForTemplate($attributes, 4);
        if ($data) {
            if ($mobileApp) {
                GfShopThemePlugins::getTemplatePartials('gutenberg', 'categoryShowcase', 'categoryShowcaseMobileApp',
                    $data);
                return ob_get_clean();
            }
            GfShopThemePlugins::getTemplatePartials('gutenberg', 'categoryShowcase', 'categoryShowcase', $data);
        }
        return ob_get_clean();
    }

    public static function renderArticle(
        $href,
        $title,
        $imageSrc,
        $width,
        $height,
        $type,
        $catUrl,
        $catTitle,
        $postDateHtml,
        $imageAltText
    ) {
        $escapedTitle = esc_attr($title);
        $escapedCatTitle = esc_attr($catTitle);
        $html = '<article class="news__item">
                <a href="' . $href . '" title="' . $escapedTitle . '">
                    <figure>
                        <picture>
                            <img src="' . $imageSrc . '" alt="' . $imageAltText . '" height="' . $height . '" width="' . $width . '">';
        if ($type === 'video') {
            $html .= '<span class="icon"><i class="fas fa-play"></i></span>';
        }
        if ($type === 'audio') {
            $html .= '<span class="icon"><i class="fas fa-microphone"></i></span>';
        }
        $html .= '</picture>
                    </figure>
                </a>
                <div>
                    <span class="news__category"><a class="catWithPostDate" href="' . $catUrl . '" title="' . $escapedCatTitle . '">' . $catTitle . '</a>
                     ' . $postDateHtml . '
                    </span>
                    <h2><a title="' . $escapedTitle . '" href="' . $href . '">' . $title . '</a></h2>
                </div>
            </article>';

        return $html;
    }
}

