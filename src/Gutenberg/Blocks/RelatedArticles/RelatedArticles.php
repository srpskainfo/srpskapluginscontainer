<?php

namespace GfWpPluginContainer\Gutenberg\Blocks\RelatedArticles;

use Elastica\Exception\ResponseException;
use GfWpPluginContainer\Gutenberg\Blocks\BaseBlock\AbstractBlock;
use GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;
use GfWpPluginContainer\Wp\Article;
use GfWpPluginContainer\Wp\Logger;
use GfWpPluginContainer\Wp\ShortCodes;

class RelatedArticles extends AbstractBlock {
	public function register() {
		register_block_type( 'wpplugincontainer/relatedarticles', array(
			'editor_script'   => 'gfBlocks',
			'editor_style'    => 'gfBlocksEditor',
			'style'           => 'gfBlocksFront',
			'render_callback' => [ $this, 'render' ]
		) );
		$this->registerShortcode();
	}

    private function getPosts($attributes, $numberOfPosts)
    {
        global $restrictedPosts;

        // fallback for old version of the block, new passes the id.
        $currentPostId = get_queried_object_id();
        // If it's a new version of the block, get the current ID
        if(isset($attributes['currentPostId'])) {
            $currentPostId = $attributes['currentPostId'];
            $restrictedPosts[] = $currentPostId;
        }

        $postIds = [];
        $posts = [];
        //populates postIds array with user input and skips empty strings
        for ($i = 1; $i <= $numberOfPosts; $i++) {
            if (isset($attributes['post' . $i]) && strlen($attributes['post' . $i]) > 0) {
                $postIds[] = $attributes['post' . $i];
            }
        }

        //If there is user input for post ids get those first
        if (count($postIds) !== 0) {
            foreach ($postIds as $postId) {
                $post = get_post($postId);
                if (!$post) {
                    continue;
                }
                $posts[] = Article::fromPost($post);
                $restrictedPosts[] = $postId;
            }
        }
        /*If user didnt input enough post ids for template to work exclude posts from input and get the most popular
            for the remaining posts based on the post tags and category in the last 7 days
         */
        if (count($postIds) < $numberOfPosts) {
            $numberOfPostsMissing = $numberOfPosts - count($postIds);
            $tagIds = [];
            foreach (wp_get_post_tags($currentPostId) as $postTag) {
                $tagIds[] = [
                    'tagId' => $postTag->term_id,
                    'name' => $postTag->name,
                ];
            }
            $catId = get_the_category($currentPostId)[0]->term_id;
            try {
                $relatedPosts = ArticleRepo::relatedByTagsFromCategory($tagIds, $catId, get_queried_object()->post_date, $restrictedPosts, 20)['articles'];
            } catch (\Exception $e) {
                Logger::generateDebugLog('elasticDebugLog',date('d-m-Y H:i:s') . '/procitajteJos->' . $e->getTraceAsString());
                $relatedPosts = [];
            }

            shuffle($relatedPosts);
            $relatedPosts = array_slice($relatedPosts, 0, $numberOfPostsMissing);
            /* @var Article $post */
            foreach ($relatedPosts as $post) {
                $restrictedPosts[] = $post->getPostId();
            }
            // If there is still not enough posts to populate the block, search by category and view count
            if(count($posts) < $numberOfPostsMissing) {
                // @TODO should not be needed anymore
            }
            // Combine the user input and the rest of the posts that populate the empty input fields into one array
            $posts = array_merge($posts, $relatedPosts);
        }
        if (!count($posts)) {
            return null;
        }
        return $posts;
    }

	/**
	 *
	 * @param $attributes
     * @param $numberOfPosts
	 *
	 * @return array
	 * @todo srediti kod kada se uradi dobra validacija iz js-a
	 */
//    private function getPosts($attributes, $numberOfPosts)
//    {
//        $currentPostId = '';
//        // If it's a new version of the block, get the current ID
//        if(isset($attributes['currentPostId'])) {
//            $currentPostId = $attributes['currentPostId'];
//        }
//        // If numberOfPosts is set
//        if ($numberOfPosts) {
//            // Get the current post id and make it restricted
//            global $restrictedPosts;
//            $restrictedPosts[] = $currentPostId;
//            $postIds = [];
//            $postsDb = [];
//            //populates postIds array with user input and skips empty strings
//            for ($i = 1; $i <= $numberOfPosts; $i++) {
//                if (isset($attributes['post' . $i]) && strlen($attributes['post' . $i]) > 0) {
//                    $postIds[] = $attributes['post' . $i];
//                }
//            }
//            //If there is user input for post ids get those first
//            if (count($postIds) !== 0) {
//                foreach ($postIds as $postId) {
//                    $postsDb[] = get_post($postId);
//                    $restrictedPosts[] = $postId;
//                }
//            }
//            /*If user didnt input enough post ids for template to work
//                         exclude posts from input and get newest for the remaining posts based on the post tags and category
//                         */
//            if (count($postIds) < $numberOfPosts) {
//                $numberOfPostsMissing = $numberOfPosts - count($postIds);
//                $postTagIds = [];
//                $postTags = wp_get_post_tags($currentPostId);
//                foreach ($postTags as $postTag) {
//                    $postTagIds[] = $postTag->term_id;
//                }
//                $posts = get_posts([
//                    'tag__in' => $postTagIds,
//                    'numberposts' => $numberOfPostsMissing,
//                    'post__not_in' => $restrictedPosts,
//                    'category' => get_the_category($currentPostId)[0]->term_id,
//                    'date_query' => [
//                      [
//                          'after' => '7 days ago'
//                      ]
//                    ],
//                    'orderby' => 'date',
//                    'order' => 'DESC',
//                    'suppress_filters' => false,
//                    'category__not_in' => [55687]
//                ]);
//                shuffle($posts);
//                $posts = array_slice($posts, 0, $numberOfPostsMissing);
//                foreach ($posts as $post) {
//                    $restrictedPosts[] = $post->ID;
//                }
//                // If there is still not enough posts to populate the block, search by category and view count
//                if(count($posts) < $numberOfPostsMissing) {
//                    $postsSecondQuery = get_posts([
//                        'numberposts' => $numberOfPostsMissing - count($posts),
//                        'category' => get_the_category($currentPostId)[0]->term_id,
//                        'post__not_in' => $restrictedPosts,
//                        'meta_key' => 'gfPostViewCount',
//                        'date_query' => [
//                            [
//                                'after' => '24 hours ago'
//                            ]
//                        ],
//                        'orderby' => 'meta_value_num',
//                        'order' => 'DESC',
//                        'suppress_filters' => false,
//                        'category__not_in' => [55687]
//                    ]);
//                    foreach ($postsSecondQuery as $post) {
//                        $restrictedPosts[] = $post->ID;
//                        $posts[] = $post;
//                    }
//                }
//                // Combine the user input and the rest of the posts that populate the empty input fields into one array
//                $postsDb = array_merge($postsDb, $posts);
//            }
//            if (!isset($postsDb)) {
//                return null;
//            }
//            return $postsDb;
//        }
//        return null;
//    }

    /**
     * @param $attributes
     * @return string
     */
	public function prepareHtml($attributes)
    {
        $numberOfPosts = '3';
        // Accepted values in the admin edit (select element)
        $acceptedValues = ['1','2','3','4','5'];
        if (isset($attributes['numberOfPosts']) && in_array($attributes['numberOfPosts'], $acceptedValues)) {
            $numberOfPosts = $attributes['numberOfPosts'];
        }
        if (is_admin()) {
            return '';
        }

        $posts = $this->getPosts($attributes, $numberOfPosts);
        $html = '';
        if ($posts) {
            // Icon class differs depending on whether it is amp or not
            $iconClass = 'fas';
            if (basename($_SERVER['REQUEST_URI']) === 'amp') {
                $iconClass = 'fa';
            }
            $html = '<div class="article__related"> <h3>Pročitajte još</h3><ul>';
            $isApp = $_SERVER['REQUEST_URI'] === '/mobile-home/' || strpos($_SERVER['REQUEST_URI'], '/app/') !== false;
            /* @var Article $post */
            foreach ($posts as $post) {
                if ($isApp) {
                    $postLink = parseAppUrl('article', get_permalink($post->getPostId()));
                } else {
                    $postLink = get_permalink($post->getPostId());
                }

                $html .= sprintf('  <li><i class="%s fa-chevron-right"></i><a title="%s" href="%s">%s</a></li>',
                    $iconClass, $post->getTitle(), $postLink, $post->getTitle());
            }
            $html .= '</ul></div>';
        }

        return $html;
	}

	/**
	 * Shortcode for showing read more inside posts, used instead of block for migrated posts
	 */
	public function registerShortcode() {
		ShortCodes::registerShortCode( 'pj', PLUGIN_DIR . 'templates/shortcodes/relatedArticles.php' );
	}
}












