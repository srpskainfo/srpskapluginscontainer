<?php

namespace GfWpPluginContainer\Gutenberg\Blocks\HpgBlock;

use GfWpPluginContainer\Gutenberg\Blocks\BaseBlock\AbstractBlock;

class HpgBlock extends AbstractBlock
{

    public function register()
    {
        register_block_type('wpplugincontainer/gfhpgblock', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));
    }

    public function prepareHtml($attributes)
    {
        global $restrictedPosts;

        // If there is user input get that info as a priority
        if(isset($attributes['hitDana']) && $attributes['hitDana'] !== '') {
            $hitDana = get_post($attributes['hitDana']);
            $hitPermalink = get_permalink($hitDana->ID);
            $hitTitle = str_replace(['HIT', 'DANA'], '', $hitDana->post_title);
            $restrictedPosts[] = $hitDana->ID;
        } else { // If there is no user input get the newest post with the HIT tag
            $hitInfo = $this->getNewestHpgPost('hit');
            $hitDana = $hitInfo['postObject'][0];
            $hitPermalink = $hitInfo['postPermaLink'];
            $hitTitle = str_replace(['HIT', 'DANA'], '', $hitInfo['postTitle']);
            $restrictedPosts[] = $hitDana->ID;
        }
        // If there is user input get that info as a priority
        if(isset($attributes['pobednikDana']) && $attributes['pobednikDana'] !== '') {
            $pobednikDana = get_post($attributes['pobednikDana']);
            $pobednikPermalink = get_permalink($pobednikDana->ID);
            $pobednikTitle = str_replace(['POBJEDNIK', 'DANA'], '', $pobednikDana->post_title);
            $restrictedPosts[] = $pobednikDana->ID;
        } else { // If there is no user input get the newest post with the POBJEDNIK tag
            $pobednikInfo = $this->getNewestHpgPost('pobjednik');
            $pobednikDana = $pobednikInfo['postObject'][0];
            $pobednikPermalink = $pobednikInfo['postPermaLink'];
            $pobednikTitle = str_replace(['POBJEDNIK', 'DANA'], '', $pobednikInfo['postTitle']);
            $restrictedPosts[] = $pobednikDana->ID;
        }
        // If there is user input get that info as a priority
        if(isset($attributes['gubitnikDana']) && $attributes['gubitnikDana'] !== '') {
            $gubitnikDana = get_post($attributes['gubitnikDana']);
            $gubitnikPermalink = get_permalink($gubitnikDana->ID);
            $gubitnikTitle = str_replace(['GUBITNIK', 'DANA'], '', $gubitnikDana->post_title);
            $restrictedPosts[] = $gubitnikDana->ID;
        } else { // If there is no user input get the newest post with the GUBITNIK tag
            $gubitnikInfo = $this->getNewestHpgPost('gubitnik');
            $gubitnikDana = $gubitnikInfo['postObject'][0];
            $gubitnikPermalink = $gubitnikInfo['postPermaLink'];
            $gubitnikTitle = str_replace(['GUBITNIK', 'DANA'], '', $gubitnikInfo['postTitle']);
            $restrictedPosts[] = $gubitnikDana->ID;
        }

        ob_start();
        if ($this->isMobileApp($_SERVER)) {
            $hitPermalink = parseAppUrl('article', $hitPermalink);
            $pobednikPermalink = parseAppUrl('article', $pobednikPermalink);
            $gubitnikPermalink = parseAppUrl('article', $gubitnikPermalink);
        }

        include(PLUGIN_DIR . 'templates/gutenberg/HpgBlock/HpgBlock.phtml');
        return ob_get_clean();
    }

    public function renderHpgTemplate($href, $title, $imageSrc, $width, $height, $icon, $boxTitle) {
        $escapedTitle = esc_attr($title);

        return '<article class="news__item">
                    <a href="' . $href . '" title="'. $escapedTitle .'">
                        <figure>
                            <img height="'.$height.'" width="'.$width.'" src="' . $imageSrc . '" alt="'. $escapedTitle .'">
                            <span class="icon"><i class="far fa-'.$icon.'"></i></span>
                        </figure>
                    </a>
                    <div>
                        <span class="news__category"><a href="' . $href . '" title="'. $escapedTitle .'">'.$boxTitle.'</a></span>
                        <h3><a href="' . $href . '" title="'. $escapedTitle .'">' . $title . '</a></h3>
                    </div>
                </article>';
    }

    /**
     * Gets the newest HPG post depending on the param provided.
     * The query will search the posts by tag and return 1 post (newest)
     *
     * @param $typeOfPost
     * @return array
     */
    public function getNewestHpgPost($typeOfPost): array
    {
        $args = [];
        switch ($typeOfPost) {
            case 'hit':
                $args = [
                'numberposts' => '1',
                'tax_query'  => [
                        [
                            'taxonomy'  => 'post_tag',
                            'field'     => 'slug',
                            'terms'     =>  'hit-dana'
                        ],
                    ],

                ];
                break;
            case 'pobjednik':
                $args = [
                    'numberposts' => '1',
                    'tax_query'  => [
                        [
                            'taxonomy'  => 'post_tag',
                            'field'     => 'slug',
                            'terms'     =>  'pobjednik-dana'
                        ],
                    ],
                ];
                break;
            case 'gubitnik':
                $args = [
                    'numberposts' => '1',
                    'tax_query'  => [
                        [
                            'taxonomy'  => 'post_tag',
                            'field'     => 'slug',
                            'terms'     =>  'gubitnik-dana'
                            ],
                        ],
                ];
                break;
        }
        $hpgPost = get_posts($args);
        $hpgPermalink = get_permalink($hpgPost[0]->ID);
        $hpgTitle = $hpgPost[0]->post_title;

        return [
            'postObject' => $hpgPost,
            'postPermaLink' => $hpgPermalink,
            'postTitle' => $hpgTitle
            ];
    }
}
