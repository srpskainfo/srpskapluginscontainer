<?php


namespace GfWpPluginContainer\Gutenberg\Blocks\PulseEmbed;


class PulseEmbed
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public function register()
    {
        register_block_type('wpplugincontainer/pulseembed', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));

    }

    public function render($attributes)
    {
        return $attributes['embedHtml'];
    }
}