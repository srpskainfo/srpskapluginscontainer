<?php

namespace GfWpPluginContainer\Gutenberg\Blocks\Ahtrefile;

use GfWpPluginContainer\Gutenberg\Blocks\BaseBlock\AbstractBlock;
use GfWpPluginContainer\Wp\ShortCodes;

class Ahtrefile extends AbstractBlock {
    public function register() {
        register_block_type( 'wpplugincontainer/ahtrefile', array(
            'editor_script'   => 'gfBlocks',
            'editor_style'    => 'gfBlocksEditor',
            'style'           => 'gfBlocksFront',
            'render_callback' => [ $this, 'render' ]
        ) );
    }

    /**
     *
     * @param $attributes
     *
     * @return string|null
     * @todo srediti kod kada se uradi dobra validacija iz js-a
     */
    public function prepareHtml( $attributes ) {

        if ($attributes) {
            $html = sprintf('<div class="article__related ahtrefile"><h3>%s</h3><div class="ahtrefileParagraph"><p>%s</p></div></div>',$attributes['title'],$attributes['content']);
            return $html;
        }

        return null;
    }
}












