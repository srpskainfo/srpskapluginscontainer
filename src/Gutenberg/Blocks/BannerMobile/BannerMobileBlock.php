<?php


namespace GfWpPluginContainer\Gutenberg\Blocks\BannerMobile;

class BannerMobileBlock {
	public function __construct() {
		add_action( 'init', [ $this, 'register' ] );
	}

	public function register() {
		register_block_type( 'wpplugincontainer/gfbannermobileblock', array(
			'editor_script'   => 'gfBlocks',
			'editor_style'    => 'gfBlocksEditor',
			'style'           => 'gfBlocksFront',
			'render_callback' => [ $this, 'render' ]
		) );
	}

	public function render( $attributes ) {
		/*   Inputs: fixed, sticky, sticky area.
			 Fixed and sticky have the same layout with different classes,
			 sticky area has a different layout with different classes.
		*/

		if ( ! wp_is_mobile() ) {
			return null;
		}

		// Default class for mobile
		$class = 'banner banner--mobile ';

		if (isset($attributes['selectedType'])) {
			// If sticky is selected change the class
			if ( $attributes['selectedType'] === '1' ) {
				$class .= 'banner--sticky ';
			}
			// If sticky billboard is selected
			if ( $attributes['selectedType'] === '2' ) {
				$class .= 'banner--sticky banner--sticky_billboard ';
			}
		}

		if ( isset( $attributes['selectedTypeMargin'] ) ) {
			if ( $attributes['selectedTypeMargin'] === '1' ) {
				$class .= 'banner--space-top ';
			}

			if ( $attributes['selectedTypeMargin'] === '2' ) {
				$class .= 'banner--space-bottom ';
			}
		}

		// If content is passed in the input field
		if ( isset( $attributes['content'] ) ) {
			return sprintf( '<div class="%s">%s</div>', $class, $attributes['content'] );
		}

		return null;
	}
}
