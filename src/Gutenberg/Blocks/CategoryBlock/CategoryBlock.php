<?php

namespace GfWpPluginContainer\Gutenberg\Blocks\CategoryBlock;

use GfWpPluginContainer\GfShopThemePlugins;
use GfWpPluginContainer\Gutenberg\Blocks\BaseBlock\AbstractBlock;
use GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;
use GfWpPluginContainer\Wp\Article;
use GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors;

class CategoryBlock extends AbstractBlock {
	public function register() {
		register_block_type( 'wpplugincontainer/gfcategoriesblock', array(
			'editor_script'   => 'gfBlocks',
			'editor_style'    => 'gfBlocksEditor',
			'style'           => 'gfBlocksFront',
			'render_callback' => [ $this, 'render' ]
		) );
	}

	private function getPostsForTemplate( $attributes, $numberOfPosts ) {
		if ( isset( $attributes['selectedCategory'] ) && $attributes['selectedCategory'] != 0 ) {
			$posts = $this->getPosts( $attributes, $numberOfPosts );

			return [
				'template'         => $attributes['template'],
				'posts'            => $posts,
				'selectedCategory' => $attributes['selectedCategory']
			];
		}

		return null;
	}

    /**
     * Parses selected (fixed) posts, and returns an array of WP_Posts
     *
     * @param $attributes
     * @param $numberOfPosts
     * @return array
     */
	private function parseFixedPosts($attributes, $numberOfPosts)
    {
        // Prepare the array
        $postIds = $postsDb = [];
        // Set the current date
        $dtNow = new \DateTime('now', new \DateTimeZone('Europe/Sarajevo'));
        // Set the variables according the the number of posts
        for ( $i = 1; $i <= $numberOfPosts; $i ++ ) {
            $postIndex = 'post' . $i;
            $postExpiryIndex = 'post' . $i . 'Expiry';
            $postReplacementIndex = 'post' . $i . 'Replacement';
            // If there is user input for the fixed post
            if ($this->isPostIndexEmpty($attributes,$postIndex) === false) {
                // If the expiry index is set
                if ($this->isSetPostExpiry($attributes,$postExpiryIndex) === true) {
                    // Get the date of the expiry
                    $dt = new \DateTime($attributes[$postExpiryIndex], new \DateTimeZone('Europe/Sarajevo'));
                    // If the current datetime is greater than the expiry get the replacement index
                    if ($dtNow->getTimestamp() >= $dt->getTimestamp()) {
                        // If the replacement index is set get the id
                        if ($this->isSetPostReplacement($attributes,$postReplacementIndex) === true) {
                            $postIds[] = $attributes[$postReplacementIndex];
                        }
                    } else { // If the current datetime is NOT greater than the expiry get the post index
                        $postIds[] = $attributes[$postIndex];
                    }
                } else { // If the expiry index is not set
                    $postIds[] = $attributes[$postIndex];
                }
            //If there is no user input for the fixed post but the expiry and the replacement is set
            } elseif ($this->isPostIndexEmpty($attributes, $postIndex) === true
                && $this->isSetPostExpiry($attributes, $postExpiryIndex) === true
                && $this->isSetPostReplacement($attributes, $postReplacementIndex) === true) {

                // Get the expiry index date
                $dt = new \DateTime($attributes[$postExpiryIndex], new \DateTimeZone('Europe/Sarajevo'));
                // If the current date is greater the expiry date get the replacement post index
                if ($dtNow->getTimestamp() >= $dt->getTimestamp()) {
                    $postIds[] = $attributes[$postReplacementIndex];
                }
            }
        }
        // Get the posts and populate the restricted posts respectively
        foreach ($postIds as $postId) {
            if (isset($postId) && strlen($postId) !== ''){
                $post = get_post($postId);
            }
            if ($post) {
                $postsDb[] = $post;
            }
        }
        return $postsDb;
    }

	private function getPosts( $attributes, $numberOfPosts ) {
		global $restrictedPosts;
		$posts = [];

        $postsDb = $this->parseFixedPosts($attributes, $numberOfPosts);

		//When specific category is selected
		if ( $attributes['selectedCategory'] !== '-1' ) {
			$categoryId   = $attributes['selectedCategory'];
			$categoryName = $attributes['selectedCategoryName'];

            // parse user input first
            foreach ($postsDb as $post) {
                $posts[] = $this->parseWpPost($categoryName, $categoryId, $post);
                $restrictedPosts[] = $post->ID;
            }

			/* If user didn't input enough post ids for template to work
			 * exclude posts from input and get newest for the remaining posts
			 */
			try {
                if (count($postsDb) < $numberOfPosts) {
                    $mockTerm = new \stdClass();
                    $mockTerm->term_id = $categoryId;
                    $postsEs = ArticleRepo::getItemsFromElasticBy('category', $mockTerm, 1, $numberOfPosts - count($postsDb), $restrictedPosts)['articles'];
                    /* @var Article $article */
                    foreach ($postsEs as $article) {
                        $posts[] = $this->parseWpPost($categoryName, $categoryId, get_post($article->getPostId()));
                        $restrictedPosts[] = $article->getPostId();
                    }
                }
            } catch (\Exception $e) {
                if ( count( $postsDb ) < $numberOfPosts ) {
                    $postsDb = array_merge( $postsDb, get_posts( [
                        'category'    => $categoryId,
                        'exclude'     => $restrictedPosts,
                        'numberposts' => $numberOfPosts - count( $postsDb ),
                        'post_status'     => 'publish'
                    ] ) );
                }
                foreach ( $postsDb as $key => $post ) {
                    $restrictedPosts[] = $post->ID;
                    $posts[ $key ]     = [
                        'categoryName' => $categoryName,
                        'categoryId'   => $categoryId,
                        'categoryUrl'  => str_replace( 'category/', '', get_category_link( $categoryId ) ),
                        'url'          => get_permalink( $post->ID ),
                        'postId'       => $post->ID,
                        'imageId'      => get_post_thumbnail_id( $post ),
                        'postType'     => $post->post_type,
                        'postTitle'    => $post->post_title,
                        'postContent'  => count( parse_blocks( $post->post_content ) ) > 0 ? parse_blocks( $post->post_content ) : [],
                        'publishDate' => $post->post_date
                    ];
                }
            }

		} else  /*When all categories is selected */ {
			/* If user didn't input enough post ids for template to work
			 * exclude posts from input and get newest for the remaining posts
			 */
			if ( count( $postsDb ) < $numberOfPosts ) {
				$postsDb = array_merge( $postsDb, get_posts( [
					'exclude'     => $restrictedPosts,
					'numberposts' => $numberOfPosts - count( $postsDb ),
					'post_status'     => 'publish',
                    'category__not_in' => [55687]
                ] ) );
			}

			foreach ( $postsDb as $key => $post ) {
				$restrictedPosts[] = $post->ID;
				$categoryObject    = get_the_category( $post->ID );
				$posts[ $key ]     = [
					'categoryName' => esc_html( $categoryObject[0]->name ),
					'categoryId'   => $categoryObject[0]->term_id,
					'postId'       => $post->ID,
					'categoryUrl'  => esc_url( str_replace( 'category/', '', get_category_link( $categoryObject ) ) ),
					'url'          => esc_url( get_permalink( $post->ID ) ),
					'imageId'      => (get_post_thumbnail_id($post) ? : get_option("defaultFeaturedImage")),
					'postType'     => get_post_format( $post->ID ),
					'postTitle'    => esc_html( $post->post_title ),
					'postContent'  => count( parse_blocks( $post->post_content ) ) > 0 ? parse_blocks( $post->post_content ) : [],
                    'publishDate'  => $post->post_date
				];
			}
		}

		if ( ! isset( $posts ) ) {
			return null;
		}
		return $posts;
	}

    /**
     * @TODO test out and comlete
     * @param $categoryName
     * @param $categoryId
     * @param Article $article
     * @return array
     */
    private function parseArticle($categoryName, $categoryId, Article $article)
    {
        return [
            'categoryName' => $categoryName,
            'categoryId'   => $categoryId,
            'categoryUrl'  => str_replace('category/', '', get_category_link($categoryId)),
            'url'          => $article->getPermalink(),
            'postId'       => $article->getPostId(),
            'imageId'      => (get_post_thumbnail_id($article->getPostId()) ? : get_option("defaultFeaturedImage")),
            'postType'     => 'TODO',
            'postTitle'    => $article->getTitle(),
            'postContent'  => $article->getBody(),
            'publishDate'  => $article->getPublishedAt()
        ];
    }

	private function parseWpPost($categoryName, $categoryId, $post)
    {
        return [
            'categoryName' => $categoryName,
            'categoryId'   => $categoryId,
            'categoryUrl'  => str_replace( 'category/', '', get_category_link( $categoryId ) ),
            'url'          => get_permalink( $post->ID ),
            'postId'       => $post->ID,
            'imageId'      => (get_post_thumbnail_id($post) ? : get_option("defaultFeaturedImage")),
            'postType'     => $post->post_type,
            'postTitle'    => $post->post_title,
            'postContent'  => count(parse_blocks($post->post_content)) > 0 ? parse_blocks($post->post_content) : [],
            'publishDate'  => $post->post_date
        ];
    }

	/**
	 * @param $attributes
	 *
	 * Prepares html for blocks, mobileApp check needs to be first because
	 * mobile app also counts as mobile and we want mobileApp template on mobile App
	 *
	 * @return false|mixed|string|null
	 */
	public function prepareHtml( $attributes ) {
        global $cache;
		if ( isset( $attributes['template'] ) ) {
			ob_start();
			$mobileApp = $this->isMobileApp( $_SERVER );
            $multipleAuthors = new MultipleAuthors();
			switch ( $attributes['template'] ) {
				case '1':
					$data = $this->getPostsForTemplate( $attributes, 5 );
					if ( $data ) {
                        if($attributes['selectedCategory'] === '6') {
                            $restrictedSportPosts = [];
                            foreach($data['posts'] as $post) {
                                if($post['postId']) {
                                    $restrictedSportPosts[] = $post['postId'];
                                }
                            }
                            $cache->set('restrictedSportPosts', serialize($restrictedSportPosts),0);
                        }
						$data = $this->extractContentForTemplate1( $data );
                        $data['multipleAuthors'] = $multipleAuthors;
						if ( $mobileApp ) {
						    if($attributes['selectedCategory'] === '-1' || $attributes['selectedCategory'] === '6') {
                                GfShopThemePlugins::getTemplatePartials('gutenberg', 'categoriesBlock', 'catTemplate1MobileApp', $data);
                                break;
                            }
                            GfShopThemePlugins::getTemplatePartials('gutenberg', 'categoriesBlock', 'catTemplateMobileApp', $data);
						    break;
						}

						if ( $this->isMobile ) {
                            if($attributes['selectedCategory'] === '-1' || $attributes['selectedCategory'] === '6') {
                                GfShopThemePlugins::getTemplatePartials('gutenberg', 'categoriesBlock', 'catTemplate1Mobile', $data);
                                break;
                            }
                            GfShopThemePlugins::getTemplatePartials('gutenberg', 'categoriesBlock', 'catTemplateMobile', $data);
                            break;
                        }
						GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplate1', $data );
					}
					break;
				case '2':
					$data = $this->getPostsForTemplate( $attributes, 4 );
					if ( $data ) {
                        $data['multipleAuthors'] = $multipleAuthors;
						if ( $mobileApp ) {
							GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobileApp', $data );
							break;
						}
						if ( $this->isMobile ) {
							GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobile', $data );
							break;
						}
						GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplate2', $data );
					}
					break;
				case '3':
					$data = $this->getPostsForTemplate( $attributes, 3 );
					if ( $data ) {
                        $data['multipleAuthors'] = $multipleAuthors;
						if ( $mobileApp ) {
							GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobileApp', $data );
							break;
						}
						if ( $this->isMobile ) {
							GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobile', $data );
							break;
						}
						GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplate3', $data );
					}
					break;
				case '4':
					$data = $this->getPostsForTemplate( $attributes, 3 );
					if ( $data ) {
                        $data['multipleAuthors'] = $multipleAuthors;
						if ( $mobileApp ) {
							GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobileApp', $data );
							break;
						}
						if ( $this->isMobile ) {
							GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobile', $data );
							break;
						}
						GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplate4', $data );
					}
					break;
				case '5':
					$data = $this->getPostsForTemplate( $attributes, 6 );
					if ( $data ) {
                        $data['multipleAuthors'] = $multipleAuthors;
						if ( $mobileApp ) {
							GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobileApp', $data );
							break;
						}
						if ( $this->isMobile ) {
							GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobile', $data );
							break;
						}
						GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplate5', $data );
					}
					break;
                case '6':
                    $data = $this->getPostsForTemplate( $attributes, 10 );
                    if ( $data ) {
                        $data = $this->extractContentForTemplate1( $data );
                        $data['multipleAuthors'] = $multipleAuthors;
                        if ( $mobileApp ) {
                            GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobileApp', $data );
                            break;
                        }

                        if ( $this->isMobile ) {
                            GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplateMobile', $data );
                            break;
                        }
                        GfShopThemePlugins::getTemplatePartials( 'gutenberg', 'categoriesBlock', 'catTemplate6', $data );
                    }
                    break;
				default:
					break;
			}

			return ob_get_clean();
		}

		return null;
	}

	private function extractContentForTemplate1( $data ) {
		foreach ( $data['posts'] as $key => $post ) {
			// return only the first heading of post content
			$data['posts'][ $key ]['postContent'] = parseHeading($post['postContent']);

			//change h2 element into p element so it better fits the layout
//			$data['posts'][ $key ]['postContent'] = str_replace( '<h2>', '<p>', $data['posts'][ $key ]['postContent'] );
//			$data['posts'][ $key ]['postContent'] = str_replace( '</h2>', '</p>', $data['posts'][ $key ]['postContent'] );

			//if there is no heading in post return first paragraph
//			if ( strlen( $data['posts'][ $key ]['postContent'] ) === 0 ) {
				//In case first item in post body is not paragraph, find start of the first paragraph
//				$data['posts'][ $key ]['postContent'] = strstr( $post['postContent'], '<!-- wp:paragraph -->', false );
//
				//Take first paragraph ending and return everything before it
//				$data['posts'][ $key ]['postContent'] = strstr( $data['posts'][ $key ]['postContent'], '<!-- /wp:paragraph -->', true );
//			}
		}

		return $data;
	}
}