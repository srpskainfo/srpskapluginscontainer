<?php


namespace GfWpPluginContainer\Gutenberg\Blocks\NewsletterBlock;


class NewsletterBlock
{
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    public function register()
    {
        register_block_type('wpplugincontainer/gfnewsletterblock', array(
            'editor_script' => 'gfBlocks',
            'editor_style' => 'gfBlocksEditor',
            'style' => 'gfBlocksFront',
            'render_callback' => [$this, 'render']
        ));
    }

    public function render($attributes , $content)
    {
        $title = $attributes['newsletterTitle'];
        $text = $attributes['newsletterText'];
        $code =  $attributes['newsletterCode'];
        return sprintf(' <div ' . ($this->isMobile ? '' : 'class="gray"') . '>
            <div ' . ($this->isMobile ? '' : 'class="container"') . '>
                <div ' . ($this->isMobile ? '' : 'class="box box--center"') . '>
                    <div ' . ($this->isMobile ? '' : 'class="box__left"') . '>
                   <!--temporary place for the script -->
                    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0&appId=206933876746107&autoLogAppEvents=1"></script>
                        <section ' . ($this->isMobile ? '' : 'class="newsletter"') . '>
                            <h4>%s</h4>
                            <p>%s</p>
                            <form>
                                <input type="text" placeholder="Email Adresa">
                                <button>Pošalji</button>
                            </form>
                        </section>
                    </div>
                    <div ' . ($this->isMobile ? '' : 'class="box__right"') . '>
                        <section ' . ($this->isMobile ? '' : 'class="fb__page"') . '>
                            %s
                        </section>
                    </div>
                </div>
            </div>
        </div>', $title, $text, $code );
    }
}