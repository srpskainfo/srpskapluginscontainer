import {registerBlockType} from '@wordpress/blocks';
import {InnerBlocks} from '@wordpress/editor';
import {InspectorControls, MediaUpload, MediaUploadCheck} from '@wordpress/block-editor';
import {TextControl, PanelBody, PanelRow, Button } from '@wordpress/components';
import axios from 'axios';

let blockDate = Math.floor(Math.random() * 101);

(function($) {
    $(document).ready(function(){
        $('body').on('click', '.resetExpiry', function() {
            // $(this).prev().val('').trigger('change');
            $(this).prev().prev().val('').focus().datetimepicker('destroy').prev().focus();
            // $(this).prev().val('').trigger('change');
        });

        $('body').on('click', '.datepicker', clickHandler);
        function clickHandler(event) {
            $(".datepicker").datetimepicker({
                lang: 'bs',
                step: 10,
                onChangeDateTime:function(ct,$i){
                    // console.log($i.val());
                }
            });
        }
    });
})(jQuery);

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfcategoriesblock')) {
    registerBlockType('wpplugincontainer/gfcategoriesblock', {
        title: 'GF Categories',
        icon: 'archive',
        category: 'gfBlocks',
        attributes: {
            categories: {
                type: 'object'
            },
            selectedCategory: {
                type: 'string'
            },
            selectedCategoryName: {
                type: 'string'
            },
            template: {
                type: 'string'
            },
            post1: {
                type: 'int'
            },
            post1Expiry: {
                type: 'int'
            },
            post1Replacement: {
                type: 'int'
            },
            post2: {
                type: 'int'
            },
            post2Expiry: {
                type: 'int'
            },
            post2Replacement: {
                type: 'int'
            },
            post3: {
                type: 'int'
            },
            post3Expiry: {
                type: 'int'
            },
            post3Replacement: {
                type: 'int'
            },
            post4: {
                type: 'int'
            },
            post4Expiry: {
                type: 'int'
            },
            post4Replacement: {
                type: 'int'
            },
            post5: {
                type: 'int'
            },
            post5Expiry: {
                type: 'int'
            },
            post5Replacement: {
                type: 'int'
            },
            post6: {
                type: 'int'
            },
            post6Expiry: {
                type: 'int'
            },
            post6Replacement: {
                type: 'int'
            },
            post7: {
                type: 'int'
            },
            post7Expiry: {
                type: 'int'
            },
            post7Replacement: {
                type: 'int'
            },
            post8: {
                type: 'int'
            },
            post8Expiry: {
                type: 'int'
            },
            post8Replacement: {
                type: 'int'
            },
            post9: {
                type: 'int'
            },
            post9Expiry: {
                type: 'int'
            },
            post9Replacement: {
                type: 'int'
            },
            post10: {
                type: 'int'
            },
            post10Expiry: {
                type: 'int'
            },
            post10Replacement: {
                type: 'int'
            },
            posts: {
                type: 'object'
            },
            date: {
                type: 'string',
                default: null

            }
        },

        edit: function (props) {
            function getTemplateData(template) {
                switch (template) {
                    case '1':
                        return [1,2,3,4,5];
                    case '2':
                        return [1,2,3,4];
                    case '3':
                        return [1,2,3];
                    case '4':
                        return [1,2,3];
                    case '5':
                        return [1,2,3,4,5,6];
                    case '6':
                        return [1,2,3,4,5,6,7,8,9,10]
                    default:
                        return [1,2,3,4,5];
                }
            }

            function printInputs() {
                let counter = getTemplateData(props.attributes.template);
                const items = [];
                for (const [index, value] of counter.entries()) {
                    const name = "post" + value
                    const expiry = "post" + value + "Expiry"
                    const replacement = "post" + value + "Replacement"
                    items.push(<label>{"Post #"+ value}</label>)
                    items.push(<input type="text" name={name} placeholder={"Post #" + value} onChange={updateAttribute}
                                      size="6" value={props.attributes[name]}/>)
                    items.push(<input type="text" name={"post"+value+"Expiry"} placeholder={"Post #"+value+" Expiry"}
                        onBlur={updateAttribute} size="13" class="datepicker" value={props.attributes[expiry]}/>)
                    items.push(<input type="text" name={"post"+value+"Replacement"} placeholder={"Post #"+value+" Replacement"}
                        onChange={updateAttribute} size="6" value={props.attributes[replacement]}/>)
                    items.push(<a class="resetExpiry" href="#">X</a>)
                    items.push(<br />)
                }
                return <div className="newsInputIdEditor">
                    <div>
                        {items}
                    </div>
                </div>;
            }

            if (!props.attributes.categories) {
                wp.apiFetch({
                    url: '/wp-json/wp/v2/categories?per_page=100&exclude=55687'
                }).then(categories => {
                    props.setAttributes({
                        categories: categories
                    });
                });
            }
            if (!props.attributes.categories) {
                return "Loading...";
            }

            if (props.attributes.categories && props.attributes.categories.length === 0) {
                return "No categories found, please add some!";
            }

            function updateAttribute(e) {
                let attrName = {};
                console.log('updating attribute ' + e.target.name);
                attrName[e.target.name] = e.target.value;
                props.setAttributes(attrName);
            }

            function updateSelectedCategory(e) {
                props.setAttributes({
                    selectedCategory: e.target.value,
                    selectedCategoryName: e.target.options[e.target.selectedIndex].getAttribute('data-categoryName')
                });
            }

            function updateSelectedTemplate(e) {
                props.setAttributes({
                    template: e.target.value,
                    post1: null,
                    post1Expiry: null,
                    post1Replacement: null,
                    post2: null,
                    post2Expiry: null,
                    post2Replacement: null,
                    post3: null,
                    post3Expiry: null,
                    post3Replacement: null,
                    post4: null,
                    post4Expiry: null,
                    post4Replacement: null,
                    post5: null,
                    post5Expiry: null,
                    post5Replacement: null,
                    post6: null,
                    post6Expiry: null,
                    post6Replacement: null,
                    post7: null,
                    post7Expiry: null,
                    post7Replacement: null,
                    post8: null,
                    post8Expiry: null,
                    post8Replacement: null,
                    post9: null,
                    post9Expiry: null,
                    post9Replacement: null,
                    post10: null,
                    post10Expiry: null,
                    post10Replacement: null
                })
                let inputsWrapper = e.target.parentElement.querySelector('.newsInputIdEditor');
                let inputs = inputsWrapper.querySelectorAll('input')

                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].value = '';
                }
            }

            return [
                <div class='gfCategoriesBlockEditor gfBlock'>
                    <h3>Categories Block</h3>
                    <label for="categorySelect">Izaberite kategoriju</label>
                    <select class="categorySelect" onChange={updateSelectedCategory}
                            value={props.attributes.selectedCategory}>
                        <option>--------------------</option>

                        <option value="-1">Sve kategorije</option>
                        {
                            props.attributes.categories.map(category => {
                                return (
                                    <option data-categoryName={category.name} value={category.id} key={category.id}>
                                        {category.name}
                                    </option>
                                );
                            })
                        }
                    </select>

                    <label for="templateSelect">Izaberite šablon</label>
                    <select class="templateSelect" onChange={updateSelectedTemplate} value={props.attributes.template}>
                        <option>-</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                    {printInputs()}
                </div>,

            ];
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfnewsblock')) {
    registerBlockType('wpplugincontainer/gfnewsblock', {

        title: 'GF News Block',
        icon: 'grid-view',
        category: 'gfBlocks',
        attributes: {
            imageUrl: {
                type: 'string'
            }
        },

        edit: function (props) {
            const assets = '/wp-content/plugins/srpskapluginscontainer/src/Gutenberg/Blocks/NewsBlock/assets/static/';
            const innerColumns = [
                ['core/columns', {}, [
                    ['core/column', {}, [
                        ['wpplugincontainer/gfnewsleftcolumn']
                    ]],
                    ['core/column', {}, [
                        ['wpplugincontainer/gfnewsrightcolumn']
                    ]
                    ]
                ]
                ]];

            return [
                <div class='gfNewsBlockEditor'>
                    <h2>Gf News Block</h2>
                    <label htmlFor="templatePreview">Pregled vrsti šablona</label>
                    {
                        <div>
                            <img src={assets + 'layout.png'} alt="templateLayouts"/>
                        </div>
                    }
                    <InnerBlocks
                        template={innerColumns}
                        templateLock="all"
                    />
                </div>
            ];
        },

        save: function (props) {
            return (
                <InnerBlocks.Content/>
            )
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfnewsleftcolumn')) {
    registerBlockType('wpplugincontainer/gfnewsleftcolumn', {
        title: 'GF News Left Column',
        icon: 'align-left',
        category: 'gfBlocks',
        attributes: {},


        edit: function (props) {
            let ALLOWED_BLOCKS = ['wpplugincontainer/gfcategoriesblock', 'wpplugincontainer/gfbannermobileblock'];
            return [
                <div>
                    <InnerBlocks
                        allowedBlocks={ALLOWED_BLOCKS}
                    />
                </div>
            ];
        },

        save: function (props) {
            return (
                <InnerBlocks.Content/>
            )
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfnewsrightcolumn')) {
    registerBlockType('wpplugincontainer/gfnewsrightcolumn', {
        title: 'GF News Right Column',
        icon: 'align-right',
        category: 'gfBlocks',
        attributes: {},


        edit: function (props) {
            let ALLOWED_BLOCKS = ['wpplugincontainer/gfblicblock', 'wpplugincontainer/gfbannerblock', 'wpplugincontainer/gfbannermobileblock',
                'wpplugincontainer/gfnewpopular', 'wpplugincontainer/gfcategorysidebar', 'core/shortcode'];
            return [
                <div className={'rightColumn'}>
                    <InnerBlocks
                        allowedBlocks={ALLOWED_BLOCKS}
                    />
                </div>
            ];
        },

        save: function (props) {
            return (
                <InnerBlocks.Content/>
            )
        },
    });

}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfbannermobileblock')) {
    registerBlockType('wpplugincontainer/gfbannermobileblock', {
        title: 'GF Banner Mobile',
        icon: 'embed-generic',
        category: 'gfBlocks',
        attributes: {
            widthDesktop: {
                type: 'int'
            },
            heightDesktop: {
                type: 'int'
            },
            widthTablet: {
                type: 'int'
            },
            heightTablet: {
                type: 'int'
            },
            widthMobile: {
                type: 'int'
            },
            heightMobile: {
                type: 'int'
            },
            selectedType: {
                type: 'string'
            },
            selectedTypeMargin: {
                type: 'string'
            },
            content: {
                type: 'string'
            }
        },


        edit: function (props) {

            function updateSelectedType(e) {
                props.setAttributes({
                    selectedType: e.target.value
                });
            }

            function updateSelectedTypeMargin(e) {
                props.setAttributes({
                    selectedTypeMargin: e.target.value
                });
            }

            function updateContent(e) {
                props.setAttributes({
                    content: e.target.value
                });
            }

            function updateDesktopHeight() {
                props.setAttributes({
                    heightDesktop: e.target.value
                });
            }

            function updateDesktopWidth() {
                props.setAttributes({
                    widthDesktop: e.target.value
                });
            }

            function updateTabletWidth() {
                props.setAttributes({
                    widthTablet: e.target.value
                });
            }

            function updateTabletHeight() {
                props.setAttributes({
                    heightTablet: e.target.value
                });
            }

            function updateMobileHeight() {
                props.setAttributes({
                    heightMobile: e.target.value
                });
            }

            function updateMobileWidth() {
                props.setAttributes({
                    widthMobile: e.target.value
                });
            }


            return [
                <div class='gfBannerBlockEditor gfBlock'>
                    <h3>Banner Block Mobile</h3>
                    <div>
                        <label for="bannerTypeSelect">Tip banera</label>
                        <select id="bannerTypeSelect" onChange={updateSelectedType}
                                value={props.attributes.selectedType}>
                            <option>--------------------</option>
                            <option value="0">Fixed</option>
                            <option value="1">Sticky</option>
                            <option value="2">Sticky Billboard</option>
                        </select>
                    </div>
                    <div>
                        <label htmlFor="bannerTypeSelect">Gde zelite marginu</label>
                        <select id="bannerMarginSelect" onChange={updateSelectedTypeMargin}
                                value={props.attributes.selectedTypeMargin}>
                            <option>--------------------</option>
                            <option value="1">Space Top</option>
                            <option value="2">Space Bottom</option>
                        </select>
                    </div>
                    <div>
                        <label>Unesite kod za baner</label>
                        <div>
                            <textarea cols="100" onChange={updateContent} value={props.attributes.content}></textarea>
                        </div>
                    </div>
                </div>,
                <div>
                    <InspectorControls>
                        <PanelBody title="Responsive Settings" initialOpen={true}>
                            <PanelRow>
                                <label>Desktop</label>
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Visina"
                                    value={props.attributes.heightDesktop}
                                    onChange={updateDesktopHeight}
                                />
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Širina"
                                    value={props.attributes.widthDesktop}
                                    onChange={updateDesktopWidth}
                                />
                            </PanelRow>
                            <PanelRow>
                                <label>Tablet</label>
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Visina"
                                    value={props.attributes.heightTablet}
                                    onChange={updateTabletHeight}
                                />
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Širina"
                                    value={props.attributes.widthTablet}
                                    onChange={updateTabletWidth}
                                />
                            </PanelRow>
                            <PanelRow>
                                <label>Mobile</label>
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Visina"
                                    value={props.attributes.heightMobile}
                                    onChange={updateMobileHeight}
                                />
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Širina"
                                    value={props.attributes.widthMobile}
                                    onChange={updateMobileWidth}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                </div>
            ];
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfbannerblock')) {
    registerBlockType('wpplugincontainer/gfbannerblock', {
        title: 'GF Banner',
        icon: 'embed-generic',
        category: 'gfBlocks',
        attributes: {
            widthDesktop: {
                type: 'int'
            },
            heightDesktop: {
                type: 'int'
            },
            widthTablet: {
                type: 'int'
            },
            heightTablet: {
                type: 'int'
            },
            widthMobile: {
                type: 'int'
            },
            heightMobile: {
                type: 'int'
            },
            selectedType: {
                type: 'string'
            },
            content: {
                type: 'string'
            }
        },


        edit: function (props) {

            function updateSelectedType(e) {
                props.setAttributes({
                    selectedType: e.target.value
                });
            }

            function updateContent(e) {
                props.setAttributes({
                    content: e.target.value
                });
            }

            function updateDesktopHeight() {
                props.setAttributes({
                    heightDesktop: e.target.value
                });
            }

            function updateDesktopWidth() {
                props.setAttributes({
                    widthDesktop: e.target.value
                });
            }

            function updateTabletWidth() {
                props.setAttributes({
                    widthTablet: e.target.value
                });
            }

            function updateTabletHeight() {
                props.setAttributes({
                    heightTablet: e.target.value
                });
            }

            function updateMobileHeight() {
                props.setAttributes({
                    heightMobile: e.target.value
                });
            }

            function updateMobileWidth() {
                props.setAttributes({
                    widthMobile: e.target.value
                });
            }


            return [
                <div class='gfBannerBlockEditor gfBlock'>
                    <h3>Banner Block</h3>
                    <div>
                        <label for="bannerTypeSelect">Tip banera</label>
                        <select id="bannerTypeSelect" onChange={updateSelectedType}
                                value={props.attributes.selectedType}>
                            <option>--------------------</option>
                            <option value="0">Fixed</option>
                            <option value="1">Sticky</option>
                            <option value="2">Sticky Area</option>
                        </select>
                    </div>
                    <div>
                        <label>Unesite kod za baner</label>
                        <div>
                            <textarea cols="100" onChange={updateContent} value={props.attributes.content}></textarea>
                        </div>
                    </div>
                </div>,
                <div>
                    <InspectorControls>
                        <PanelBody title="Responsive Settings" initialOpen={true}>
                            <PanelRow>
                                <label>Desktop</label>
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Visina"
                                    value={props.attributes.heightDesktop}
                                    onChange={updateDesktopHeight}
                                />
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Širina"
                                    value={props.attributes.widthDesktop}
                                    onChange={updateDesktopWidth}
                                />
                            </PanelRow>
                            <PanelRow>
                                <label>Tablet</label>
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Visina"
                                    value={props.attributes.heightTablet}
                                    onChange={updateTabletHeight}
                                />
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Širina"
                                    value={props.attributes.widthTablet}
                                    onChange={updateTabletWidth}
                                />
                            </PanelRow>
                            <PanelRow>
                                <label>Mobile</label>
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Visina"
                                    value={props.attributes.heightMobile}
                                    onChange={updateMobileHeight}
                                />
                            </PanelRow>
                            <PanelRow>
                                <TextControl
                                    label="Širina"
                                    value={props.attributes.widthMobile}
                                    onChange={updateMobileWidth}
                                />
                            </PanelRow>
                        </PanelBody>
                    </InspectorControls>
                </div>
            ];
        },

        save: function () {
            return null;
        },
    });
}
if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfblicblock')) {
    registerBlockType('wpplugincontainer/gfblicblock', {

        title: 'GF Blic naslovna',
        icon: 'format-image',
        category: 'gfBlocks',
        attributes: {
            imageUrl: {
                type: 'string'
            },
        },

        edit: function (props) {
            function updateBlicNaslovna(imageUrl ) {
                var params = new URLSearchParams();
                params.append('action', 'updateBlicNaslovna');
                params.append('blicNaslovnaImageUrl', imageUrl);
                axios.post(gfBlockData.ajaxUrl, params )
                    .then( function (response) {
                        console.log(response.data);
                    })
                    .catch( function (error) {
                        console.log(error);
                    });
            }

            function handleImageSave(media) {
                props.setAttributes({imageUrl: media.url});
                updateBlicNaslovna(media.url);
            }

            const getImageButton = (openEvent) => {
                if (props.attributes.imageUrl) {
                    return (
                        <img
                            src={props.attributes.imageUrl}
                            onClick={openEvent}
                            className="gfImageEditor"
                        />
                    );
                } else {
                    return (
                        <div className="button-container">
                            <Button
                                onClick={openEvent}
                                className="button button-large">
                                Izaberite sliku
                            </Button>
                        </div>
                    );
                }
            };

            return [
                <div class='gfBlock'>
                    <h3>Euroblic naslovna</h3>
                    <MediaUpload
                        onSelect={(media) => handleImageSave(media)}
                        type='image'
                        value={props.attributes.imageUrl}
                        render={({open}) => getImageButton(open)}
                    />
                </div>
            ];
        },

        save: function (props) {
            return null;
        },
    });
}
if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfhpgblock')) {
    registerBlockType('wpplugincontainer/gfhpgblock', {
        title: 'GF HPG Block',
        icon: 'align-wide',
        category: 'gfBlocks',
        attributes: {
            hitDana: {
                type: 'int'
            },
            pobednikDana: {
                type: 'int'
            },
            gubitnikDana: {
                type: 'int'
            },
        },


        edit: function (props) {

            function updateHitDana(e) {
                props.setAttributes({
                    hitDana: e.target.value
                });
            }

            function updatePobednikDana(e) {
                props.setAttributes({
                    pobednikDana: e.target.value
                });
            }

            function updateGubitnikDana(e) {
                props.setAttributes({
                    gubitnikDana: e.target.value
                });
            }

            return (
                <div className={'gfBlock'}>
                    <h3>HPG Block</h3>
                    <table className={'hpgTable'}>
                        <tr>
                            <th>Hit dana</th>
                            <th>Pobjednik dana</th>
                            <th>Gubitnik dana</th>
                        </tr>
                        <tr>
                            <td><input type="text" placeholder='Post ID' onChange={updateHitDana}
                                       value={props.attributes.hitDana}/></td>
                            <td><input type="text" placeholder='Post ID' onChange={updatePobednikDana}
                                       value={props.attributes.pobednikDana}/></td>
                            <td><input type="text" placeholder='Post ID' onChange={updateGubitnikDana}
                                       value={props.attributes.gubitnikDana}/></td>
                        </tr>
                    </table>
                </div>
            );
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfnewpopular')) {
    registerBlockType('wpplugincontainer/gfnewpopular', {
        title: 'GF New/Popular Sidebar',
        icon: 'align-pull-right',
        category: 'gfBlocks',
        attributes: {
            numberOfPosts: {
                type: 'string'
            }
        },


        edit: function (props) {

            function updatePostsPerPage(e) {
                props.setAttributes({
                    numberOfPosts: e.target.value
                });
            }

            return (
                <div className={'gfBlock'}>
                    <h3>News Sidebar Block</h3>
                    <div>
                        <label>Unesite koliko želite članaka da bude prikazano</label>
                    </div>
                    <input type="text" onChange={updatePostsPerPage}
                           value={props.attributes.numberOfPosts}/>
                </div>
            );
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfcategorysidebar')) {
    registerBlockType('wpplugincontainer/gfcategorysidebar', {
        title: 'Category Sidebar',
        icon: 'align-left',
        category: 'gfBlocks',
        attributes: {
            categories: {
                type: 'object'
            },
            selectedCategory: {
                type: 'string'
            },
            selectedCategoryName: {
                type: 'string'
            },
            post1: {
                type: 'int'
            },
            post1Expiry: {
                type: 'int'
            },
            post1Replacement: {
                type: 'int'
            },
            post2: {
                type: 'int'
            },
            post2Expiry: {
                type: 'int'
            },
            post2Replacement: {
                type: 'int'
            },
            post3: {
                type: 'int'
            },
            post3Expiry: {
                type: 'int'
            },
            post3Replacement: {
                type: 'int'
            },
            post4: {
                type: 'int'
            },
            post4Expiry: {
                type: 'int'
            },
            post4Replacement: {
                type: 'int'
            },
            post5: {
                type: 'int'
            },
            post5Expiry: {
                type: 'int'
            },
            post5Replacement: {
                type: 'int'
            },
            numberOfPosts: {
                type: 'string'
            }
        },


        edit: function (props) {
            function getNumberOfPosts(numberOfPosts) {
                switch (numberOfPosts) {
                    case '1':
                        return [1];
                    case '2':
                        return [1,2];
                    case '3':
                        return [1,2,3];
                    case '4':
                        return [1,2,3,4];
                    case '5':
                        return [1,2,3,4,5];
                    default:
                        return [1,2,3,4,5];
                }
            }


            function printInputsSidebar() {
                let counter = getNumberOfPosts(props.attributes.numberOfPosts);
                const items = [];
                for (const [index, value] of counter.entries()) {
                    const name = "post" + value
                    const expiry = "post" + value + "Expiry"
                    const replacement = "post" + value + "Replacement"
                    items.push(<label>{"Post #"+ value}</label>)
                    items.push(<input type="text" name={name} placeholder={"Post #" + value} onChange={updateAttribute}
                                      size="6" value={props.attributes[name]}/>)
                    items.push(<input type="text" name={"post"+value+"Expiry"} placeholder={"Post #"+value+" Expiry"}
                                      onBlur={updateAttribute} size="13" class="datepicker" value={props.attributes[expiry]}/>)
                    items.push(<input type="text" name={"post"+value+"Replacement"} placeholder={"Post #"+value+" Replacement"}
                                      onChange={updateAttribute} size="6" value={props.attributes[replacement]}/>)
                    items.push(<a class="resetExpiry" href="#">X</a>)
                    items.push(<br />)
                }
                return <div className="newsInputIdEditor">
                    <div>
                        {items}
                    </div>
                </div>;
            }

            if (!props.attributes.categories) {
                wp.apiFetch({
                    url: '/wp-json/wp/v2/categories?per_page=100&exclude=55687'
                }).then(categories => {
                    props.setAttributes({
                        categories: categories
                    });
                });
            }
            if (!props.attributes.categories) {
                return "Loading...";
            }

            if (props.attributes.categories && props.attributes.categories.length === 0) {
                return "No categories found, please add some!";
            }

            function updateAttribute(e) {
                let attrName = {};
                attrName[e.target.name] = e.target.value;
                props.setAttributes(attrName);
            }

            function updateSelectedCategory(e) {
                props.setAttributes({
                    selectedCategory: e.target.value,
                    selectedCategoryName: e.target.options[e.target.selectedIndex].getAttribute('data-categoryName')
                });
            }
            function updateNumberOfPosts(e) {
                props.setAttributes({
                    numberOfPosts: e.target.value,
                    post1: null,
                    post1Expiry: null,
                    post1Replacement: null,
                    post2: null,
                    post2Expiry: null,
                    post2Replacement: null,
                    post3: null,
                    post3Expiry: null,
                    post3Replacement: null,
                    post4: null,
                    post4Expiry: null,
                    post4Replacement: null,
                    post5: null,
                    post5Expiry: null,
                    post5Replacement: null,
                })
                let inputsWrapper = e.target.parentElement.parentElement.querySelector('.newsInputIdEditor');
                let inputs = inputsWrapper.querySelectorAll('input')

                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].value = '';
                }
            }


            return [
                <div class='gfCategoriesBlockEditor gfBlock'>
                    <h3>Category Sidebar</h3>
                    <label for="categorySelect">Izaberite kategoriju</label>
                    <select class="categorySelect" onChange={updateSelectedCategory}
                            value={props.attributes.selectedCategory}>
                        <option>--------------------</option>

                        <option value="-1">Sve kategorije</option>
                        {
                            props.attributes.categories.map(category => {
                                return (
                                    <option data-categoryName={category.name} value={category.id} key={category.id}>
                                        {category.name}
                                    </option>
                                );
                            })
                        }
                    </select>
                    <div>
                        <label for="numberOfPosts">Izaberite broj vesti za prikaz</label>
                        <select class="numberOfPoSTS" onChange={updateNumberOfPosts} value={props.attributes.numberOfPosts}>
                            <option>-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    {printInputsSidebar()}
                </div>,

            ];
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfcategoryshowcase')) {
    registerBlockType('wpplugincontainer/gfcategoryshowcase', {
        title: 'Category Showcase',
        icon: 'editor-insertmore',
        category: 'gfBlocks',
        attributes: {
            categories: {
                type: 'object'
            },
            selectedCategory: {
                type: 'string'
            },
            selectedCategoryName: {
                type: 'string'
            },
            post1: {
                type: 'int'
            },
            post1Expiry: {
                type: 'int'
            },
            post1Replacement: {
                type: 'int'
            },
            post2: {
                type: 'int'
            },
            post2Expiry: {
                type: 'int'
            },
            post2Replacement: {
                type: 'int'
            },
            post3: {
                type: 'int'
            },
            post3Expiry: {
                type: 'int'
            },
            post3Replacement: {
                type: 'int'
            },
            post4: {
                type: 'int'
            },
            post4Expiry: {
                type: 'int'
            },
            post4Replacement: {
                type: 'int'
            },
        },


        edit: function (props) {

            function updateAttribute(e) {
                let attrName = {};
                attrName[e.target.name] = e.target.value;
                props.setAttributes(attrName);
            }

            function printInputsShowcase() {
                // Showcase has a fixed number of 4
                let numberOfPosts = [1,2,3,4];
                const items = [];
                for (const [index, value] of numberOfPosts.entries()) {
                    const name = "post" + value
                    const expiry = "post" + value + "Expiry"
                    const replacement = "post" + value + "Replacement"
                    items.push(<label>{"Post #"+ value}</label>)
                    items.push(<input type="text" name={name} placeholder={"Post #" + value} onChange={updateAttribute}
                                      size="6" value={props.attributes[name]}/>)
                    items.push(<input type="text" name={"post"+value+"Expiry"} placeholder={"Post #"+value+" Expiry"}
                                      onBlur={updateAttribute} size="13" class="datepicker" value={props.attributes[expiry]}/>)
                    items.push(<input type="text" name={"post"+value+"Replacement"} placeholder={"Post #"+value+" Replacement"}
                                      onChange={updateAttribute} size="6" value={props.attributes[replacement]}/>)
                    items.push(<a class="resetExpiry" href="#">X</a>)
                    items.push(<br/>)
                }
                return [<div>{items}</div>]
            }


            if (!props.attributes.categories) {
                wp.apiFetch({
                    url: '/wp-json/wp/v2/categories?per_page=100&exclude=55687'
                }).then(categories => {
                    props.setAttributes({
                        categories: categories
                    });
                });
            }
            if (!props.attributes.categories) {
                return "Loading...";
            }

            if (props.attributes.categories && props.attributes.categories.length === 0) {
                return "No categories found, please add some!";
            }

            function updateSelectedCategory(e) {
                props.setAttributes({
                    selectedCategory: e.target.value,
                    selectedCategoryName: e.target.options[e.target.selectedIndex].getAttribute('data-categoryName')
                });
            }

            return [
                <div class='gfCategoriesBlockEditor gfBlock'>
                    <h3>Category Showcase</h3>
                    <label for="categorySelect">Izaberite kategoriju</label>
                    <select class="categorySelect" onChange={updateSelectedCategory}
                            value={props.attributes.selectedCategory}>
                        <option>--------------------</option>
                        {
                            props.attributes.categories.map(category => {
                                return [
                                    <option data-categoryName={category.name} value={category.id} key={category.id}>
                                        {category.name}
                                    </option>,
                                ];
                            })
                        }
                    </select>
                    {printInputsShowcase()}
                </div>,

            ];
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfnewsletterblock')) {
    registerBlockType('wpplugincontainer/gfnewsletterblock', {
        title: 'Newsletter Block',
        icon: 'email-alt',
        category: 'gfBlocks',
        attributes: {
            newsletterTitle: {
                type: 'string'
            },
            newsletterText: {
                type: 'string'
            },
            newsletterCode: {
                type: 'string'
            },
        },


        edit: function (props) {

            function updateNewsletterTitle(e) {
                props.setAttributes({
                    newsletterTitle: e.target.value
                });
            }

            function updateNewsletterText(e) {
                props.setAttributes({
                    newsletterText: e.target.value
                });
            }

            function updateNewsletterCode(e) {
                props.setAttributes({
                    newsletterCode: e.target.value
                });
            }

            return [
                <div className='gfBlock'>
                    <h3>Newsletter Block</h3>
                    <div class='newsletterBlock'>
                        <div class='nlBlockInner'>
                            <label>Unesite naslov bloka</label>
                            <input onChange={updateNewsletterTitle} value={props.attributes.newsletterTitle}/>
                        </div>

                        <div class='nlBlockInner'>
                            <label>Unesite tekst</label>
                            <textarea cols="70" onChange={updateNewsletterText}
                                      value={props.attributes.newsletterText}></textarea>
                        </div>
                        <div class='nlBlockInner'>
                            <label>Unesite kod</label>
                            <textarea cols="70" onChange={updateNewsletterCode}
                                      value={props.attributes.newsletterCode}></textarea>
                        </div>
                    </div>
                </div>
            ]
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/pulseembed')) {
    registerBlockType('wpplugincontainer/pulseembed', {
        title: 'Blic video',
        icon: 'archive',
        category: 'gfBlocks',
        attributes: {
            embedHtml: {
                type: 'string'
            }
        },
        edit: function (props) {

            function updateEmbedHtml(e) {
                props.setAttributes({
                    embedHtml: e.target.value
                });
            }

            return (
                <div className={'gfBlock'}>
                    <h3>Pulse Embed</h3>
                    <div>
                        <label>Unesite html</label>
                    </div>
                    <div>
                        <textarea cols="100" rows="5" onChange={updateEmbedHtml}
                                  value={props.attributes.embedHtml}></textarea>
                    </div>
                </div>
            );
        },

        save: function () {
            return null;
        },
    });
}
if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/blockquote')) {
    registerBlockType('wpplugincontainer/blockquote', {
        title: 'GF Blockquote',
        icon: 'editor-quote',
        category: 'gfBlocks',
        attributes: {
            quote: {
                type: 'string'
            }
        },


        edit: function (props) {

            function updateQuote(e) {
                props.setAttributes({
                    quote: e.target.value
                });
            }


            return (
                <div className={'gfBlock'}>
                    <h3>Blockquote</h3>
                    <div>
                        <label>Unesite tekst</label>
                    </div>
                    <div>
                        <input type="text" placeholder='Unesite tekst' onChange={updateQuote}
                               value={props.attributes.quote}/>
                    </div>
                </div>
            );
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/relatedarticles')) {
    registerBlockType('wpplugincontainer/relatedarticles', {
        title: 'Pročitajte još',
        icon: 'align-left',
        category: 'gfBlocks',
        attributes: {
            post1: {
                type: 'int'
            },
            post2: {
                type: 'int'
            },
            post3: {
                type: 'int'
            },
            post4: {
                type: 'int'
            },
            post5: {
                type: 'int'
            },
            numberOfPosts: {
                type: 'string',
                default: '3'
            },
            currentPostId: {
                type: 'int',
                default: null
            },
            uniqueBlockIdentifier: {
                type: 'int',
                default: null
            }
        },


        edit: function (props) {

            if (props.attributes.currentPostId === null) {
                props.setAttributes({
                    currentPostId: wp.data.select("core/editor").getCurrentPostId()
                });
            }
            if (props.attributes.uniqueBlockIdentifier === null) {
                props.setAttributes({
                    uniqueBlockIdentifier: Date.now()
                })
            }




        function printInputsRelated() {
            switch (props.attributes.numberOfPosts) {
                case '1':
                    return <div className="newsInputIdEditor">
                        <div>
                            <input type="text" placeholder="Post #1" onChange={updatePostId1}
                                   value={props.attributes.post1}/>
                        </div>
                    </div>;
                case '2':
                    return <div className="newsInputIdEditor">
                        <div>
                            <input type="text" placeholder="Post #1" onChange={updatePostId1}
                                   value={props.attributes.post1}/>
                            <input type="text" placeholder="Post #2" onChange={updatePostId2}
                                   value={props.attributes.post2}/>
                        </div>
                    </div>;
                case '3':
                    return <div className="newsInputIdEditor">
                        <input type="text" placeholder="Post #1" onChange={updatePostId1}
                               value={props.attributes.post1}/>
                        <input type="text" placeholder="Post #2" onChange={updatePostId2}
                               value={props.attributes.post2}/>
                        <input type="text" placeholder="Post #3" onChange={updatePostId3}
                               value={props.attributes.post3}/>
                    </div>;
                case '4':
                    return <div className="newsInputIdEditor">
                        <input type="text" placeholder="Post #1" onChange={updatePostId1}
                               value={props.attributes.post1}/>
                        <input type="text" placeholder="Post #2" onChange={updatePostId2}
                               value={props.attributes.post2}/>
                        <input type="text" placeholder="Post #3" onChange={updatePostId3}
                               value={props.attributes.post3}/>
                        <input type="text" placeholder="Post #4" onChange={updatePostId4}
                               value={props.attributes.post4}/>
                    </div>;
                case '5':
                    return <div className="newsInputIdEditor">
                        <div>
                            <input type="text" placeholder="Post #1" onChange={updatePostId1}
                                   value={props.attributes.post1}/>
                            <input type="text" placeholder="Post #2" onChange={updatePostId2}
                                   value={props.attributes.post2}/>
                            <input type="text" placeholder="Post #3" onChange={updatePostId3}
                                   value={props.attributes.post3}/>
                            <input type="text" placeholder="Post #4" onChange={updatePostId4}
                                   value={props.attributes.post4}/>
                            <input type="text" placeholder="Post #5" onChange={updatePostId5}
                                   value={props.attributes.post5}/>
                        </div>
                    </div>;
                default:
                    return null;
            }
        }
            function updatePostId1(e) {
                props.setAttributes({
                    post1: e.target.value
                });
            }

            function updatePostId2(e) {
                props.setAttributes({
                    post2: e.target.value
                });
            }

            function updatePostId3(e) {
                props.setAttributes({
                    post3: e.target.value
                });
            }

            function updatePostId4(e) {
                props.setAttributes({
                    post4: e.target.value
                });
            }

            function updatePostId5(e) {
                props.setAttributes({
                    post5: e.target.value
                });
            }

            function updateNumberOfPostsRelated(e) {
                props.setAttributes({
                    numberOfPosts: e.target.value,
                    post1: null,
                    post2: null,
                    post3: null,
                    post4: null,
                    post5: null,
                })
                let inputsWrapper = e.target.parentElement.querySelector('.newsInputIdEditor');
                let inputs = inputsWrapper.querySelectorAll('input')

                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].value = '';
                }
            }

            return [
                <div class='gfBlock'>
                    <h3>Pročitajte još</h3>
                    <p>Izaberite broj postova za prikaz</p>
                    <div>
                        <select class="numberOfPoSTS" onChange={updateNumberOfPostsRelated} value={props.attributes.numberOfPosts}>
                            <option>-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option  selected="selected" value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    {printInputsRelated()}
                </div>
            ];
        },

        save: function () {
            return null;
        },
    });
}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/gfgalleryblock')) {

    registerBlockType('wpplugincontainer/gfgalleryblock', {

        title: 'GF Gallery block',
        icon: 'format-image',
        category: 'gfBlocks',
        attributes: {
            images: {
                type: 'array'
            },
        },

        edit: function (props) {
            const getImageButton = (openEvent) => {
                return [
                    <Button
                        onClick={openEvent}
                        className="button button-large">
                        Izaberite slike
                    </Button>
                ];
            };
            return [
                <div className='gfBlock'>
                    <h3>Euroblic Naslovna</h3>
                    <div className="button-container">
                        <MediaUpload
                            onSelect={(media) => props.setAttributes({images: media})}
                            type='image'
                            multiple={'add'}
                            value={props.attributes.images}
                            render={({open}) => getImageButton(open)}
                        />
                    </div>
                    <div dangerouslySetInnerHTML={{__html: printImages(props)}}>
                    </div>
                </div>

            ];

            function printImages(props) {
                if (props.attributes.images) {
                    let html = '';
                    props.attributes.images.forEach(image =>
                        html += `<img src=${image.url} className="gfImageEditor" style="width:150px; height:150px;"/>`);
                    return html;
                }
            }
        },

        save: function () {
            return null;
        },
    });

}

if (!wp.data.select('core/blocks').getBlockType('wpplugincontainer/ahtrefile')) {
    registerBlockType('wpplugincontainer/ahtrefile', {
        title: 'Antrfile',
        icon: 'align-left',
        category: 'gfBlocks',
        attributes: {
            title: {
                type: 'string'
            },
            content: {
                type: 'string'
            }
        },


        edit: function (props) {

            function updateTitle(e) {
                props.setAttributes({
                    title: e.target.value
                });
            }

            function updateContent(e) {
                props.setAttributes({
                    content: e.target.value
                });
            }

            return [
                <div class='gfBlock'>
                    <h3>Antrfile</h3>
                    <p>Unesite naslov i tekst</p>
                    <div>
                        <div>
                            <label>Naslov</label>
                        </div>
                        <input class="antrefileTitle" type="text" placeholder='Unesite naslov' onChange={updateTitle}
                               value={props.attributes.title}/>
                    </div>
                    <div>
                        <div>
                            <label>Tekst</label>
                        </div>
                        <textarea class="antrefileText" cols="80" onChange={updateContent} value={props.attributes.content}></textarea>
                    </div>
                </div>,

            ];
        },

        save: function () {
            return null;
        },
    });
}






