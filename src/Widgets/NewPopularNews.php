<?php


namespace GfWpPluginContainer\Widgets;

use Carbon\Carbon;
use GfWpPluginContainer\Wp\PostCount;
use GfWpPluginContainer\Wp\PostHelper;

class NewPopularNews extends \WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'gf_news_sidebar',
            'Newest/Popular News',
            ['description' => 'Widget for displaying latest/most popular news']
        );

    }

    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        // Default value of 5
        $numberOfPosts = '5';
        // If there is input
        if (isset($instance['numberOfPosts']) && $instance['numberOfPosts'] !== '') {
            $numberOfPosts = $instance['numberOfPosts'];
        }
        ?>
        <label for="<?= $this->get_field_id('numberOfPosts') ?>">Izaberite broj članaka za prikaz (najviše 10)</label>

        <div>
            <input type="text" id="<?= $this->get_field_id('numberOfPosts') ?>"
                   name="<?= $this->get_field_name('numberOfPosts') ?>" value="<?= $numberOfPosts ?>">
        </div>
        <?php
    }

    /**
     * Front-end display of widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     * @see WP_Widget::widget()
     *
     */
    public function widget($args, $instance)
    {
	    global $isApp, $isMobile;

	    $categorySlug = '';

        // If the user wants to display more than 10 posts or inputs a value of less than 0, set number of posts to 10
        if ($instance['numberOfPosts'] > '10' || $instance['numberOfPosts'] < '0') {
            $instance['numberOfPosts'] = '10';
        }
        // If there is input
        if (isset($instance['numberOfPosts']) && $instance['numberOfPosts'] !== '') {
            global $cache;

            $category = null;
            if (is_single()) {
                $category = get_category(wp_get_post_categories(get_queried_object()->ID)[0])->term_id;
            } else {
                $term = get_category_by_slug(get_queried_object()->post_name);
                if ($term) {
                    $category = $term->term_id;
                    $categorySlug = $term->slug;
                }
            }
	        $newestNewsPageUrl = get_permalink(get_page_by_path('sve-vijesti')->ID);
	        $popularNewsPageUrl = get_permalink(get_page_by_path('popularno')->ID);
            $key = 'newestPopularWidgetHtml#';
            if ((int)wp_is_mobile()) {
                $key = 'newestPopularWidgetHtmlMobile#';
            }
            if ($isApp) {
                $key = 'newestPopularWidgetHtmlApp#';
            }
            if($categorySlug === 'sport' && $categorySlug !== '') {
                $key = 'newestPopularWidgetSportHtml#';
            }
            $key .= $category;
            $isSport = false;
            if(PostHelper::isSportOrChildPage(get_queried_object())) {
                $isSport = true;
            }

            $template = $cache->get($key);
            if ($template === false) {
                $newest = PostHelper::getNewestPosts($instance['numberOfPosts']);
                if($isSport) {
                    $newest = PostHelper::getNewestPosts($instance['numberOfPosts'],$category);
                }
                $popular = PostHelper::getPopularPosts($instance['numberOfPosts'], $category);

                if ($isApp) {
                    $newestNewsPageUrl = parseAppUrl('page', $newestNewsPageUrl);
                    $popularNewsPageUrl = parseAppUrl('page', $popularNewsPageUrl);
                    ob_start();
                    $template = include(PLUGIN_DIR . 'templates/widgets/NewPopularNews/NewPopularNewsMobile.phtml');
                    ob_get_clean();
                } elseif($isMobile) {
                    ob_start();
                    $template = include(PLUGIN_DIR . 'templates/widgets/NewPopularNews/NewPopularNewsMobile.phtml');
                    ob_get_clean();
                } else {
                    ob_start();
                    $template = include(PLUGIN_DIR . 'templates/widgets/NewPopularNews/NewPopularNews.phtml');
                    ob_get_clean();
                }
                $cache->set($key, $template);
            }
            echo $template;
        }
    }

    private function printItems($posts)
    {
        // Check if request is coming from the APP and set route param in var
	    $isApp      = $_SERVER['REQUEST_URI'] === '/mobile-home/' || strpos( $_SERVER['REQUEST_URI'], 'isapp=true' ) !== false
	                  || strpos( $_SERVER['REQUEST_URI'], '/app/' ) !== false;

        global $isMobile;
        Carbon::setLocale('bs');
        $html = '';
        foreach ($posts as $post) {
            $postDate = new Carbon($post->post_date, new \DateTimeZone('Europe/Sarajevo'));
            $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());
            $postUrl = get_permalink($post->ID);
            $escapedTitle = esc_attr($post->post_title);
	        if ($isApp) {
		        $postUrl = parseAppUrl('article', $postUrl);
	        }
            $html .= sprintf('<li>
                    <div' . ($isMobile ? '' : ' class="news__time"') . '>
                        <span>%d</span>
                        <span>%s</span>
                    </div>
                    <h3><a title="%s" href="%s">%s</a></h3>
                </li>', $timeAgo[0], $timeAgo[1], $escapedTitle, $postUrl, $post->post_title);
        }
        return $html;
    }


    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     * @see WP_Widget::update()
     *
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['numberOfPosts'] = (!empty($new_instance['numberOfPosts'])) ? $new_instance['numberOfPosts'] : '';

        return $instance;
    }
}