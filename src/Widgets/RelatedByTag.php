<?php

namespace GfWpPluginContainer\Widgets;

use GfWpPluginContainer\Wp\Article;
use GfWpPluginContainer\Wp\Logger;
use GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner;
use GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors;
use GfWpPluginContainer\Wp\PostHelper;
use GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;

class RelatedByTag extends \WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'gf_related_tag',
            'Related posts by tag',
            ['description' => 'Widget for displaying related posts by tag']
        );
    }

    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        // Default value of 4
        $numberOfPosts = '4';
        // If there is input
        if (isset($instance['numberOfPosts']) && $instance['numberOfPosts'] !== '') {
            $numberOfPosts = $instance['numberOfPosts'];
        }
        ?>
        <label for="<?= $this->get_field_id('numberOfPosts') ?>">Izaberite broj tekstova za prikaz (4,8,12)</label>

        <div>
            <input type="text" id="<?= $this->get_field_id('numberOfPosts') ?>"
                   name="<?= $this->get_field_name('numberOfPosts') ?>" value="<?= $numberOfPosts ?>">
        </div>
        <?php
    }

    public function widget($args, $instance)
    {
        global $cache, $isApp;
        if($isApp) {
            $key = 'relatedByTagApp#' . get_queried_object_id();
        } else {
            $key = 'relatedByTag#' . get_queried_object_id();
        }
        $html = $cache->get($key);
        if ($html === false) {
            $html = $this->prepareHtml($args, $instance);
            $cache->set($key, $html);
        }

        echo $html;
    }

    public function prepareHtml($args, $instance)
    {
        $multipleAuthors = new MultipleAuthors();
        global $restrictedPosts, $isApp, $appSinglePost;

        // If the user wants to display other than 4,8,12 posts reset to 4
        if (!in_array($instance['numberOfPosts'], [4, 8, 12])) {
            $instance['numberOfPosts'] = 4;
        }
        $isSport = false;
        $forceCat = false;
        if ($isApp) {
            $isSport = PostHelper::isSportOrChildPageApp($appSinglePost);
        } else {
            $isSport = PostHelper::isSportOrChildPage(get_post(get_the_ID()));
        }
        if($isSport) {
            $forceCat = 6; // this is the sport category ID used to force posts from this category
        }
        $currentPostId = get_queried_object_id();
        $restrictedPosts[] = $currentPostId;
//        $postsWithSameTags = PostHelper::getPostsWithMatchingTag($currentPostId, $restrictedPosts, $instance['numberOfPosts']);

        $tagIds = [];
        foreach (wp_get_post_tags($currentPostId) as $postTag) {
            $tagIds[] = [
                'tagId' => $postTag->term_id,
                'name' => $postTag->name,
            ];
        }

        try {
            $postsWithSameTags = ArticleRepo::relatedByTags($tagIds, get_queried_object()->post_date, $restrictedPosts, $instance['numberOfPosts'], '_score', $forceCat)['articles'];
        } catch(\Exception $e) {
            Logger::generateDebugLog('elasticDebugLog',date('d-m-Y H:i:s') . '/relatedByTag/postsWithSameTags->' . $e->getTraceAsString());
            $postsWithSameTags = [];
        }

        if (count($postsWithSameTags) < $instance['numberOfPosts']) {
            $category = get_the_category($currentPostId)[0];
            if($category && $category->term_id !== 6) {
                $restrictedPosts = false;
            }
            try {
                $xtraPosts = ArticleRepo::getItemsFromElasticBy('category', $category, 1, $instance['numberOfPosts'] - count($postsWithSameTags),$restrictedPosts)['articles'];
            } catch(\Exception $e) {
                Logger::generateDebugLog('elasticDebugLog',date('d-m-Y H:i:s') . '/relatedByTag/xtraPostsByCat->' . $e->getTraceAsString());
                $xtraPosts = [];
            }

            $postsWithSameTags = array_merge($postsWithSameTags, $xtraPosts);
        }

        $className = $isSport ? 'sportRelatedBy' : '';
        $html = '<!--  Related Posts by Tags Start      -->
                <div class="container relatedBy">
                    <div class="box ' . $className .'">
                        <h3 class="box__title">Preporučujemo</h3>
                        <section class="news">';
        /* @var Article $article */
        foreach ($postsWithSameTags as $article) {
            $restrictedPosts[] = $article->getPostId();
            $postPermalink = $article->getPermalink();
            $postThumbnailUrl = $article->getThumbnail();
            if(!$postThumbnailUrl) {
                $postThumbnailUrl=wp_get_attachment_image_url(get_option('defaultFeaturedImage'),'list-small');
            }


            if($article->hasSecondLevelCategory()) {
                foreach($article->getCategory() as $articleCat) {
                    if($isSport) {
                        if ($articleCat['level'] === 2) {
                            $categoryName = $articleCat['name'];
                            $catSlug = $articleCat['slug'];
                        }
                    } elseif($articleCat['level'] === 1) {
                        $categoryName = $articleCat['name'];
                        $catSlug = $articleCat['slug'];
                    }
                }
            } else {
                $categoryName = $article->getCategory()['name'];
                $catSlug = $article->getCategory()['slug'];
            }
            $categoryLink = $isApp ?
                parseAppUrl('page', $catSlug) : get_home_url() . '/' . $catSlug;

            $postTitle = $article->getTitle();
            $postType = $article->getType();
            $postPermalink = $isApp ? parseAppUrl('article',get_permalink($article->getPostId())): get_permalink($article->getPostId());
            if (mb_strtoupper($categoryName) === 'KOLUMNE' ) {
                /* @var GfPostOwner $owner */
                $owner = $multipleAuthors->getOwnersForPost($article->getPostId())[0];
                $categoryName = $owner->getAuthorDisplayName();
                $authorUrl =  get_author_posts_url($owner->getAuthorId());
                $categoryLink = $isApp ? parseAppUrl('author',$authorUrl) : $authorUrl;
            }

	        if(basename($_SERVER['REQUEST_URI']) === 'amp'){
		        $html .= include( __DIR__ . '/../../templates/article/listItemMobileAmp.phtml' );
	        } else {
		        $html .= include(__DIR__ . '/../../templates/article/listItem.phtml');
	        }
        }
        $html .=  '</section></div></div><!--   Related Posts by Tags End     -->';

        return $html;
    }


    public function isMobileApp($request)
    {
        return $request['REQUEST_URI'] === '/mobile-home/' || strpos($request['REQUEST_URI'], '?isapp=true') !== false;
    }
}