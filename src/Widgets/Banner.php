<?php


namespace GfWpPluginContainer\Widgets;


class Banner extends \WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'gf_banner',
            'Banner',
            ['description' => 'Widget for displaying banners']
        );

    }


    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        $inputField = '';
        if (isset($instance['inputField']) && $instance['inputField'] !== '') {
            $inputField = $instance['inputField'];
        }
        $selectedFixed = '';
        $selectedSticky = '';
        $selectedStickyArea= '';

        if (isset($instance['bannerType'])) {
            switch ($instance['bannerType']) {
                case 'fixed':
                    $selectedFixed = 'selected';
                    break;
                case 'sticky':
                    $selectedSticky = 'selected';
                    break;
                case 'stickyArea':
                    $selectedStickyArea = 'selected';
                    break;
            }
        }



        ?>
        <label for="<?= $this->get_field_id('bannerType') ?>">Izabeirte tip banera</label>
        <div>
            <select id="<?= $this->get_field_id('bannerType') ?>" name=<?= $this->get_field_name('bannerType') ?>>
                <option <?= $selectedFixed ?> value="fixed">Fixed</option>
                <option <?= $selectedSticky ?> value="sticky">Sticky</option>
                <option <?= $selectedStickyArea ?> value="stickyArea">Sticky Area</option>
            </select>
        </div>
        <label for="<?= $this->get_field_id('inputField') ?>">Unesite kod za baner</label>
        <div>
            <input type="text" id="<?= $this->get_field_id('inputField') ?>"
                   name="<?= $this->get_field_name('inputField') ?>" value="<?= esc_html($inputField)?>">
        </div>
        <?php
    }

    /**
     * Front-end display of widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     * @see WP_Widget::widget()
     *
     */

    public function widget($args, $instance)
    {
        if (isset($instance['inputField'], $instance['bannerType']) && $instance['inputField'] !== '') {
            switch ($instance['bannerType']) {
                case 'fixed':
                    if (wp_is_mobile()) {
	                    echo '<div class="banner banner__top">'. $instance['inputField'] .'</div>';
                    } else {
	                    echo  $instance['inputField'];
                    }
                    break;
                case 'sticky':
                    echo '<div class="banner sticky">'. $instance['inputField'] .'</div>';
                    break;
                case 'stickyArea':
                    echo '<div class="sticky__area">'. '<div class="banner banner--desktop banner--space-bottom sticky">' . $instance['inputField'] .'</div></div>';
                    break;
                default:

                    break;
            }
        }

    }


    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     * @see WP_Widget::update()
     *
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['bannerType'] = (!empty($new_instance['bannerType'])) ? $new_instance['bannerType'] : '';
        $instance['inputField'] = (!empty($new_instance['inputField'])) ? $new_instance['inputField'] : '';

        return $instance;
    }
} 