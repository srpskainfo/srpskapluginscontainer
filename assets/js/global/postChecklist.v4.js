let isPost = false;
let isNewPost = false;
let isEditPost = false;

jQuery(document).ready(function ($) {
    if (jQuery('body').hasClass('post-type-post')) {
        isPost = true
    }
    if (jQuery('body').hasClass('post-new-php')) {
        isNewPost = true;
    }
    if (jQuery('body').hasClass('post-php')) {
        isEditPost = true;
    }
    if(!isEditPost && userRole === 'author'){
        jQuery(window).on("load",(function () {
            if (!jQuery('body').hasClass('post-type-page')) {
                jQuery('.editor-post-publish-panel__toggle, .editor-post-publish-button, .editor-post-publish-button__button')
                    .css('display', 'none');
            }
        }));
    }
    if (isEditPost && userRole === 'author') {
        jQuery(window).on("load",(function (){
            let postData = jQuery.extend({}, wp.data.select("core/editor").getCurrentPost(), wp.data.select("core/editor").getPostEdits());
            if (postData.status === 'draft' || postData.status === 'pending') {
                jQuery('.editor-post-publish-panel__toggle, .editor-post-publish-button, .editor-post-publish-button__button')
                    .css('display', 'none');
            }
        }))
    }

})
/*
    Save draft button is not loaded at first, this function keeps publishing button hidden until save draft one is visible
    so script for preventing saving post without category can work on that button also
 */
function waitForElementToDisplay(selector,selector2, time) {
    if(document.querySelector(selector)!=null || document.querySelector(selector2)!=null) {
        //This code only works with gutenberg editor
        jQuery('.editor-post-publish-panel__toggle, .editor-post-publish-button, .editor-post-publish-button__button, .editor-post-save-draft, .editor-post-switch-to-draft').click(function (e) {
            var allowed = true;
            let uncategorized = false;
            let msg = '';
            let postData = jQuery.extend({}, wp.data.select("core/editor").getCurrentPost(), wp.data.select("core/editor").getPostEdits());
            let postCategories = postData.categories;
            let postTags = postData.tags;
            let postFeaturedImage = postData.featured_media;
            let selectedCount = postCategories.length;
            if (postCategories) {
                if (selectedCount > 0) {
                    for (let i = 0; i < selectedCount; i++) {
                        if (postCategories[i] === 1) {
                            uncategorized = true;
                        }
                    }
                }
            }

            if (document.querySelectorAll('.authorSelect:checked').length === 0){
                allowed = false;
                e.preventDefault();
                msg += '\n Morate izabrati makar jednog autora teksta';
            }

            if (postTags.length < 1) {
                allowed = false;
                e.preventDefault();
                msg += '\n Morate izabrati minimum 1 tag';
            }
            if (postFeaturedImage === 0) {
                allowed = false;
                msg += '\n Morate izabrati istaknutu sliku'
            }

            if (uncategorized) {
                allowed = false;
                msg += '\n Morate ukloniti uncategorized kategoriju pre objavljivanja članka'
            } else if (!selectedCount || selectedCount === 0) {
                allowed = false;
                msg += '\n Morate izabrati kategoriju pre objavljivanja članka';
            } else if (selectedCount > 1) {
                // allowed = false; @todo sport has sub cats so this doesnt work now
                msg += '\n Može biti izabrana samo jedna kategorija'
            }
            if (allowed === false) {
                alert(msg);
                e.preventDefault();

            }
            return allowed
        })
    }
    else {
        setTimeout(function() {
            waitForElementToDisplay(selector,selector2, time);
        }, time);
    }
}

jQuery(window).on("load",(function ($) {
    if (isPost) {
        waitForElementToDisplay('.editor-post-save-draft','.editor-post-switch-to-draft',5000)
    }
}))

//sometimes window load doesent work
if (isPost) {
    waitForElementToDisplay('.editor-post-save-draft','.editor-post-switch-to-draft',5000)
}
