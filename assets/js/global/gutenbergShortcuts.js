class Shortcuts
{
    shortcutDescriptionContainer;
    constructor(props) {
        this.shortcutDescriptionContainerClassName = props.shortcutDescriptionContainerClassName;
        this.gutenbergShortcutDescriptions = props.gutenbergShortcutDescriptions;
    }

    init() {
        this.addListenerForShortcuts(this.handleGlobalAdminShortcuts);
        if(this.isNotPostPage() === false) {
            this.waitForElementToLoad(this.shortcutDescriptionContainerClassName,this.addShortcutDescription,1000);
            this.addListenerForShortcuts(this.handleGutenbergShortcuts);
        }
    }


    isNotPostPage() {
        return (!document.body.classList.contains('post-type-post') && !document.body.classList.contains('post-new-php'))
            || document.body.classList.contains('edit-php');
    }

    /* Sets an interval that checks if the element exists, if so executes the callback,
       if the element hasn't been found after the intervalMs the interval is cleared
     */
    waitForElementToLoad(elemClassName,callback,intervalMs) {
        let self = this;
        let timeElapsed = 0;
        let interval = setInterval(function (){
            let elem = document.querySelectorAll('.' + elemClassName)[0].querySelector('div');
            timeElapsed += 1;
            if(timeElapsed >= 10) {
                clearInterval(interval);
            }
            if(elem) {
                self.shortcutDescriptionContainer = elem;
                clearInterval(interval);
                callback();
            }
        },intervalMs)
    }
    /* Foreach shortcut description set in the props,
       call addShortcutInfo which appends the description to the container
    */
    addShortcutDescription = () => {
        let self = this;
        this.gutenbergShortcutDescriptions.forEach(function(value){
            self.addShortcutInfo(value.firstKey, value.secondKey, value.description)
        })
    }
    addShortcutInfo(firstKey,secondKey,description) {
        const shortcutSpan = document.createElement('span');
        shortcutSpan.style.marginRight = '10px';
        shortcutSpan.innerHTML = `<b style="font-weight: 900">${firstKey}+${secondKey}</b> = ${description}`;
        this.shortcutDescriptionContainer.append(shortcutSpan);
    }

    // Add event listeners on keyup with altKey. Takes a callback which should handle the seconds key press
    addListenerForShortcuts(callback) {
        let self = this;
        document.addEventListener('keyup', function(e) {
            self.getEvent(e || window.event).then(function(response){
                callback(response)
            })
        });
    }

    // If the event is an altKey return the event.which, which represents the key pressed after the altKey
    async getEvent(event) {
        if (event.altKey) {
            return event.which
        }
    }
    // Handle gutenberg shortcuts based on the second key pressed
    handleGutenbergShortcuts = (event) => {
        // Get the block index based on the clientId of the block
        let activeBlock = wp.data.select( 'core/block-editor' ).getSelectedBlock();
        if(activeBlock) {
            let activeBlockIndex = wp.data.select( 'core/block-editor' ).getBlockIndex(activeBlock.clientId);
            this.gutenbergShortcutDescriptions.forEach(function(value){
                if(event === value.secondKey.charCodeAt(0)) {
                    // If no elements are focused append the block to the bottom, else add it after the focused block
                    if(activeBlockIndex === -1) {
                        wp.data.dispatch("core/block-editor").insertBlocks(wp.blocks.createBlock(value.gutenbergBlockName, {}));
                    } else {
                        wp.data.dispatch("core/block-editor").insertBlocks(wp.blocks.createBlock(value.gutenbergBlockName, {}),activeBlockIndex +1);
                    }

                }
            });
        }
    }

    // Handle global admin shortcuts based on the second key pressed
    handleGlobalAdminShortcuts = (event) => {
        switch (event) {
            case (84):
                // Alt + T
                // Redirects to the new post page
                window.location.href = '/wp-admin/post-new.php';
                break;
        }
    }
}

let shortcuts = new Shortcuts({
    shortcutDescriptionContainerClassName: 'edit-post-header__toolbar',
    gutenbergShortcutDescriptions: [
        {firstKey:'ALT', secondKey: 'P', description: 'Pročitajte jos', gutenbergBlockName: 'wpplugincontainer/relatedarticles'},
        {firstKey:'ALT', secondKey: 'S', description: 'Slika', gutenbergBlockName: 'core/image'},
        {firstKey:'ALT', secondKey: 'V', description: 'Video Blic', gutenbergBlockName: 'wpplugincontainer/pulseembed'},
        {firstKey:'ALT', secondKey: 'G', description: 'Galerija', gutenbergBlockName: 'core/gallery'},
        {firstKey:'ALT', secondKey: 'A', description: 'Antrfile', gutenbergBlockName: 'wpplugincontainer/ahtrefile'},
    ]
})
wp.domReady(function(){
    shortcuts.init();
})